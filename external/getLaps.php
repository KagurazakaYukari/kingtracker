<?php

$base_path = realpath(dirname(__DIR__));
chdir($base_path);

require 'init_autoloader.php';
require 'vendor/lfs/LFSString.php';

use Zend\Loader\AutoloaderFactory;
use Zend\Mvc\Application;

// Setup autoloader
AutoloaderFactory::factory();

// Get application stack configuration
$configuration = include 'config/application.config.php';

// Init app
$app = @Application::init($configuration);
$sm = $app->getServiceManager();

// Get laps service
$lapsService = $sm->get('Insim\Service\LapsService');
$lapsService->debugSQL = true; // show SQL query

// Get 10 latest laps older than 2016-11-23 20:00:00
//$laps = $lapsService->fetchAllBy($where = ['clean' => 1, 'updated' => '>= "2016-11-23 20:22:53"'], $buffered = true, $tableGateway = false, $order = 'laps.updated DESC', $limit = 10);

// Get all clean laps
$laps = $lapsService->fetchAllBy($where = ['clean' => 1], $buffered = true, $tableGateway = false, $order = 'laps.updated DESC', $limit = null);

foreach ($laps as $lap) {
    //Zend\Debug\Debug::dump($lap);
    echo "ID: {$lap->id}, Time: {$lap->time_string} [{$lap->car}@$lap->track] - " . (LfsString::convert($lap->PName, 'CP1252')) . '<br/>';
}

die();

// Get top laps - same as !top command (fastest lap made by player)
// Lap conditions
$lapConditions = new \Insim\Model\LapTop();
$lapConditions->clean = true;
$lapConditions->track = 'BL1';
//$lapConditions->layout = '';
//$lapConditions->player_id = 0;
//$lapConditions->H_Mass = 0;
//$lapConditions->H_TRes = 0;

// Filter by cars
$lapFilter = [
    /*'car' => [
        'XFG' => 1, 'XRG' => 0
    ]*/
];

// Search by player name %PName%
$serach = [
    'PName_utf' => ''
];


$laps = $lapsService->findLapByConditions($lapConditions, true, $lapFilter, $serach);

foreach ($laps as $lap) {
    //Zend\Debug\Debug::dump($lap);
    echo "ID: {$lap->id}, Time: {$lap->time_string} [{$lap->car}@$lap->track] - " . (LfsString::convert($lap->PName, 'CP1252')) . '<br/>';
}
