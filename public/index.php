<?php

define('REQUEST_MICROTIME', microtime(true));

if (isset($_SERVER['APPLICATION_ENV']) && $_SERVER['APPLICATION_ENV'] == 'development') {
    
}

error_reporting(E_ALL ^ E_DEPRECATED);
    ini_set("display_errors", 1);

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

define('VERSION', 'v0.5.2f');
define('VERSION_INT', 55);   // installed version

define('BASE_PATH', realpath(dirname(__DIR__)));
define('PUBLIC_PATH', BASE_PATH.'/public');
define('VENDOR_PATH', BASE_PATH.'/vendor');
define('THEMES_PATH', PUBLIC_PATH.'/themes');
define('DEBUG_CHAT', false);

ini_set('log_errors', 1);
ini_set('error_log', BASE_PATH . '/logs/' . date('Y-m-d') . '_errors.log');

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server') {
    $path = realpath(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    if (__FILE__ !== $path && is_file($path)) {
        return false;
    }
    unset($path);
}

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();