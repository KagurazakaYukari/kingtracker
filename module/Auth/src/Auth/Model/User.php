<?php

namespace Auth\Model;

class User {

    public $id;
    public $login;
    public $passwd;
    public $salt;
    public $role;
    public $created_on;
    public $active;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->login = (!empty($data['login'])) ? $data['login'] : null;
        $this->passwd = (!empty($data['passwd'])) ? $data['passwd'] : null;
        $this->salt = (!empty($data['salt'])) ? $data['salt'] : null;
        $this->role = (!empty($data['role'])) ? $data['role'] : null;
        $this->created_on = (!empty($data['created_on'])) ? $data['created_on'] : null;
        $this->active = (!empty($data['active'])) ? $data['active'] : null;
    }
    
    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['login'] = (!empty($this->login)) ? $this->login : null;
        $data['passwd'] = (!empty($this->passwd)) ? $this->passwd : null;
        $data['salt'] = (!empty($this->salt)) ? $this->salt : null;
        $data['role'] = (!empty($this->role)) ? $this->role : null;
        $data['created_on'] = (!empty($this->created_on)) ? $this->created_on : null;
        $data['active'] = (!empty($this->active)) ? $this->active : null;

        return $data;
    }
}
