<?php

namespace Auth\Service;

use Application\Service\CoreService;
use Zend\Crypt\Password\Bcrypt;
use Zend\Db\TableGateway\TableGateway;

class UserService extends CoreService {
    protected $config;

    public function __construct(TableGateway $tableGateway) {
        parent::__construct($tableGateway);
    }
    
    public function getByLogin($login){
        return $this->getBy(array('login' => $login));
    }


    public function hashPassword($passwd, &$salt) {
        $bcrypt_cost = $this->config['bcrypt_cost'];
        $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
        
        $bcrypt = new Bcrypt();
        $bcrypt->setCost($bcrypt_cost);
        $bcrypt->setSalt($salt);
        return $bcrypt->create($passwd);
    }
    
    public function setConfig($service){
        $this->config = $service;
    }
}
