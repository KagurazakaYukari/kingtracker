<?php

namespace Auth\Form;

use Zend\Form\Form;

class LoginForm extends Form{
    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        
        $this->add(array(
            'name' => 'login',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'passwd',
            'attributes' => array(
                'type' => 'password',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'remember',
            'attributes' => array(
                'type' => 'checkbox',
            ),
            'options' => array(
                'label' => 'Remember me'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Login'
            ),
        ));
    }
}