<?php

namespace Auth\Form;

use Zend\Form\Form;

class RegisterForm extends Form{
    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        
        $this->add(array(
            'name' => 'login',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Login'
            )
        ));
        $this->add(array(
            'name' => 'passwd',
            'attributes' => array(
                'type' => 'password',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Password'
            )
        ));
        $this->add(array(
            'name' => 'passwd2',
            'attributes' => array(
                'type' => 'password',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Password'
            )
        ));
    }
}