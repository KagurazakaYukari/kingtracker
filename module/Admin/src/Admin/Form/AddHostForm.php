<?php

namespace Admin\Form;

use Zend\Form\Form;

class AddHostForm extends Form {

    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);

        $this->add(array('name' => 'active'));
        $this->add(array('name' => 'local'));

        $this->add(array(
            'name' => 'name',
            'options' => array(
                'label' => 'Host name',
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'ip',
            'options' => array(
                'label' => 'IP:port',
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control'
            ),
        ));
        
        $this->add(array( 'name' => 'port' ));

        $this->add(array(
            'name' => 'passwd',
            'options' => array(
                'label' => 'Admin password',
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'path',
            'options' => array(
                'label' => 'Full path to server',
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control'
            ),
        ));
    }
}
