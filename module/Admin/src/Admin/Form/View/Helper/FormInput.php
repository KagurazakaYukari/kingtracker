<?php

namespace Admin\Form\View\Helper;

use Zend\Form\ElementInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

/**
 * Description of FormInput
 *
 * @author Ján Kapľavka
 */
class FormInput extends \Zend\Form\View\Helper\FormInput {

    public function __invoke(ElementInterface $element = null){
        return parent::__invoke($element);
    }
    
    public function render(ElementInterface $element) {
        $out = parent::render($element);

        $view = new PhpRenderer();
        $resolver = new TemplateMapResolver();
        $resolver->setMap(array(
            'formInput' => __DIR__ . '/../../../../../../Application/view/helper/form/formInput.phtml',
        ));

        $view->setResolver($resolver);
        $layout = new ViewModel();
        $layout->setTemplate('formInput');
        $layout->setVariables(array(
            'element' => $element,
            'input' => $out,
        ));
        
        return $view->render($layout);
    }

}
