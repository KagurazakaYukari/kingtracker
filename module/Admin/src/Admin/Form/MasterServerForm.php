<?php

namespace Admin\Form;

use Zend\Form\Form;

class MasterServerForm extends Form {

    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);

        $this->add(array('name' => 'automatic_update'));

        $this->add(array(
            'name' => 'cid',
            'options' => array(
                'label' => 'CID',
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'key',
            'options' => array(
                'label' => 'KEY',
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control'
            ),
        ));
        
    }
}
