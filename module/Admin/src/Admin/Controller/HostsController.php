<?php

namespace Admin\Controller;

use Admin\Form\AddHostForm;
use Admin\Form\AddHostInputFilter;
use Admin\Model\Host;
use Admin\Service\HostService;
use Auth\Controller\AdminController;
use Exception;
use Insim\Types\InSimTypes;
use Zend\View\Model\ViewModel;

class HostsController extends AdminController {

    protected $hostService;

    public function __construct(HostService $hostService) {
        $this->hostService = $hostService;
    }

    public function indexAction() {
        $view = new ViewModel();

        $hosts = $this->hostService->fetchAllBy(array(), true, false, 'name ASC');

        return $view->setVariables(array(
                    'hosts' => $hosts,
        ));
    }

    /**
     * Add host
     */
    public function addAction() {
        $view = new ViewModel();
        $view->setTemplate('admin/hosts/add');

        $form = new AddHostForm();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost();

            $ipPort = explode(':', $post->ip);
            if (count($ipPort) == 2) {
                $post->ip = $ipPort[0];
                $post->port = $ipPort[1];
            } else {
                $form->get('ip')->setMessages(array('Wrong format use IP:PORT'));
            }

            $inputFilter = new AddHostInputFilter();
            $form->setInputFilter($inputFilter->inputFilter);
            $form->setData($post);
            if ($form->isValid()) {
                $data = $form->getData();

                $host = new Host();
                $host->name = $data['name'];
                $host->passwd = $data['passwd'];
                $host->path = $data['path'];
                $host->local = $data['local'];
                $host->ip = $data['ip'];
                $host->port = $data['port'];
                $host->active = 1;

                $this->hostService->save($host);
                $this->flashMessenger()->setNamespace('success')->addMessage('Host was successfully added');
                return $this->redirect()->toRoute('admin/default', array('controller' => 'hosts'));
            }

            $view->setVariable('post', $post);
        }

        return $view->setVariables(array(
                    'form' => $form
        ));
    }

    /**
     * Edit host
     * @return type
     */
    public function editAction() {
        $id = intval($this->getEvent()->getRouteMatch()->getParam('id'));

        try {
            $host = $this->hostService->getByID($id);
        } catch (Exception $ex) {
            return $this->redirect()->toRoute('admin/default', array('controller' => 'hosts'));
        }

        $view = new ViewModel();
        $view->setTemplate('admin/hosts/add');

        $form = new AddHostForm();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost();

            $ipPort = explode(':', $post->ip);
            if (count($ipPort) == 2) {
                $post->ip = $ipPort[0];
                $post->port = $ipPort[1];
            } else {
                $form->get('ip')->setMessages(array('Wrong format use IP:PORT'));
            }

            $inputFilter = new AddHostInputFilter();
            $form->setInputFilter($inputFilter->inputFilter);
            $form->setData($post);
            if ($form->isValid()) {
                $data = $form->getData();

                $host->name = $data['name'];
                $host->passwd = $data['passwd'];
                $host->path = $data['path'];
                $host->local = $data['local'];
                $host->ip = $data['ip'];
                $host->port = $data['port'];
                $host->active = $data['active'];

                $this->hostService->save($host);
                $this->flashMessenger()->setNamespace('success')->addMessage('Your changes have been saved');
                return $this->redirect()->toRoute('admin/default', array('controller' => 'hosts'));
            }

            $view->setVariable('post', $post);
        }

        return $view->setVariables(array(
                    'host' => $host,
                    'form' => $form
        ));
    }

    /**
     * Change host state - active / inactive
     * @return type
     */
    public function changeStateAction() {
        $id = intval($this->getEvent()->getRouteMatch()->getParam('id'));

        try {
            $host = $this->hostService->getByID($id);
        } catch (Exception $ex) {
            return $this->redirect()->toRoute('admin/default', array('controller' => 'hosts'));
        }

        $host->active = $host->active ? 0 : 1;
        $host->status = InSimTypes::CONN_NOTCONNECTED;
        $this->hostService->save($host);

        return $this->redirect()->toRoute('admin/default', array('controller' => 'hosts'));
    }

}
