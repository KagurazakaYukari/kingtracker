<?php

namespace Application\Service;

use Insim\Types\InSimTypes;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

abstract class CoreService {

    protected $tableGateway;
    protected $sm;
    public $debug = false;

    public function __construct(TableGateway $tableGateway = null) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Get entity by ...
     * @param type $where = array()
     * @param type $tableGateway = false
     * @return type
     */
    public function getBy($where = array(), $tableGateway = false) {
        $table = $tableGateway ? $tableGateway : $this->tableGateway;

        $rowset = $table->select($where);
        $row = $rowset->current();
        return $row;
    }

    public function getByID($id, $tableGateway = false) {
        return $this->getBy(array('id' => $id), $tableGateway);
    }

    public function fetchAll($buffered = false, $tableGateway = false) {
        $table = $tableGateway ? $tableGateway : $this->tableGateway;
        $resultSet = $table->select();

        if ($buffered)
            $resultSet->buffer();

        return $resultSet;
    }

    public function fetchAllBy($where = array(), $buffered = false, $tableGateway = false, $order = null, $limit = null) {
        $table = $tableGateway ? $tableGateway : $this->tableGateway;
        $resultSet = $table->select(function(Select $select) use ($where, $order, $limit) {
            foreach ($where as $key => $value) {
                $select->where->and->equalTo($key, $value);
            }

            if ($order)
                $select->order($order);

            if ($limit)
                $select->limit($limit);
        });

        if ($buffered)
            $resultSet->buffer();
        return $resultSet;
    }

    public function count($where = array(), $tableGateway = false) {
        $table = $tableGateway ? $tableGateway : $this->tableGateway;
        $resultSet = $table->select(function (Select $select) use($where) {
            foreach ($where as $key => $value) {
                $select->where->and->equalTo($key, $value);
            }

            $select->columns(array(
                'count' => new Expression('COUNT(id)')
            ));
        });

        return intval($resultSet->current()->count);
    }

    /**
     * Save object to DB
     * @param type $object
     * @param type $tableGateway
     * @return type
     */
    public function save($object, $tableGateway = false) {
        $table = $tableGateway ? $tableGateway : $this->tableGateway;
        $data = $object->exchangeObject();
        
        $id = (int) $object->id;
        if ($id == 0) {
            $table->insert($data);
            return $table->lastInsertValue;
        } else {
            $table->update($data, array('id' => $id));
        }
    }

    public function delete($id) {
        $where = new Where();
        $where->equalTo('id', (int) $id);

        $this->tableGateway->delete($where);
    }

    public function setDebug($debug) {
        $this->debug = $debug;
    }

    public function log($message, $time = true) {
        $message = ($time ? "[" . (date(InSimTypes::LOG_DATE_FORMAT)) . "] - " : "") . $message . PHP_EOL;

        if ($this->debug)
            echo $message;
    }

    public function toArray($resultSet, $key = null) {
        $return = array();

        foreach ($resultSet as $res) {
            if($key){
                $return[$res->{$key}] = $res;
            }else{
                $return[] = $res;
            }
        }

        return $return;
    }

    public function objectToArray($d) {
        if (is_object($d)) {
            return get_object_vars($d);
        }
    }

}
