<?php

namespace Application;

use Zend\Mvc\ModuleRouteListener;

class Module {

    public function onBootstrap(\Zend\Mvc\MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $this->errorLogger($e);

        /* $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, function($e) {
          $result = $e->getResult();
          $result->setTerminal(TRUE);
          }); */
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Logger' => function($sm) {
                    $logger = new \Zend\Log\Logger;
                    $writer = new \Zend\Log\Writer\Stream(BASE_PATH . '/logs/' . date('Y-m-d') . '_errors_zf.log');
                    $formatter = new \Zend\Log\Formatter\Simple('[%timestamp%] %priorityName% (%priority%): %message% %extra%' . PHP_EOL, 'Y-m-d H:i:s');
                    $writer->setFormatter($formatter);
                    $logger->addWriter($writer);
                    return $logger;
                },
            )
        );
    }

    public function errorLogger($e) {
        $sharedManager = $e->getApplication()->getEventManager()->getSharedManager();
        $sm = $e->getApplication()->getServiceManager();
        $sharedManager->attach('Zend\Mvc\Application', 'dispatch.error', function($e) use ($sm) {
            if ($e->getParam('exception')) {
                $ex = $e->getParam('exception');
                do {
                    $msg = sprintf(
                            "[%s] %s (%d): %s in %s:%d \nStackTrace:\n%s", date('Y-m-d H:i:s'), get_class($ex), $ex->getCode(), $ex->getMessage(), $ex->getFile(), $ex->getLine(), $ex->getTraceAsString()
                    );
                    $msg .= '----------------------------------------------------------------------------';
                    $sm->get('Logger')->crit($ex);
                } while ($ex = $ex->getPrevious());
            }
        }
        );
    }

}
