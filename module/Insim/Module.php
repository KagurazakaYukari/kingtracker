<?php

namespace Insim;

use Insim\Controller\InsimController;
use Insim\Model\Host;
use Insim\Model\HostConn;
use Insim\Model\HostPlayer;
use Insim\Model\LapTop;
use Insim\Model\LFSWWR;
use Insim\Model\Param;
use Insim\Model\Player;
use Insim\Model\PlayerACL;
use Insim\Model\PlayerBan;
use Insim\Model\PlayerRecent;
use Insim\Model\Task;
use Insim\Model\Track;
use Insim\Model\PlayerSettings;
use Insim\Model\HostSettings;
use Insim\Service\ACLService;
use Insim\Service\CommandService;
use Insim\Service\HostService;
use Insim\Service\InsimService;
use Insim\Service\LapsService;
use Insim\Service\LFSWService;
use Insim\Service\ParamsService;
use Insim\Service\PlayerService;
use Insim\Service\RequestsService;
use Insim\Service\TaskService;
use Insim\Service\TrackService;
use Insim\Service\PlayerSettingsService;
use Insim\Service\HostSettingsService;
use Insim\Service\UIService;
use Insim\Service\UpdateService;
use Insim\Service\SafetyService;
use Insim\Service\LayoutService;
use Zend\Console\Adapter\AdapterInterface as Console;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;
use Zend\Mvc\Controller\ControllerManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ConsoleUsageProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Insim\Service\InsimService' => function($sm) {
                    $service = new InsimService();
                    return $service;
                },
                'Insim\Service\HostService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Host());
                    $tableGateway = new TableGateway('hosts', $dbAdapter, null, $resultSetPrototype);

                    $table = new HostService($tableGateway);
                    return $table;
                },
                'Insim\Service\TaskService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Task());
                    $tableGateway = new TableGateway('tasks', $dbAdapter, null, $resultSetPrototype);

                    $table = new TaskService($tableGateway);
                    return $table;
                },
                'Insim\Service\TrackService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Track());
                    $tableGateway = new TableGateway('tracks', $dbAdapter, null, $resultSetPrototype);

                    $table = new TrackService($tableGateway);
                    return $table;
                },
                'Insim\Service\LFSWService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new LFSWWR());
                    $tableWR = new TableGateway('lfsw_wr', $dbAdapter, null, $resultSetPrototype);

                    $resultSetPrototypePB = new ResultSet();
                    $resultSetPrototypePB->setArrayObjectPrototype(new Model\LFSWPB());
                    $tablePB = new TableGateway('lfsw_pb', $dbAdapter, null, $resultSetPrototypePB);

                    $resultSetPrototypePubStat = new ResultSet();
                    $resultSetPrototypePubStat->setArrayObjectPrototype(new Model\PubStat());
                    $tableGateway = new TableGateway('lfsw_pubstat', $dbAdapter, null, $resultSetPrototypePubStat);
                    $service = new LFSWService($tableGateway);
                    $service->tableWR = $tableWR;
                    $service->tablePB = $tablePB;
                    return $service;
                },
                'Insim\Service\PlayerService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');

                    $resultSetPlayer = new ResultSet();
                    $resultSetPlayer->setArrayObjectPrototype(new HostPlayer());
                    $tablePlayers = new TableGateway('hosts_players', $dbAdapter, null, $resultSetPlayer);

                    $resultSetConn = new ResultSet();
                    $resultSetConn->setArrayObjectPrototype(new HostConn());
                    $tableConn = new TableGateway('hosts_conns', $dbAdapter, null, $resultSetConn);

                    $resultSetPLDB = new ResultSet();
                    $resultSetPLDB->setArrayObjectPrototype(new Player());
                    $tablePLDB = new TableGateway('players', $dbAdapter, null, $resultSetPLDB);

                    $resultSetPLBan = new ResultSet();
                    $resultSetPLBan->setArrayObjectPrototype(new PlayerBan);
                    $tablePLBan = new TableGateway('players_ban', $dbAdapter, null, $resultSetPLBan);

                    $resultSetPLRecent = new ResultSet();
                    $resultSetPLRecent->setArrayObjectPrototype(new PlayerRecent);
                    $tablePLRecent = new TableGateway('players_recent', $dbAdapter, null, $resultSetPLRecent);

                    $service = new PlayerService($tablePlayers);
                    $service->setConnsTable($tableConn);
                    $service->setplayersTable($tablePLDB);
                    $service->setPlayersBanTable($tablePLBan);
                    $service->setPlayersRecentTable($tablePLRecent);
                    return $service;
                },
                'Insim\Service\PlayerSettingsService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PlayerSettings());
                    $table = new TableGateway('players_settings', $dbAdapter, null, $resultSetPrototype);

                    $table = new PlayerSettingsService($table);
                    return $table;
                },
                'Insim\Service\HostSettingsService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new HostSettings());
                    $table = new TableGateway('hosts_settings', $dbAdapter, null, $resultSetPrototype);

                    $table = new HostSettingsService($table);
                    return $table;
                },
                'Insim\Service\CommandService' => function($sm) {
                    $service = new CommandService();
                    return $service;
                },
                'Insim\Service\LapsService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new LapTop());
                    $tableGateway = new TableGateway('laps', $dbAdapter, null, $resultSetPrototype);

                    $service = new LapsService($tableGateway);
                    return $service;
                },
                'Insim\Service\UIService' => function($sm) {
                    $service = new UIService();
                    return $service;
                },
                'Insim\Service\RequestsService' => function($sm) {
                    $service = new RequestsService();
                    return $service;
                },
                'Insim\Service\ACLService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PlayerACL());
                    $tableGateway = new TableGateway('players_acl', $dbAdapter, null, $resultSetPrototype);

                    $service = new ACLService($tableGateway);
                    return $service;
                },
                'Insim\Service\UpdateService' => function($sm) {
                    $service = new UpdateService();
                    return $service;
                },
                'Insim\Service\ParamsService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Param());
                    $tableGateway = new TableGateway('params', $dbAdapter, null, $resultSetPrototype);

                    $service = new ParamsService($tableGateway);
                    return $service;
                },
                'Insim\Service\ResultService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Result());
                    $tableGateway = new TableGateway('results', $dbAdapter, null, $resultSetPrototype);

                    $resultSetPrototypePlayer = new ResultSet();
                    $resultSetPrototypePlayer->setArrayObjectPrototype(new Model\ResultPlayer());
                    $tableGatewayPlayer = new TableGateway('results_player', $dbAdapter, null, $resultSetPrototypePlayer);

                    $resultSetPrototypeEvent = new ResultSet();
                    $resultSetPrototypeEvent->setArrayObjectPrototype(new Model\ResultEvent());
                    $tableGatewayEvent = new TableGateway('results_event', $dbAdapter, null, $resultSetPrototypeEvent);

                    $service = new Service\ResultService($tableGateway);
                    $service->setResultPlayerTable($tableGatewayPlayer);
                    $service->setResultEventTable($tableGatewayEvent);
                    return $service;
                },
                'Insim\Service\CarService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Car());
                    $tableGateway = new TableGateway('cars', $dbAdapter, null, $resultSetPrototype);

                    $service = new Service\CarService($tableGateway);
                    return $service;
                },
                'Insim\Service\SafetyService' => function($sm) {
                    $service = new SafetyService();
                    return $service;
                },
                'Insim\Service\LayoutService' => function($sm) {
                    $service = new LayoutService();
                    return $service;
                },
            ),
        );
    }

    public function getConsoleUsage(Console $console) {
        return array(
            'insim start [--debug|-d] ' => 'Start InSim server',
            array('--debug|-d', '(optional) turn on console debug mode'),
        );
    }

    public function getControllerConfig() {
        return array(
            'factories' => array(
                'Insim\Controller\Insim' => function(ControllerManager $cm) {
                    $sm = $cm->getServiceLocator();

                    $c = new InsimController();

                    // Order is important here
                    $c->setInsimService($sm->get('Insim\Service\InsimService'));
                    $c->setTranslatorService($sm->get('translator'));
                    $c->setPlayerSettingsService($sm->get('Insim\Service\PlayerSettingsService'));
                    $c->setHostSettingsService($sm->get('Insim\Service\HostSettingsService'));
                    $c->setCarService($sm->get('Insim\Service\CarService'));
                    $c->setSafetyService($sm->get('Insim\Service\SafetyService'));
                    $c->setLFSWService($sm->get('Insim\Service\LFSWService'));
                    $c->setTrackService($sm->get('Insim\Service\TrackService'));
                    $c->setLayoutService($sm->get('Insim\Service\LayoutService'));
                    $c->setParamsService($sm->get('Insim\Service\ParamsService'));
                    $c->setResultService($sm->get('Insim\Service\ResultService'));
                    $c->setHostService($sm->get('Insim\Service\HostService'));
                    $c->setTaskService($sm->get('Insim\Service\TaskService'));
                    $c->setACLService($sm->get('Insim\Service\ACLService'));
                    $c->setUIService($sm->get('Insim\Service\UIService'));
                    $c->setCommandService($sm->get('Insim\Service\CommandService'));
                    $c->setTaskService($sm->get('Insim\Service\TaskService'));
                    $c->setLapsService($sm->get('Insim\Service\LapsService'));
                    $c->setUpdateService($sm->get('Insim\Service\UpdateService'));

                    $c->setRequestsService($sm->get('Insim\Service\RequestsService'));
                    $c->setPlayersService($sm->get('Insim\Service\PlayerService'));

                    return $c;
                },
            ),
        );
    }

}
