<?php

return array(
    'controllers' => array(
        'invokables' => array(
        //'Insim\Controller\Insim' => 'Insim\Controller\InsimController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'insim' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/insim[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Insim\Controller\Insim',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
                'start-server' => array(
                    'options' => array(
                        'route' => 'insim start [--debug|-d]',
                        'defaults' => array(
                            'controller' => 'Insim\Controller\Insim',
                            'action' => 'start'
                        )
                    )
                ),
                'Update' => array(
                    'options' => array(
                        'route' => 'insim update',
                        'defaults' => array(
                            'controller' => 'Insim\Controller\Insim',
                            'action' => 'update'
                        )
                    )
                )
            )
        )
    ),
    'view_manager' => array(
        'doctype' => 'HTML5',
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy'
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => function (\Zend\ServiceManager\ServiceManager $serviceManager) {
                $translator = \Insim\Service\TranslatorService::factory([
                            'locale' => 'en_US',
                            'translation_file_patterns' => array(
                                array(
                                    'type' => 'phparray',
                                    'base_dir' => BASE_PATH . '/langs',
                                    'pattern' => '%s.txt',
                                ),
                            ),
                ]);

                $translator->setFallbackLocale('en_US');

                return $translator;
            }
                ),
                'aliases' => array(
                // Alias needed for Identity view helper
                //'Zend\Authentication\AuthenticationService' => 'AuthService',
                ),
            ),
        );
        