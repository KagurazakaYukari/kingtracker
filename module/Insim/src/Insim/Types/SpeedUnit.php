<?php

namespace Insim\Types;

class SpeedUnit{
    const KPH = 'kph';
    const MPH = 'mph';
}