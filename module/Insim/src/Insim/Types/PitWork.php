<?php

namespace Insim\Types;

class PitWork {

    const PSE_NOTHING = 1;          // (1 << 0)); // bit 0 (1)
    const PSE_STOP = 2;             // (1 << 1)); // bit 1 (2)
    const PSE_FR_DAM = 4;           // (1 << 2)); // bit 2 (4)
    const PSE_FR_WHL = 8;           // (1 << 3)); // etc...
    const PSE_LE_FR_DAM = 16;       // (1 << 4));
    const PSE_LE_FR_WHL = 32;       // (1 << 5));
    const PSE_RI_FR_DAM = 64;       // (1 << 6));
    const PSE_RI_FR_WHL = 128;      // (1 << 7));
    const PSE_RE_DAM = 256;         // (1 << 8));
    const PSE_RE_WHL = 512;         // (1 << 9));
    const PSE_LE_RE_DAM = 1024;     // (1 << 10));
    const PSE_LE_RE_WHL = 2048;     // (1 << 11));
    const PSE_RI_RE_DAM = 4096;     // (1 << 12));
    const PSE_RI_RE_WHL = 8192;     // (1 << 13));
    const PSE_BODY_MINOR = 16384;   // (1 << 14));
    const PSE_BODY_MAJOR = 32768;   // (1 << 15));
    const PSE_SETUP = 65536;        // (1 << 16));
    const PSE_REFUEL = 131072;      // (1 << 17));

    public static function isAllTyres($work) {
        return ($work & self::PSE_LE_FR_WHL) && ($work & self::PSE_RI_FR_WHL) && ($work & self::PSE_LE_RE_WHL) && ($work & self::PSE_RI_RE_WHL);
    }
    
    public static function isRefuel($work) {
        return ($work & self::PSE_REFUEL);
    }

}
