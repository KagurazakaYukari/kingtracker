<?php

namespace Insim\Types;

class InSimTypes{
    const INSIM_VERSION = 7;
    
    const CONN_NOTCONNECTED = 0;
    const CONN_CONNECTING = 1;
    const CONN_CONNECTED = 2;
    const CONN_TIMEOUT = 3;
    
    const LOG_DATE_FORMAT = 'Y-m-d H:i:s';
    
    const KEEP_ALIVE_TIME = 80;
    
    /**
     * Delay between sending packets to player in ms
     */
    const DELAY_PACKET_SEND = 6;
    
    const ISF_MCI = 32;
    const ISF_CON = 64;
    const ISF_OBH = 128;
    const ISF_HLV = 256;
}