<?php

namespace Insim\Types;

class MsgTypes{
    const INFO = '^7';
    const NORMAL = '^8';
    const NOTIFY = '^6';
    const WARNING = '^1';
    
    const BLACK = '^0';
    const RED = '^1';
    const GREEN = '^2';
    const YELLOW = '^3';
    const BLUE = '^4';
    const PINK = '^5';
    const LIGHT_BLUE = '^6';
    const WHITE = '^7';
    const GRAY = '^8';
}

class MsgEncoding{
    const LATIN = '^L';
}