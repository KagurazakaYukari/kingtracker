<?php

namespace Insim\Types;

class PlayerFlags{
    const SWAPSIDE = 1;
    const RESERVED_2 = 2;
    const RESERVED_4 = 4;
    const AUTOGEARS = 8;
    const SHIFTER = 16;
    const RESERVED_32 = 32;
    const HELP_B = 64;
    const AXIS_CLUTCH = 128;
    const INPITS = 256;
    const AUTOCLUTCH = 512;
    const MOUSE = 1024;
    const KB_NO_HELP = 2048;
    const KB_STABILISED = 4096;
    const CUSTOM_VIEW = 8192;
    
    public static function toString($type){
        if($type & PlayerFlags::MOUSE)
            $out = 'M';
        elseif($type & PlayerFlags::KB_NO_HELP)
            $out = 'KB';
        elseif($type & PlayerFlags::KB_STABILISED)
            $out = 'KBS';
        else
            $out = 'W';
            
        if($type & PlayerFlags::AUTOCLUTCH)
            $out .= ', AC';
        elseif($type & PlayerFlags::AXIS_CLUTCH)
            $out .= ', AxisC';
        else
            $out .= ', BC';
        
        if($type & PlayerFlags::AUTOGEARS)
            $out .= ', AutoGears';
        elseif($type & PlayerFlags::SHIFTER)
            $out .= ', H-Shifter';
        
        if($type & PlayerFlags::HELP_B)
            $out .= ', Brake Help';
        
        return $out;
    }
    
}