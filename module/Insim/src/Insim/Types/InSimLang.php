<?php

namespace Insim\Types;

class InSimLang {

    const LFS_ENGLISH = 0;
    const LFS_DEUTSCH = 1;
    const LFS_PORTUGUESE = 2;
    const LFS_FRENCH = 3;
    const LFS_SUOMI = 4;
    const LFS_NORSK = 5;
    const LFS_NEDERLANDS = 6;
    const LFS_CATALAN = 7;
    const LFS_TURKISH = 8;
    const LFS_CASTELLANO = 9;
    const LFS_ITALIANO = 10;
    const LFS_DANSK = 11;
    const LFS_CZECH = 12;
    const LFS_RUSSIAN = 13;
    const LFS_ESTONIAN = 14;
    const LFS_SERBIAN = 15;
    const LFS_GREEK = 16;
    const LFS_POLSKI = 17;
    const LFS_CROATIAN = 18;
    const LFS_HUNGARIAN = 19;
    const LFS_BRAZILIAN = 20;
    const LFS_SWEDISH = 21;
    const LFS_SLOVAK = 22;
    const LFS_GALEGO = 23;
    const LFS_SLOVENSKI = 24;
    const LFS_BELARUSSIAN = 25;
    const LFS_LATVIAN = 26;
    const LFS_LITHUANIAN = 27;
    const LFS_TRADITIONAL_CHINESE = 28;
    const LFS_SIMPLIFIED_CHINESE = 29;
    const LFS_JAPANESE = 30;
    const LFS_KOREAN = 31;
    const LFS_BULGARIAN = 32;
    const LFS_LATINO = 33;
    const LFS_UKRAINIAN = 34;
    const LFS_INDONESIAN = 35;
    const LFS_ROMANIAN = 36;
    const LFS_NUM_LANG = 37;

    protected static $langs = array(
        InSimLang::LFS_ENGLISH => 'EN',
        InSimLang::LFS_DEUTSCH => 'DE',
        InSimLang::LFS_PORTUGUESE => 'PT',
        InSimLang::LFS_FRENCH => 'FR',
        InSimLang::LFS_SUOMI => 'FI',
        InSimLang::LFS_NORSK => 'NO',
        InSimLang::LFS_NEDERLANDS => 'NL',
        InSimLang::LFS_CATALAN => 'CA',
        InSimLang::LFS_TURKISH => 'TR',
        InSimLang::LFS_CASTELLANO => 'ES',
        InSimLang::LFS_ITALIANO => 'IT',
        InSimLang::LFS_DANSK => 'DA',
        InSimLang::LFS_CZECH => 'CS',
        InSimLang::LFS_RUSSIAN => 'RU',
        InSimLang::LFS_ESTONIAN => 'ET',
        InSimLang::LFS_SERBIAN => 'SR',
        InSimLang::LFS_GREEK => 'EL',
        InSimLang::LFS_POLSKI => 'PL',
        InSimLang::LFS_CROATIAN => 'HR',
        InSimLang::LFS_HUNGARIAN => 'HU',
        InSimLang::LFS_BRAZILIAN => 'BR',
        InSimLang::LFS_SWEDISH => 'SV',
        InSimLang::LFS_SLOVAK => 'SK',
        InSimLang::LFS_GALEGO => 'GL',
        InSimLang::LFS_SLOVENSKI => 'SL',
        InSimLang::LFS_BELARUSSIAN => 'BE',
        InSimLang::LFS_LATVIAN => 'LV',
        InSimLang::LFS_LITHUANIAN => 'LT',
        InSimLang::LFS_TRADITIONAL_CHINESE => 'ZH',
        InSimLang::LFS_SIMPLIFIED_CHINESE => 'ZH',
        InSimLang::LFS_JAPANESE => 'JA',
        InSimLang::LFS_KOREAN => 'KO',
        InSimLang::LFS_BULGARIAN => 'BG',
        InSimLang::LFS_LATINO => 'LA',
        InSimLang::LFS_UKRAINIAN => 'UK',
        InSimLang::LFS_INDONESIAN => 'ID',
        InSimLang::LFS_ROMANIAN => 'RO',
    );
    
    protected static $locales = array(
        InSimLang::LFS_ENGLISH => 'en_US',
        InSimLang::LFS_DEUTSCH => 'de_DE',
        InSimLang::LFS_PORTUGUESE => 'pt_PT',
        InSimLang::LFS_FRENCH => 'fr_FR',
        InSimLang::LFS_SUOMI => 'fi_FI',
        InSimLang::LFS_NORSK => 'nb_NO',
        InSimLang::LFS_NEDERLANDS => 'da_DK',
        InSimLang::LFS_CATALAN => 'ca_ES',
        InSimLang::LFS_TURKISH => 'tr_TR',
        InSimLang::LFS_CASTELLANO => 'en_US',
        InSimLang::LFS_ITALIANO => 'it_IT',
        InSimLang::LFS_DANSK => 'da_DK',
        InSimLang::LFS_CZECH => 'cs_CZ',
        InSimLang::LFS_RUSSIAN => 'ru_RU',
        InSimLang::LFS_ESTONIAN => 'et_EE',
        InSimLang::LFS_SERBIAN => 'sr_RS',
        InSimLang::LFS_GREEK => 'el_GR',
        InSimLang::LFS_POLSKI => 'pl_PL',
        InSimLang::LFS_CROATIAN => 'hr_HR',
        InSimLang::LFS_HUNGARIAN => 'hu_HU',
        InSimLang::LFS_BRAZILIAN => 'pt_BR',
        InSimLang::LFS_SWEDISH => 'sv_SE',
        InSimLang::LFS_SLOVAK => 'sk_SK',
        InSimLang::LFS_GALEGO => 'en_US',
        InSimLang::LFS_SLOVENSKI => 'sl_SI',
        InSimLang::LFS_BELARUSSIAN => 'be_BY',
        InSimLang::LFS_LATVIAN => 'lv_LV',
        InSimLang::LFS_LITHUANIAN => 'lt_LT',
        InSimLang::LFS_TRADITIONAL_CHINESE => 'zh_TW',
        InSimLang::LFS_SIMPLIFIED_CHINESE => 'zh_CN',
        InSimLang::LFS_JAPANESE => 'ja_JP',
        InSimLang::LFS_KOREAN => 'ko_KR',
        InSimLang::LFS_BULGARIAN => 'bg_BG',
        InSimLang::LFS_LATINO => 'en_US',
        InSimLang::LFS_UKRAINIAN => 'ru_UA',
        InSimLang::LFS_INDONESIAN => 'id_ID',
        InSimLang::LFS_ROMANIAN => 'ro_RO',
    );

    public static function getLang($lang_code){
        return isset(self::$langs[$lang_code]) ? self::$langs[$lang_code] : 'EN';
    }
    
    public static function getLocale($lang_code){
        return isset(self::$locales[$lang_code]) ? self::$locales[$lang_code] : 'en_US';
    }
}