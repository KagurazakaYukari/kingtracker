<?php

namespace Insim\Types;

class GridModes{
    const FINISH = 'finish';
    const REVERSE = 'reverse';
    const RANDOM = 'random';
}