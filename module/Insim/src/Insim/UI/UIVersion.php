<?php

namespace Insim\UI;

use Insim\Model\LapTop;
use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Packets\isMTC;
use Insim\Types\ButtonStyles;
use Zend\Debug\Debug;
use Zend\Paginator\Adapter\Iterator as PaginatorIterator;
use Zend\Paginator\Paginator;

class UIVersion extends UI {

    public $columns = array();
    public $rows = array();
    public $cars = array();
    public $filter = array();
    public $search = array();
    public $topActions = array();
    public $own_time = array();
    public $own_buttons = array();
    public $rows_buttons = array();
    public $rows_per_page = 20;
    public $items_per_page = 20;
    public $show_help = false;
    protected $lines = array();

    function __construct($alias, PlayerClass &$player, $width = 122, $height = 101, $top = 30) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = (200 - $width) / 2;

        // columns
        $this->columns = array(
            'line' => array('width' => $this->width - 2, 'name' => 'Line', 'style' => ButtonStyles::ISB_LEFT),
        );

        $this->lines = file(BASE_PATH . '/CHANGELOG.txt');

        parent::__construct($alias, $player);
    }

    public function setData() {
        $this->rows = array();

        $start = $this->current_page == 1 ? 0 : ($this->current_page - 1) * $this->rows_per_page;
        $end = $start + $this->rows_per_page - 1;

        for ($i = $start; $i <= $end; $i++) {
            $this->rows[] = array(
                'line' => isset($this->lines[$i]) ? $this->lines[$i] : '',
            );
        }
        
        $this->max_page = ceil(count($this->lines) / $this->rows_per_page);
        $this->status_line = $this->current_page . ' / ' . $this->max_page;
    }

    public function show($showBase = true) {
        $this->setData();

        if ($showBase)
            $this->showBase();

        $this->button_content_min = $this->id_current + 1;

        $button = new isBTN();
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 2 + 12;
        $button->H = 88;
        $button->W = $this->width - 2;
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);
        
        $button = new isBTN();
        $newLine = 0;
        foreach ($this->rows as $key => $row) {
            $rowIndex = 0;
            foreach ($this->columns as $keyCol => $column) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = !$rowIndex ? ($this->left) + 3 : $button->L + $button->W;
                $button->T = $this->top + 2 + 15 + $newLine;
                $button->H = 4;
                $button->W = $column['width'];
                $button->BStyle = ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
                $button->Text = isset($row[$keyCol]) ? $row[$keyCol] : '';

                $this->send($button);
                $this->rows_buttons[$key][$keyCol] = $button->ClickID;
                $rowIndex++;
            }
            $newLine += 4;
        }


        $this->showFooter(-2);


        $this->button_content_max = $this->id_current;

        parent::show();
    }

    public function redrawContent() {
        $this->setData();

        $button = new isBTN();
        $button->ClickID = $this->button_id_status_line;
        $button->Text = $this->status_line;
        $this->send($button);

        foreach ($this->rows as $row_id => $row) {
            $buttons = $this->rows_buttons[$row_id];

            foreach ($buttons as $key => $id) {
                $button->ClickID = $id;
                $button->Text = $this->rows[$row_id][$key];
                $this->send($button);
            }
        }

        parent::redrawContent();
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);
    }

}
