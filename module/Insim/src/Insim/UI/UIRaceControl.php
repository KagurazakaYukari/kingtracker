<?php

namespace Insim\UI;

use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Types\ButtonStyles;

class UIRaceControl extends UI {

    public $columns = array();
    public $rows = array();
    public $cars = array();
    public $filter = array();
    public $search = array();
    public $topActions = array();
    public $own_time = array();
    public $own_buttons = array();
    public $rows_buttons = array();
    public $rows_per_page = 22;
    public $items_per_page = 22;
    public $show_help = false;
    protected $button_id_timer = 0;
    protected $timer_init_value = 0;
    protected $timer = 0;
    protected $timer_sec_last = 0;
    public $destroyEvent = null;
    public $redrawTitle = false;
    public $delayedShow = false;
            
    function __construct($alias, PlayerClass &$player) {
        $id_start = 1;
        foreach ($player->ui->ui_list_perm as $key => $ui){
            if($ui->displayed){
                $id_start = $ui->id_end + 1;
            }
        }
        
        $id_end = $id_start + 3;
        
        parent::__construct($alias, $player, ($id_start), $id_end);
    }

    public function setData($countDown, $destroyEvent, $title = 'Race restarts', $delayedShow = false) {
        $this->width = 25;
        $this->top = 10;
        $this->left = (200 - $this->width) / 2;
        
        $this->timer = $countDown + 1;
        $this->timer_init_value = $this->timer;
        $this->destroyEvent = $destroyEvent;
        $this->title = $title;
        $this->delayedShow = $delayedShow;
    }

    public function update(){
        if (time() - $this->timer_sec_last >= 1) {
            $this->timer--;
            
            if(!$this->displayed && $this->delayedShow > 0 && $this->delayedShow == $this->timer){
                $this->show();
            }
            
            if($this->timer < 0 && $this->destroyEvent){
                call_user_func($this->destroyEvent);
            }else{
                if($this->displayed && $this->timer >= 0){
                    $this->redrawTimer();
                }else{
                    if($this->timer <= ($this->timer_init_value - 20)){
                        $this->show();
                    }
                }
            }
            
            $this->timer_sec_last = time();
        }
    }

    public function show() {

        $button = new isBTN();
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = 12;
        $button->W = $this->width;
        $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY;
        $button->Text = '';
        $this->send($button);
        
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = 4;
        $button->W = $this->width;
        $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY;
        $button->Text = \Insim\Types\MsgTypes::WHITE.$this->title;
        $this->button_id_title = $button->ReqI;
        $this->send($button);
        
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top + $button->H;
        $button->H = 8;
        $button->W = $this->width;
        $button->BStyle = ButtonStyles::COLOUR_LIGHT_GREY;
        $button->Text = \Insim\Types\MsgTypes::WHITE.\Insim\Helper\InSimHelper::secToString($this->timer);
        $this->button_id_timer = $button->ReqI;
        $this->send($button);
        
        $this->button_content_max = $this->id_current;

        parent::show();
    }

    public function redrawTimer(){
        if(!$this->button_id_timer)
            return;
        
        $button = new isBTN();
        $button->ReqI = $this->button_id_timer;
        $button->ClickID = $this->button_id_timer;
        $button->Text = \Insim\Types\MsgTypes::WHITE.\Insim\Helper\InSimHelper::secToString($this->timer);
        
        $this->send($button);
        
        if($this->redrawTitle){
            $this->redrawTitle();
        }
    }

    public function redrawTitle(){
        if(!$this->button_id_title)
            return;
        
        $button = new isBTN();
        $button->ReqI = $this->button_id_title;
        $button->ClickID = $this->button_id_title;
        $button->Text = \Insim\Types\MsgTypes::WHITE.$this->title;
        
        $this->send($button);
        
        $this->redrawTitle = false;
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);
    }
    
    public function close(){
        parent::close();
    }

}
