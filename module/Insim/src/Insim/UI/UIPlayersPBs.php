<?php

namespace Insim\UI;

use Insim\Model\LFSWPB;
use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Packets\isMTC;
use Insim\Types\ButtonStyles;
use Zend\Paginator\Adapter\Iterator as PaginatorIterator;
use Zend\Paginator\Paginator;

class UIPlayersPBs extends UI {

    public $columns = array();
    public $rows = array();
    public $cars = array();
    public $filter = array();
    public $search = array();
    public $topActions = array();
    public $own_time = array();
    public $own_buttons = array();
    public $rows_buttons = array();
    public $rows_per_page = 16;
    public $items_per_page = 16;
    public $content_left_offset = 0;
    public $show_help = false;

    function __construct($alias, PlayerClass &$player, $width = 0, $height = 0, $top = 0) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = (200 - $width) / 2;

        // columns
        $this->columns = array(
            'n' => array('width' => 6, 'name' => 'N', 'style' => ButtonStyles::ISB_RIGHT),
            'lap' => array('width' => 12, 'name' => 'Lap time', 'style' => ''),
            'name' => array('width' => 58, 'name' => 'Driver name', 'style' => ButtonStyles::ISB_LEFT),
            'splits' => array('width' => 44, 'name' => 'Splits', 'style' => ButtonStyles::ISB_LEFT),
        );

        // Own time
        $this->own_time = array(
            'n' => '---',
            'lap' => '^7---^8',
            'name' => 'PB time not found',
            'splits' => '',
        );

        // Top actions
        $this->topActions = array();

        // Cars filter
        $temp_state = array('id' => 0, 'state' => 0);
        $this->cars = array(
            'UF1' => $temp_state, 'XFG' => $temp_state, 'XRG' => $temp_state, 'LX4' => $temp_state, 'LX6' => $temp_state, 'RB4' => $temp_state,
            'FXO' => $temp_state, 'XRT' => $temp_state, 'RAC' => $temp_state, 'FZ5' => $temp_state, 'UFR' => $temp_state, 'XFR' => $temp_state, 'FXR' => $temp_state,
            'XRR' => $temp_state, 'FZR' => $temp_state, 'MRT' => $temp_state, 'FBM' => $temp_state, 'FOX' => $temp_state, 'FO8' => $temp_state, 'BF1' => $temp_state
        );

        parent::__construct($alias, $player);
    }

    public function setData() {
        // Clear rows
        $this->rows = array();

        for ($i = 0; $i < $this->rows_per_page; $i++) {
            $temp = array();
            foreach ($this->columns as $key => $col) {
                $temp[$key] = '';
            }
            $this->rows[] = $temp;
        }

        // Cars filter
        $aFilter = array(
            'track' => $this->player->host->TrackInfo->LFSWid
        );
        foreach ($this->cars as $carName => $value) {
            if (!empty($value['state']))
                $aFilter['car'][$carName] = $value['state'];
        }

        $conns = $this->player->playerService->getCurrentConnsSimple();
        $paginator = new Paginator(new PaginatorIterator($this->player->lapsService->LFSWService->fetchBestPBs($aFilter, $conns)));
        $paginator->setCurrentPageNumber($this->current_page)
                ->setItemCountPerPage($this->items_per_page)
                ->setPageRange(8);

        $this->max_page = count($paginator);
        $this->status_line = $this->current_page . ' / ' . $this->max_page;

        $index = 0;
        foreach ($paginator as $pb) {
            $temp_data = $pb;
            $pb = new LFSWPB();
            $pb->exchangeArray($temp_data);

            $number = (($this->current_page - 1) * $this->items_per_page) + ($index + 1);

            $this->rows[$index] = array(
                'n' => $number . '.',
                'lap' => '^7' . $pb->time_string . '^8',
                'name' => "^8[{$pb->car}] {$pb->PName} ^8(Laps: {$pb->lapcount})",
                'splits' => !empty($pb->splits) ? implode('  |  ', $pb->splits_string) : '',
            );
            $index++;
        }

        // Own time
        $aFilter['UName'] = $this->player->UName;
        $ownTime = $this->player->lapsService->LFSWService->fetchBestPBs($aFilter);

        if ($ownTime) {
            $this->own_time = array(
                'n' => '',
                'lap' => '^7' . $ownTime->time_string . '^8',
                'name' => "^8[{$ownTime->car}] {$ownTime->PName} ^8(Laps: {$ownTime->lapcount})",
                'splits' => !empty($ownTime->splits) ? implode('  |  ', $ownTime->splits_string) : '',
            );
        } else {
            $this->own_time = array(
                'n' => '---',
                'lap' => '^7---^8',
                'name' => 'PB time not found',
                'splits' => '',
            );
        }
    }

    public function show($showBase = true) {
        $this->setData();

        if ($showBase)
            $this->showBase();

        $this->button_content_min = $this->id_current + 1;

        $this->showTopActions();

        // Filter cars bg
        $button = new isBTN();
        $cars_pos_top = $this->top + 2 + 12 + 2 + 80;
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $cars_pos_top;
        $button->H = 8;
        $button->W = $this->width - 2;
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);


        $this->showFooter();

        // Cars Filter
        $width = ceil(($this->width - 2) / (count($this->cars) / 2));
        $index = 0;

        foreach ($this->cars as $car => $value) {
            if ($value['state'] == -1) {
                $color = UI::COL_DISABLED;
            } elseif ($value['state'] == 1) {
                $color = UI::COL_ENABLED;
            } else {
                $color = UI::COL_NEUTRAL;
            }

            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = !$index ? $this->left + 1 : $button->L + $button->W;
            if ($index == ceil((count($this->cars) / 2)))
                $button->L = $this->left + 1;
            $button->T = $index >= ceil((count($this->cars) / 2)) ? $cars_pos_top + 4 : $cars_pos_top;
            $button->H = 4;
            $button->W = $width;
            $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::ISB_CLICK;
            $button->Text = $color . $car;

            $this->send($button);

            $this->cars[$car]['id'] = $button->ClickID;

            $index++;
        }

        // Times
        $newLine = 0;
        foreach ($this->rows as $key => $row) {
            $rowIndex = 0;
            foreach ($this->columns as $keyCol => $column) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = !$rowIndex ? ($this->left + $this->content_left_offset) + 1 : $button->L + $button->W;
                $button->T = $this->top + 2 + 12 + $newLine;
                $button->H = 5;
                $button->W = $column['width'];
                $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
                $button->Text = isset($row[$keyCol]) ? $row[$keyCol] : '';

                $this->send($button);
                $this->rows_buttons[$key][$keyCol] = $button->ClickID;
                $rowIndex++;
            }
            $newLine += 5;
        }

        // Own time
        $newLine += 5;
        $rowIndex = 0;
        foreach ($this->columns as $keyCol => $column) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = !$rowIndex ? ($this->left + $this->content_left_offset) + 1 : $button->L + $button->W;
            $button->T = $this->top + 2 + 12 + $newLine;
            $button->H = 5;
            $button->W = $column['width'];
            $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
            $button->Text = $this->own_time[$keyCol];

            $this->send($button);
            $this->own_buttons[$keyCol] = $button->ClickID;
            //$this->rows_buttons[$key][$keyCol] = $button->ClickID;
            $rowIndex++;
        }
        $newLine += 5;

        $this->button_content_max = $this->id_current;

        parent::show();
    }

    public function showTopActions() {
        $button = new isBTN();

        $left = $this->left + 3;
        foreach ($this->topActions as $key => $action) {
            $style = isset($action['style']) ? $action['style'] : 0;

            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $left;
            $button->T = $this->top + 2 + 6;
            $button->H = 4;
            $button->W = $action['width'];
            $button->BStyle = ButtonStyles::COLOUR_LIGHT_GREY + ButtonStyles::ISB_CLICK + $style;
            $button->Text = $action['title'];
            if (isset($action['typein'])) {
                $button->TypeIn = $action['typein'];
            }

            $this->send($button);

            $left += $button->W + 1;
            $this->topActions[$key]['id'] = $button->ClickID;
        }
    }

    public function redrawContent() {
        $this->setData();

        $button = new isBTN();
        $button->ClickID = $this->button_id_status_line;
        $button->Text = $this->status_line;
        $this->send($button);

        foreach ($this->rows as $row_id => $row) {
            $buttons = $this->rows_buttons[$row_id];

            foreach ($buttons as $key => $id) {
                $button->ClickID = $id;
                $button->Text = $this->rows[$row_id][$key];
                $this->send($button);
            }
        }

        parent::redrawContent();
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);

        // car filter click
        foreach ($this->cars as $carName => $value) {
            if ($value['id'] == $packet->ClickID) {
                $this->setCarFilterButton($packet, $carName);
                return;
            }
        }

        // filter click
        foreach ($this->filter as $sectionName => $filters) {
            if ($filters['id'] == $packet->ClickID) {
                $this->clearFilterGroup($packet, $sectionName);
                return;
            }

            foreach ($filters['values'] as $filterName => $filter) {
                if ($filter['id'] == $packet->ClickID) {
                    $this->setFilter($packet, $sectionName, $filterName);
                    return;
                }
            }
        }

        // top action click
        foreach ($this->topActions as $key => $action) {
            if ($action['id'] == $packet->ClickID && !isset($action['typein'])) {
                $this->{$action['callback']}();
                return;
            }
        }

        // Lap time copy
        if ($packet->CFlags == isBTC::ISB_RMB) {
            foreach ($this->rows_buttons as $row_id => $buttons) {
                foreach ($buttons as $buttonID) {
                    if ($buttonID == $packet->ClickID) {
                        $this->actionCopyTime($this->rows[$row_id]);
                        return;
                    }
                }
            }
        }
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);

        foreach ($this->topActions as $key => $action) {
            if ($action['id'] == $packet->ClickID && @$action['typein'] > 0) {
                $this->{$action['callback']}($packet);
                return;
            }
        }

        //$this->topActions[$key]['id']
    }

    public function setCarFilterButton(isBTC $packet, $car) {
        $this->current_page = 1;
        $fltr = $this->cars[$car];

        $button = new isBTN();

        if (!$packet->isCTRL()) {
            foreach ($this->cars as $carName => $value) {
                $this->cars[$carName]['state'] = false;
                $button->ClickID = $value['id'];
                $button->Text = $carName;
                $this->send($button);
            }
        }

        $color = '';
        if ($packet->isRMB()) {
            if ($fltr['state'] > -1) {
                $fltr['state'] = -1;
                $color = UI::COL_DISABLED;
            } else {
                $fltr['state'] = 0;
                $color = UI::COL_NEUTRAL;
            }
        } elseif ($packet->isLMB()) {
            if ($fltr['state'] < 1) {
                $fltr['state'] = 1;
                $color = UI::COL_ENABLED;
            } else {
                $fltr['state'] = 0;
                $color = UI::COL_NEUTRAL;
            }
        }

        $this->cars[$car]['state'] = $fltr['state'];
        $button->ClickID = $fltr['id'];
        $button->Text = $color . $car;
        $this->send($button);

        $this->redrawContent();
        $this->updateOwnTime();
    }

    public function setCarFilter($car) {
        $this->current_page = 1;
        $fltr = $this->cars[$car];

        $button = new isBTN();

        // Clear all
        foreach ($this->cars as $carName => $value) {
            $this->cars[$carName]['state'] = false;
            $button->ClickID = $value['id'];
            $button->Text = $carName;
            $this->send($button);
        }

        $fltr['state'] = 1;
        $color = UI::COL_ENABLED;

        $this->cars[$car]['state'] = $fltr['state'];
        $button->ClickID = $fltr['id'];
        $button->Text = $color . $car;
        $this->send($button);

        $this->redrawContent();
        $this->updateOwnTime();
    }

    public function updateOwnTime() {
        $aFilter = array();

        foreach ($this->cars as $carName => $value) {
            if (!empty($value['state']))
                $aFilter['car'][$carName] = $value['state'];
        }

        foreach ($this->filter as $sectionName => $filters) {
            foreach ($filters['values'] as $filterName => $filter) {
                if (!empty($filter['state']))
                    $aFilter[$sectionName][$filter['key']] = $filter['state'];
            }
        }

        // Own time
        $aFilter['track'] = $this->player->host->TrackInfo->LFSWid;
        $aFilter['UName'] = $this->player->UName;
        $pb = $this->player->lapsService->LFSWService->fetchBestPBs($aFilter);

        if ($pb) {
            $this->own_time = array(
                'n' => '',
                'lap' => '^7' . $pb->time_string . '^8',
                'name' => "^8[{$pb->car}] {$pb->PName} ^8(Laps: {$pb->lapcount})",
                'splits' => !empty($pb->splits) ? implode('  |  ', $pb->splits_string) : '',
            );
        } else {
            $this->own_time = array(
                'n' => '---',
                'lap' => '^7---^8',
                'name' => 'PB time not found',
                'splits' => '',
            );
        }

        $button = new isBTN();
        foreach ($this->own_time as $key => $value) {
            $button->ClickID = $this->own_buttons[$key];
            $button->Text = $value;
            $this->send($button);
        }
    }

    public function actionCopyTime($data) {
        if (empty($data)) {
            return;
        }

        $text = "^7{$data['n']} {$data['name']} - {$data['lap']} (" . (str_replace('  |  ', ' | ', $data['splits'])) . ")";
        $msg = new isMTC();
        $msg->UCID = 255;
        $msg->Text = $text;
        $this->send($msg);
    }

}
