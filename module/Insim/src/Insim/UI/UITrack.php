<?php

namespace Insim\UI;

use Insim\Model\LapTop;
use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Packets\isMTC;
use Insim\Types\ButtonStyles;
use Zend\Debug\Debug;
use Zend\Paginator\Adapter\Iterator as PaginatorIterator;
use Zend\Paginator\Paginator;

class UITrack extends UI {

    public $columns = array();
    public $columns_times = array();
    public $rows = array();
    public $cars = array();
    public $filter = array();
    public $search = array();
    public $topActions = array();
    public $own_time = array();
    public $own_buttons = array();
    public $rows_buttons = array();
    public $rows_per_page = 20;
    public $items_per_page = 20;
    public $show_help = false;
    protected $data = array();

    function __construct($alias, PlayerClass &$player, $width = 122, $height = 101, $top = 30) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = (200 - $width) / 2;
        $this->content_left_offset = 45;

        // columns
        $this->columns = array(
            'line' => array('width' => $this->width - 2, 'name' => 'Line', 'style' => ButtonStyles::ISB_LEFT),
        );

        $this->columns_times = array(
            'car' => array('width' => 5, 'name' => 'car', 'style' => ''),
            'name' => array('width' => 30, 'name' => 'Driver name', 'style' => ButtonStyles::ISB_LEFT),
            'lap' => array('width' => 10, 'name' => 'Lap time', 'style' => ''),
            'splits' => array('width' => 30, 'name' => 'Splits', 'style' => ButtonStyles::ISB_LEFT),
        );

        $this->data = array(
            'track' => array(
                'title' => $player->translator->translateLFS('TRACK_TRACK'),
                'data' => array(
                    'name' => array('id' => 0, 'title' => "{$player->translator->translateLFS('TRACK_NAME')}:", 'value' => ''),
                    'circuit' => array('id' => 0, 'title' => "{$player->translator->translateLFS('CIRCUIT')}:", 'value' => ''),
                    'layout' => array('id' => 0, 'title' => "{$player->translator->translateLFS('LAYOUT')}:", 'value' => ''),
                    'grid' => array('id' => 0, 'title' => "{$player->translator->translateLFS('GRID_SIZE')}:", 'value' => ''),
                    'lenght' => array('id' => 0, 'title' => "{$player->translator->translateLFS('LENGHT')}:", 'value' => ''),
                    //'elevation' => array('id' => 0, 'title' => 'elevation:', 'value' => ''),
                    'weather' => array('id' => 0, 'title' => "{$player->translator->translateLFS('WEATHER')}:", 'value' => ''),
                    'wind' => array('id' => 0, 'title' => "{$player->translator->translateLFS('WIND')}:", 'value' => ''),
                )
            ),
            'race' => array(
                'title' => $player->translator->translateLFS('TRACK_HOST'),
                'data' => array(
                    'state' => array('id' => 0, 'title' => 'state:', 'value' => '---'),
                    'timing' => array('id' => 0, 'title' => 'timing:', 'value' => '---'),
                    'mustpit' => array('id' => 0, 'title' => 'must pit:', 'value' => '---'),
                    'forceview' => array('id' => 0, 'title' => 'force in car view:', 'value' => '---'),
                    'canvote' => array('id' => 0, 'title' => 'can kick/ban:', 'value' => '---'),
                    'canreset' => array('id' => 0, 'title' => 'car reset:', 'value' => '---'),
                )
            ),
        );

        parent::__construct($alias, $player);
    }

    public function setData($data) {
        $this->rows = array();

        for ($i = 0; $i < $this->rows_per_page; $i++) {
            $temp = array();
            foreach ($this->columns_times as $key => $col) {
                $temp[$key] = '';
            }
            $this->rows[] = $temp;
        }

        if (count($data['wrs']) > 1) {
            $index = 0;
            foreach ($data['wrs'] as $wr) {
                $this->rows[$index] = array(
                    'car' => $wr->car,
                    'lap' => '^7' . $wr->time_string . '^8',
                    'name' => '^7' . $wr->UName . ' ^8(' . (date('d.m.Y', $wr->timestamp)) . ')',
                    'splits' => implode('  |  ', $wr->splits_string),
                );
                $index++;
            }
        }

        if ($data['track']) {
            $track = $data['track'];
            $this->data['track']['data']['name']['value'] = $track->track;
            $this->data['track']['data']['circuit']['value'] = $track->circuit . ($track->isOpen ? ' Open' : '') . ($track->isReversed ? ' Rev.' : '') . ' (' . $track->name . ')';
            $this->data['track']['data']['grid']['value'] = $track->grid;
            $this->data['track']['data']['lenght']['value'] = !empty($track->lenght) ? $track->lenght . ' km' : '';
            //$this->data['track']['data']['elevation']['value'] = $track->elevation;
            $this->data['track']['data']['weather']['value'] = $this->player->host->weatherToString();
            $this->data['track']['data']['wind']['value'] = $this->player->host->windToString();
        }

        $this->data['track']['data']['layout']['value'] = !empty($this->player->host->LName) ? $this->player->host->LName : 'none';
        
        $this->data['race']['data']['state']['value'] = $this->player->host->stateToString();
        $this->data['race']['data']['timing']['value'] = $this->player->host->timingToString();
        $this->data['race']['data']['mustpit']['value'] = @$this->player->host->config['mustpit'];
        $this->data['race']['data']['forceview']['value'] = @$this->player->host->config['fcv'];
        $this->data['race']['data']['canreset']['value'] = @$this->player->host->config['canreset'];
        $this->data['race']['data']['canvote']['value'] = @$this->player->host->config['vote'];
    }

    public function show($showBase = true) {
        if ($showBase)
            $this->showBase();

        $this->button_content_min = $this->id_current + 1;

        $button = new isBTN();
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 2 + 12;
        $button->H = $this->height - 13;
        $button->W = $this->width - 2;
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);

        $headerBtn = new isBTN();
        $headerBtn->W = 45;
        $headerBtn->H = 5;
        $headerBtn->L = $this->left + 1;
        $headerBtn->BStyle = ButtonStyles::COLOUR_LIGHT_GREY + ButtonStyles::ISB_LIGHT + ButtonStyles::ISB_LEFT;

        $dataBtn = new isBTN();
        $dataBtn->W = 20;
        $dataBtn->H = 4;
        $dataBtn->L = $this->left;
        $dataBtn->BStyle = ButtonStyles::COLOUR_LIGHT_GREY + ButtonStyles::ISB_RIGHT;

        $valueBtn = new isBTN();
        $valueBtn->W = 25;
        $valueBtn->H = 4;
        $valueBtn->L = $this->left + $dataBtn->W;
        $valueBtn->BStyle = ButtonStyles::COLOUR_LIGHT_GREY + ButtonStyles::ISB_LEFT;

        $newLine = 0;
        foreach ($this->data as $sKey => $section) {
            $headerBtn->ReqI = ++$this->id_current;
            $headerBtn->ClickID = $headerBtn->ReqI;
            $headerBtn->T = $this->top + 14 + $newLine;
            $headerBtn->Text = '^7' . $section['title'];
            $this->send($headerBtn);
            $this->data[$sKey]['id'] = $headerBtn->ClickID;

            $newLine += 6;

            foreach ($section['data'] as $dKey => $data) {
                $dataBtn->ReqI = ++$this->id_current;
                $dataBtn->ClickID = $dataBtn->ReqI;
                $dataBtn->T = $this->top + 14 + $newLine;
                $dataBtn->Text = $data['title'];
                $this->send($dataBtn);

                $valueBtn->ReqI = ++$this->id_current;
                $valueBtn->ClickID = $valueBtn->ReqI;
                $valueBtn->T = $this->top + 14 + $newLine;
                $valueBtn->Text = '^7' . $data['value'];
                $this->send($valueBtn);
                $this->data[$sKey]['data'][$dKey]['id'] = $valueBtn->ClickID;

                $newLine += 4;
            }

            $newLine += 4;
        }

        // WRs
        $newLine = 0;
        $headerBtn->W = $this->width - 2 - 45;
        $headerBtn->H = 5;
        $headerBtn->L = $this->left + 1 + 45;
        $headerBtn->BStyle = ButtonStyles::COLOUR_LIGHT_GREY + ButtonStyles::ISB_LIGHT + ButtonStyles::ISB_LEFT;
        $headerBtn->ReqI = ++$this->id_current;
        $headerBtn->ClickID = $headerBtn->ReqI;
        $headerBtn->T = $this->top + 14 + $newLine;
        $headerBtn->Text = '^7'.$this->player->translator->translateLFS('TRACK_WRS');
        $this->send($headerBtn);

        // Times
        $newLine = 0;
        foreach ($this->rows as $key => $row) {
            $rowIndex = 0;
            foreach ($this->columns_times as $keyCol => $column) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = !$rowIndex ? ($this->left + $this->content_left_offset) + 1 : $button->L + $button->W;
                $button->T = $this->top + 2 + 17 + $newLine;
                $button->H = 4;
                $button->W = $column['width'];
                $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
                $button->Text = isset($row[$keyCol]) ? $row[$keyCol] : '';

                $this->send($button);
                $this->rows_buttons[$key][$keyCol] = $button->ClickID;
                $rowIndex++;
            }
            $newLine += 4;
        }

        //Debug::dump($this->data);

        $this->showFooter(-2);

        $this->button_content_max = $this->id_current;

        parent::show();
    }

    public function updateWR($data) {
        $this->rows = array();

        for ($i = 0; $i < $this->rows_per_page; $i++) {
            $temp = array();
            foreach ($this->columns_times as $key => $col) {
                $temp[$key] = '';
            }
            $this->rows[] = $temp;
        }

        if (count($data['wrs']) > 1) {
            $index = 0;
            foreach ($data['wrs'] as $wr) {
                $this->rows[$index] = array(
                    'car' => $wr->car,
                    'lap' => '^7' . $wr->time_string . '^8',
                    'name' => '^7' . $wr->UName . ' ^8(' . (date('d.m.Y', $wr->timestamp)) . ')',
                    'splits' => implode('  |  ', $wr->splits_string),
                );
                $index++;
            }
        }

        $button = new isBTN();
        foreach ($this->rows as $row_id => $row) {
            $buttons = $this->rows_buttons[$row_id];

            foreach ($buttons as $key => $id) {
                $button->ClickID = $id;
                $button->Text = $this->rows[$row_id][$key];
                $this->send($button);
            }
        }
    }

    public function redrawContent() {
        $this->setData();

        $button = new isBTN();
        $button->ClickID = $this->button_id_status_line;
        $button->Text = $this->status_line;
        $this->send($button);

        foreach ($this->rows as $row_id => $row) {
            $buttons = $this->rows_buttons[$row_id];

            foreach ($buttons as $key => $id) {
                $button->ClickID = $id;
                $button->Text = $this->rows[$row_id][$key];
                $this->send($button);
            }
        }

        parent::redrawContent();
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);
    }

}
