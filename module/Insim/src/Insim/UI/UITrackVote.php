<?php

namespace Insim\UI;

use Insim\Model\PlayerClass;
use Insim\Packets\isBFN;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Types\ButtonStyles;
use Insim\Types\MsgTypes;
use Zend\Debug\Debug;

class UITrackVote extends UI {

    public $columns = array();
    public $rows = array();
    public $rows_buttons = array();
    public $rows_per_page = 7;
    public $items_per_page = 7;
    public $show_help = false;
    protected $backgound = null;
    protected $timer_init_value = 0;
    protected $timer = 0;
    protected $timer_sec_last = 0;
    public $callback = null;
    public $tracks = array();

    function __construct($alias, PlayerClass &$player, $width = 0, $height = 0, $top = 0) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = 30;

        // columns
        $this->columns = array(
            'name' => array('style' => ButtonStyles::ISB_CLICK + ButtonStyles::ISB_LEFT),
            'votes' => array('style' => ButtonStyles::ISB_LIGHT),
        );

        parent::__construct($alias, $player);
    }

    public function delayedShow($secs) {
        $this->timer = $secs + 1;
        $this->timer_init_value = $this->timer;
    }

    public function setTracks($tracks) {
        $this->tracks = $tracks;
    }

    public function showTrackVote() {
        $cols = 1;
        $count = count($this->tracks);
        $lineHeight = 10;
        $this->height = ((ceil($count / $cols)) * $lineHeight) + 5 + 4;

        // Show base
        $this->title = $this->player->translator->translateLFS('VOTE_TRACK_NOW');
        $this->show(false);

        //Debug::dump($tracks);

        $this->button_content_min = $this->id_current + 1;

        $newLine = 0;
        $rowIndex = 1;
        $buttonWidth = floor(($this->width - 2) / $cols);
        $left = $this->left + 1;

        $this->rows = array();

        foreach ($this->tracks as $track) {
            $temp = array();
            foreach ($this->columns as $key => $col) {
                switch ($key) {
                    case 'name': $temp[$key] = array('id' => 0, 'value' => $track['name']);
                        break;
                    case 'votes': $temp[$key] = array('id' => 0, 'value' => $track['votes']);
                        break;
                }
            }
            $this->rows[] = $temp;
        }

        $button = new isBTN();

        foreach ($this->rows as $key => $row) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $left;
            $button->T = $this->top + 2 + 5 + $newLine;
            $button->H = $lineHeight;
            $button->W = $buttonWidth;
            $button->BStyle = ButtonStyles::ISB_DARK;
            $button->Text = '';

            if ($rowIndex % $cols == 0) {
                $button->W += (($this->width - 2) - ($buttonWidth * $cols));
            }

            $this->send($button);

            foreach ($this->columns as $keyCol => $column) {
                switch ($keyCol) {
                    case 'name':
                        $colWidth = $button->W - 15;
                        break;
                    case 'votes':
                        $colWidth = 15;
                        break;
                }

                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = $left;
                $button->T = $this->top + 2 + 5 + $newLine;
                $button->H = $lineHeight;
                $button->W = $colWidth;
                $button->BStyle = $column['style'];
                $button->Text = MsgTypes::WHITE . $row[$keyCol]['value'];
                $this->send($button);
                $this->rows[$key][$keyCol]['id'] = $button->ClickID;

                $left += $button->W;
            }

            if ($rowIndex % $cols == 0) {
                $left = $this->left + 1;
                $newLine += $lineHeight;
                $button->W += (($this->width - 2) - ($buttonWidth * $cols));
            }

            $rowIndex++;
        }

        $this->button_content_max = $this->id_current;
    }

    public function showCircuitVote($tracks) {
        $this->tracks = $tracks;
        $cols = 2;
        $count = count($this->tracks);
        $lineHeight = 5;
        $this->height = ((ceil($count / $cols)) * $lineHeight) + 5 + 4;

        // Update bg height
        $bfn = new isBFN();
        $bfn->SubT = isBFN::BFN_DEL_BTN;
        $bfn->UCID = $this->player->UCID;
        $bfn->ClickID = $this->backgound->ClickID;
        $this->player->host->insim->send($bfn);
        $this->backgound->H = $this->height;
        $this->send($this->backgound);

        // Update title
        $this->title = $this->player->translator->translateLFS('VOTE_CIRCUIT_NOW');
        $button = new isBTN();
        $button->ReqI = $this->button_id_title;
        $button->ClickID = $this->button_id_title;
        //$button->BStyle = ButtonStyles::COLOUR_YELLOW;
        $button->Text = $this->title;
        $this->send($button);

        // Clear old buttons
        $bfn->SubT = isBFN::BFN_DEL_BTN;
        $bfn->UCID = $this->player->UCID;
        $bfn->ClickID = $this->button_content_min;
        $bfn->ClickMax = $this->button_content_max;
        $this->player->host->insim->send($bfn);

        // Reset button ID counter
        $this->id_current = $this->button_content_min - 1;
        $this->button_content_min = $this->id_current + 1;

        $newLine = 0;
        $rowIndex = 1;
        $buttonWidth = floor(($this->width - 2) / $cols);
        $left = $this->left + 1;

        $this->rows = array();

        foreach ($this->tracks as $track) {
            $temp = array();
            foreach ($this->columns as $key => $col) {
                switch ($key) {
                    case 'name': $temp[$key] = array('id' => 0, 'value' => $track['name'], 'long_name' => $track['long_name']);
                        break;
                    case 'votes': $temp[$key] = array('id' => 0, 'value' => $track['votes'], 'long_name' => $track['long_name']);
                        break;
                }
            }
            $this->rows[] = $temp;
        }

        foreach ($this->rows as $key => $row) {
            if ($row['name']['long_name'] != $row['name']['value']) {
                $text = "{$row['name']['long_name']} ({$row['name']['value']})";
            } else {
                $text = $row['name']['value'];
            }

            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $left;
            $button->T = $this->top + 2 + 5 + $newLine;
            $button->H = $lineHeight;
            $button->W = $buttonWidth - 7;
            $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::ISB_LEFT + ButtonStyles::ISB_CLICK;
            $button->Text = MsgTypes::WHITE . $text;
            $this->rows[$key]['name']['id'] = $button->ClickID;
            
            if ($rowIndex % $cols == 0) {
                $button->W += (($this->width - 2) - ($buttonWidth * $cols));
            }

            $this->send($button);

            foreach ($this->columns as $keyCol => $column) {
                $btnHeight = $lineHeight;
                switch ($keyCol) {
                    case 'name':
                        $colWidth = $button->W - 7;
                        //$btnHeight -= 3;
                        if ($row[$keyCol]['long_name'] != $row[$keyCol]['value']) {
                            $text = "{$row[$keyCol]['long_name']} ({$row[$keyCol]['value']})";
                        } else {
                            $text = $row[$keyCol]['value'];
                        }

                        break;
                    case 'votes':
                        $colWidth = 7;
                        $text = $row[$keyCol]['value'];
                        break;
                }

                if ($keyCol == 'votes') {
                    $button->ReqI = ++$this->id_current;
                    $button->ClickID = $button->ReqI;
                    $button->L = $left;
                    $button->T = $this->top + 2 + 5 + $newLine;
                    $button->H = $btnHeight;
                    $button->W = 7;
                    $button->BStyle = $column['style'];
                    $button->Text = MsgTypes::WHITE . $text;
                    $this->send($button);
                    $this->rows[$key][$keyCol]['id'] = $button->ClickID;
                }

                $left += $button->W;
            }

            if ($rowIndex % $cols == 0) {
                $left = $this->left + 1;
                $newLine += $lineHeight;
                $button->W += (($this->width - 2) - ($buttonWidth * $cols));
            }

            $rowIndex++;
        }

        $this->button_content_max = $this->id_current;
    }

    public function show($showBase = true) {
        if ($showBase)
            $this->showBase();

        // Background
        $button = new isBTN();
        $button->ReqI = $this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = $this->height;
        $button->W = $this->width;
        $button->BStyle = ButtonStyles::ISB_LIGHT;
        $button->Text = '';
        $this->backgound = clone $button;
        $this->send($button);

        // Title
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top + 1;
        $button->H = 5;
        $button->W = $this->width;
        $button->BStyle = ButtonStyles::COLOUR_YELLOW;
        $button->Text = $this->title;
        $this->button_id_title = $button->ClickID;
        $this->send($button);

        parent::show();
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);

        foreach ($this->rows as $key => $row) {
            if ($row['name']['id'] == $packet->ClickID) {
                $this->voteClick($row);
            }
        }
    }

    public function voteClick($row) {
        $this->player->host->hostService->rotationVoteClickEvent($row, $this->player);
    }

    public function update() {
        if (time() - $this->timer_sec_last >= 1 && $this->timer >= 0) {
            $this->timer--;

            if ($this->timer == 0) {
                $this->showTrackVote();
            }

            $this->timer_sec_last = time();
        }
    }

    public function updateVote($id, $votes) {
        foreach ($this->rows as $key => $row) {
            if ($row['name']['value'] == $id) {
                $this->rows[$key]['votes']['value'] = $votes;
                $buttonID = $this->rows[$key]['votes']['id'];

                $button = new isBTN();
                $button->ReqI = $buttonID;
                $button->ClickID = $buttonID;
                $button->Text = MsgTypes::WHITE . $votes;

                $this->send($button);
            }
        }
    }

    public function trackVoteDone() {
        
    }

}
