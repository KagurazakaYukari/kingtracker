<?php

namespace Insim\UI\Elements;

use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Types\ButtonStyles;
use Zend\Debug\Debug;

class BtnLabel extends Element {

    public $current;
    public $values;
    public $values_count = 0;
    public $button_id_value = 0;
    public $key_value = -1;

    function __construct(PlayerClass $player, $label = '', $height = 5, $top = 0, $left = 0) {
        parent::__construct($player, $label, $height, $top, $left);
    }

    function show($id_current, $top = 0, $left = 0) {
        parent::show($id_current, $top, $left);

        $button = new isBTN();

        $button->ReqI = ++$id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top;
        $button->H = $this->height;
        $button->W = 40;
        $button->BStyle = ButtonStyles::ISB_RIGHT;
        $button->Text = '^7' . $this->label;

        $this->addButton($button);
        $this->send($button);

        $lft = $this->left + 1 + $button->W;

        if (count($this->values) > 0) {
            foreach ($this->values as $value) {
                $button->ReqI = ++$id_current;
                $button->ClickID = $button->ReqI;
                $button->L = $lft;
                $button->T = $this->top;
                $button->H = $this->height;
                $button->W = 10;
                $button->BStyle = 0;
                $button->Text = $value['value'];

                $this->button_id_value = $button->ReqI;
                $this->addButton($button);
                $this->send($button);

                $lft += $button->W;
            }
        }

        $this->end_id = $id_current;
    }

    function setValues($values) {
        $index = 0;
        foreach ($values as $key => $value) {
            $this->values[$index] = array(
                'key' => $key,
                'value' => $value
            );

            $index++;
        }

        $this->values_count = count($values);
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);
    }

}
