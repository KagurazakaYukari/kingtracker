<?php

namespace Insim\UI;

use Insim\Model\HostConn;
use Insim\Model\Player;
use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Types\ButtonStyles;
use Insim\Types\InSimLang;
use Zend\Debug\Debug;
use Zend\Paginator\Adapter\Iterator as PaginatorIterator;
use Zend\Paginator\Paginator;

class UIPlayersList extends UI {

    public $columns = array();
    public $rows = array();
    public $rows_buttons = array();
    public $rows_per_page = 18;
    public $items_per_page = 18;
    public $show_help = false;

    function __construct($alias, PlayerClass &$player, $width = 0, $height = 0, $top = 0) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = (200 - $width) / 2;

        // columns
        $this->columns = array(
            //'view' => array('width' => 4, 'name' => 'Spec'),
            'name' => array('width' => 71, 'name' => 'Player name / Licence name / Team / Lang ...', 'style' => ButtonStyles::ISB_LEFT), //85
            //'car' => array('width' => 5, 'name' => 'Car'),
            'controller' => array('width' => 35, 'name' => 'Controller', 'style' => ButtonStyles::ISB_LEFT),
            'k' => array('width' => 6, 'name' => 'Kick'),
            'b' => array('width' => 5, 'name' => 'Ban', 'typein' => 5),
            'a' => array('width' => 3, 'name' => 'A', 'typein' => 5),
        );

        // Help text
        $helpText = '^7Help';

        foreach (preg_split("/((\r?\n)|(\r\n?))/", $helpText) as $line) {
            $this->help_lines[] = $line;
        }

        parent::__construct($alias, $player);
    }

    public function setData() {
        // Clear rowa
        $this->rows = array();

        for ($i = 0; $i < $this->rows_per_page; $i++) {
            $temp = array();
            foreach ($this->columns as $key => $col) {
                $temp[$key] = '';
            }
            $this->rows[] = $temp;
        }

        $paginator = new Paginator(new PaginatorIterator($this->player->playerService->getCurrentConns()));
        $paginator->setCurrentPageNumber($this->current_page)
                ->setItemCountPerPage($this->items_per_page)
                ->setPageRange(8);

        $this->max_page = count($paginator);
        $this->status_line = $this->current_page . ' / ' . $this->max_page;

        $index = 0;
        foreach ($paginator as $player) {
            $gmt = @$player->gmt ? ' (UTC' . $player->gmt . ')' : '';
            $country = $player->country;
            $lang = ' - ^7' . InSimLang::getLang($player->lang) . ' ^8';
            $car = $player->CName ? '^7' . $player->CName . ' ^8- ' : '';
            $role = ' ^8(^3' . $player->role . '^8)';

            $this->rows[$index] = array(
                'ucid' => $player->UCID,
                'UName' => $player->UName,
                'PName' => $player->PName,
                'plid' => $player->PLID ? $player->PLID : 0,
                'order' => trim($player->PName_utf),
                'view' => '',
                'name' => trim($player->PName) . ' ^8[' . $player->UName . ']' . $role . $lang . $country . $gmt,
                'country' => $player->country,
                'gmt' => @$player->gmt,
                'lang' => $lang,
                'role' => $role,
                'controller' => $car . (isset($player->Flags_string) ? $player->Flags_string : ''),
                'a' => 'A',
                'k' => 'Kick',
                'b' => 'Ban',
            );
            $index++;
        }
    }

    public function updatePlayersRemove($remove_row_id) {
        $to_update = array();

        foreach ($this->rows as $key => $row) {
            if (isset($row['ucid']) && $key >= $remove_row_id) {
                $to_update[] = $key;
            }
        }

        foreach ($to_update as $row_id) {
            if (isset($this->rows[$row_id + 1])) {
                $this->rows[$row_id] = $this->rows[$row_id + 1];
            } else {
                $temp = array();
                foreach ($this->columns as $key => $col) {
                    $temp[$key] = '';
                }
                $this->rows[$row_id] = $temp;
            }
        }

        foreach ($to_update as $row_id) {
            $buttons = $this->rows_buttons[$row_id];

            $button = new isBTN();
            foreach ($buttons as $key => $id) {
                $button->ClickID = $id;
                $button->Text = $this->rows[$row_id][$key];
                $this->send($button);
            }
        }
    }

    public function updatePlayersAdd($data) {
        $to_update = array();
        $names_order = array();

        foreach ($this->rows as $key => $row) {
            if (isset($row['ucid'])) {
                $names_order[$key] = $row['order'];
            }
        }

        $names_order[] = $data['PName_utf'];
        asort($names_order);

        $old_row_id = 0;
        foreach ($names_order as $new_row_id => $order_value) {
            $gmt = $data['gmt'] ? ' (UTC' . $data['gmt'] . ')' : '';
            $country = $data['country'];
            $lang = ' - ^7' . InSimLang::getLang($data['lang']) . ' ^8';
            $car = @$data['CName'] ? '^7' . $data['CName'] . ' ^8- ' : '';
            $role = ' ^8(^3' . $data['role'] . '^8)';

            if (isset($this->rows[$new_row_id]['ucid'])) {
                if (@$this->rows[$old_row_id]['ucid'] != @$this->rows[$new_row_id]['ucid'])
                    $to_update[$old_row_id] = $this->rows[$new_row_id];
            } else {
                $to_update[$old_row_id] = array(
                    'ucid' => $data['UCID'],
                    'UName' => $data['UName'],
                    'PName' => trim($data['PName']),
                    'order' => trim($data['PName_utf']),
                    'view' => '',
                    'name' => trim($data['PName']) . ' ^8[' . $data['UName'] . ']' . $role . $lang . $country . $gmt,
                    'lang' => $data['lang'],
                    'country' => $data['country'],
                    'gmt' => $data['gmt'],
                    'role' => $role,
                    'controller' => $car . '',
                    'a' => 'A',
                    'k' => 'Kick',
                    'b' => 'Ban',
                );
            }

            $old_row_id++;
        }

        //Debug::dump($to_update);

        foreach ($to_update as $row_id => $value) {
            $this->rows[$row_id] = $value;

            $buttons = $this->rows_buttons[$row_id];

            $button = new isBTN();
            foreach ($buttons as $key => $id) {
                $button->ClickID = $id;
                $button->Text = $value[$key];
                
                if (isset($this->columns[$key]['typein'])) {
                    $button->TypeIn = $this->columns[$key]['typein'];
                } else {
                    $button->TypeIn = false;
                }
                
                if($key == 'k' OR $key == 'b' OR $key == 'a'){
                    // Clear own action buttons
                    if($this->player->UCID == @$value['ucid']){
                        $button->Text = '';
                        $button->TypeIn = false;
                    }
                    
                    // Access rights
                    if($key == 'k' && !$this->player->acl->check('kick')){
                        $button->Text = '';
                        $button->TypeIn = false;
                    }
                    if($key == 'b' && !$this->player->acl->check('ban')){
                        $button->Text = '';
                        $button->TypeIn = false;
                    }
                    if($key == 'a' && !($this->player->acl->check('spec'))){
                        $button->Text = '';
                        $button->TypeIn = false;
                    }
                }
                
                $this->send($button);
            }
        }
    }

    public function updatePlayerName(HostConn $conn) {
        foreach ($this->rows as $row_id => $row) {
            if (isset($row['ucid']) && $row['ucid'] == $conn->UCID && $row_id <= $this->rows_per_page) {
                $gmt = @$row['gmt'] ? ' (UTC' . $row['gmt'] . ')' : '';
                $country = $row['country'];
                $lang = ' - ^7' . InSimLang::getLang($row['lang']) . ' ^8';
                $role = $row['role'];

                $this->rows[$row_id]['order'] = trim($conn->PName_utf);
                $this->rows[$row_id]['name'] = trim($conn->PName) . ' ^8[' . $conn->UName . ']' . $role . $lang . $country . $gmt;
                $this->rows[$row_id]['UName'] = $conn->UName;
                $this->rows[$row_id]['PName'] = trim($conn->PName);

                $buttons = $this->rows_buttons[$row_id];

                $button = new isBTN();
                foreach ($buttons as $key => $id) {
                    $button->ClickID = $id;
                    $button->Text = $this->rows[$row_id][$key];
                    $this->send($button);
                }
            }
        }
    }

    public function updatePlayersCountry(Player $player) {
        foreach ($this->rows as $row_id => $row) {
            if (isset($row['UName']) && $row['UName'] == $player->UName && $row_id <= $this->rows_per_page) {
                $gmt = $player->gmt ? ' (UTC' . $player->gmt . ')' : '';
                $country = $player->country;
                $lang = ' - ^7' . InSimLang::getLang($player->lang) . ' ^8';
                $role = $row['role'];

                $this->rows[$row_id]['name'] = $row['PName'] . ' ^8[' . $row['UName'] . ']' . $role . $lang . $country . $gmt;
                $this->rows[$row_id]['country'] = $player->country;
                $this->rows[$row_id]['gmt'] = $player->gmt;
                $this->rows[$row_id]['lang'] = $lang;

                $buttons = $this->rows_buttons[$row_id];

                $button = new isBTN();
                foreach ($buttons as $key => $id) {
                    $button->ClickID = $id;
                    $button->Text = $this->rows[$row_id][$key];
                    $this->send($button);
                }
            }
        }
    }

    public function updatePlayersCar($data = array()) {
        foreach ($this->rows as $row_id => $row) {
            if (isset($row['ucid']) && $row['ucid'] == $data['UCID'] && $row_id <= $this->rows_per_page) {
                $car = $data['CName'] ? '^7' . $data['CName'] . ' ^8- ' : '';

                $this->rows[$row_id]['plid'] = $data['PLID'];
                $this->rows[$row_id]['controller'] = $car . (isset($data['Flags_string']) ? $data['Flags_string'] : '');

                $buttons = $this->rows_buttons[$row_id];

                $button = new isBTN();
                foreach ($buttons as $key => $id) {
                    if (!$key == 'controller')
                        continue;

                    $button->ClickID = $id;
                    $button->Text = $this->rows[$row_id][$key];
                    $this->send($button);
                }
            }
        }
    }

    public function updatePlayersCarRemove($plid) {
        foreach ($this->rows as $row_id => $row) {
            if (isset($row['plid']) && $row['plid'] == $plid && $row_id <= $this->rows_per_page) {
                $this->rows[$row_id]['plid'] = 0;
                $this->rows[$row_id]['car'] = '';
                $this->rows[$row_id]['controller'] = '';

                $buttons = $this->rows_buttons[$row_id];

                $button = new isBTN();
                foreach ($buttons as $key => $id) {
                    if ($key != 'car')
                        continue;

                    $button->ClickID = $id;
                    $button->Text = $this->rows[$row_id][$key];
                    $this->send($button);
                }
            }
        }
    }

    public function show($showBase = true) {
        $this->setData();

        if ($showBase)
            $this->showBase();

        $this->button_content_min = $this->id_current + 1;
        $this->showFooter();

        $button = new isBTN();
        $newLine = 0;
        foreach ($this->rows as $key => $row) {
            $rowIndex = 0;
            foreach ($this->columns as $keyCol => $column) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = !$rowIndex ? $this->left + 1 : $button->L + $button->W;
                $button->T = $this->top + 2 + 12 + $newLine;
                $button->H = 5;
                $button->W = $column['width'];
                $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
                $button->Text = isset($row[$keyCol]) ? $row[$keyCol] : '';
                if (isset($column['typein'])) {
                    $button->TypeIn = $column['typein'];
                } else {
                    $button->TypeIn = false;
                }
                
                if($keyCol == 'k' OR $keyCol == 'b' OR $keyCol == 'a'){
                    // Clear own action buttons
                    if($this->player->UCID == @$row['ucid']){
                        $button->Text = '';
                        $button->TypeIn = false;
                    }
                    
                    // Access rights
                    if($keyCol == 'k' && !$this->player->acl->check('kick')){
                        $button->Text = '';
                        $button->TypeIn = false;
                    }
                    if($keyCol == 'b' && !$this->player->acl->check('ban')){
                        $button->Text = '';
                        $button->TypeIn = false;
                    }
                    if($keyCol == 'a' && !($this->player->acl->check('spec'))){
                        $button->Text = '';
                        $button->TypeIn = false;
                    }
                }
                
                $this->send($button);
                $this->rows_buttons[$key][$keyCol] = $button->ClickID;
                $rowIndex++;
            }
            $newLine += 5;
        }

        $this->button_content_max = $this->id_current;

        parent::show();
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);

        foreach ($this->rows_buttons as $row_id => $buttons) {
            foreach ($buttons as $keyCol => $buttonID) {
                if ($buttonID == $packet->ClickID) {
                    // Ignore own events OR empty rows
                    if (!isset($this->rows[$row_id]['ucid']) OR $packet->UCID == $this->rows[$row_id]['ucid'])
                        continue;

                    $eventInfo = array(
                        'by_id' => $this->player->player_id,
                        'by_ucid' => $this->player->UCID,
                        'UCID' => $this->rows[$row_id]['ucid'],
                        'PLID' => @$this->rows[$row_id]['plid'],
                        'UName' => $this->rows[$row_id]['UName'],
                    );
                    switch ($keyCol) {
                        case 'k':
                            if ($this->player->acl->check('kick')) {
                                $this->player->playerService->kick($eventInfo);
                            }
                            break;
                    }
                    return;
                }
            }
        }
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);

        foreach ($this->rows_buttons as $row_id => $buttons) {
            foreach ($buttons as $keyCol => $buttonID) {
                if ($buttonID == $packet->ClickID) {
                    // Ignore own events
                    if ($packet->UCID == $this->rows[$row_id]['ucid'])
                        continue;

                    $eventInfo = array(
                        'by_id' => $this->player->player_id,
                        'by_ucid' => $this->player->UCID,
                        'UCID' => $this->rows[$row_id]['ucid'],
                        'PLID' => $this->rows[$row_id]['plid'],
                        'UName' => $this->rows[$row_id]['UName'],
                        'typein' => strtolower(trim($packet->Text))
                    );
                    switch ($keyCol) {
                        case 'b':
                            if ($this->player->acl->check('ban')) {
                                $eventInfo['duration'] = intval(trim($eventInfo['typein']));
                                $this->player->playerService->ban($eventInfo);
                            }
                            break;
                        case 'a':
                            switch ($eventInfo['typein']) {
                                case 'spec':
                                case 's':
                                    if ($this->player->acl->check('spec')) {
                                        $this->player->playerService->spec($eventInfo);
                                    }
                                    break;
                                case 'dt':
                                    break;
                                case '30':
                                    break;
                            }
                            break;
                    }
                    return;
                }
            }
        }
    }

    public function redrawContent() {
        $this->setData();
        
        $button = new isBTN();
        $button->ClickID = $this->button_id_status_line;
        $button->Text = $this->status_line;
        $this->send($button);
        
        foreach ($this->rows as $row_id => $row) {
            $buttons = $this->rows_buttons[$row_id];

            foreach ($buttons as $key => $id) {
                $button->ClickID = $id;
                $button->Text = $this->rows[$row_id][$key];
                $this->send($button);
            }
        }

        parent::redrawContent();
    }

    public function close() {
        parent::close();
    }

}
