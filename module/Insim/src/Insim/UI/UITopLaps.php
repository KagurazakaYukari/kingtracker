<?php

namespace Insim\UI;

use Insim\Model\LapTop;
use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Packets\isMTC;
use Insim\Types\ButtonStyles;
use Zend\Debug\Debug;
use Zend\Paginator\Adapter\Iterator as PaginatorIterator;
use Zend\Paginator\Paginator;

class UITopLaps extends UI {

    public $columns = array();
    public $rows = array();
    public $cars = array();
    public $filter = array();
    public $search = array();
    public $topActions = array();
    public $own_time = array();
    public $own_buttons = array();
    public $rows_buttons = array();
    public $rows_per_page = 16;
    public $items_per_page = 16;
    public $content_left_offset = 15;
    public $show_help = true;

    function __construct($alias, PlayerClass &$player, $width = 0, $height = 0, $top = 0) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = (200 - $width) / 2;

        // columns
        $this->columns = array(
            'n' => array('width' => 8, 'name' => 'N', 'style' => ButtonStyles::ISB_RIGHT),
            'lap' => array('width' => 12, 'name' => 'Lap time', 'style' => ''),
            'name' => array('width' => 37, 'name' => 'Driver name', 'style' => ButtonStyles::ISB_LEFT),
            'splits' => array('width' => 47, 'name' => 'Splits', 'style' => ButtonStyles::ISB_LEFT),
        );

        // Own time
        $this->own_time = array(
            'n' => '---',
            'lap' => '^7---^8',
            'name' => $player->translator->translateLFS('PB_NOT_FOUND'),
            'splits' => '',
        );
        
        // Top actions
        $this->topActions = array(
            'all' => array('id' => 0, 'state' => true, 'title' => $player->translator->translateLFS('ALL_PLAYERS'), 'width' => 10, 'callback' => 'actionShowAllPlayers', 'style' => ButtonStyles::ISB_LEFT),
            //'team' => array('id' => 0, 'state' => false, 'title' => 'Team', 'width' => 6, 'callback' => 'actionShowTeam', 'style' => ButtonStyles::ISB_LEFT),
            'search' => array('id' => 0, 'state' => false, 'title' => $player->translator->translateLFS('SEARCH'), 'width' => 8, 'callback' => 'actionSearch', 'style' => ButtonStyles::ISB_LEFT, 'typein' => 60),
            'layout' => array('id' => 0, 'state' => false, 'title' => $player->translator->translateLFS('TOP_LAYOUT').': '.(!empty($player->host->LName) ? $player->host->LName : $player->translator->translateLFS('NONE')), 'width' => 50, 'callback' => 'actionLayout', 'style' => ButtonStyles::ISB_LEFT),
        );

        // Cars filter
        $temp_state = array('id' => 0, 'state' => 0);
        $this->cars = array(
            'UF1' => $temp_state, 'XFG' => $temp_state, 'XRG' => $temp_state, 'LX4' => $temp_state, 'LX6' => $temp_state, 'RB4' => $temp_state,
            'FXO' => $temp_state, 'XRT' => $temp_state, 'RAC' => $temp_state, 'FZ5' => $temp_state, 'UFR' => $temp_state, 'XFR' => $temp_state, 'FXR' => $temp_state,
            'XRR' => $temp_state, 'FZR' => $temp_state, 'MRT' => $temp_state, 'FBM' => $temp_state, 'FOX' => $temp_state, 'FO8' => $temp_state, 'BF1' => $temp_state
        );

        $this->filter = array(
            'HLVC' => array(
                'id' => 0,
                'title' => $player->translator->translateLFS('HLVC'),
                'values' => array(
                    'Clean only' => array('id' => 0, 'title' => $player->translator->translateLFS('CLEAN_ONLY'), 'state' => 1, 'key' => 'clean'),
                )
            ),
            'Controller' => array(
                'id' => 0,
                'title' => $player->translator->translateLFS('CONTROLLER'),
                'view' => 'H',
                'values' => array(
                    'W' => array('id' => 0, 'title' => $player->translator->translateLFS('SHORT_WHEEL'), 'state' => 0, 'key' => 'PF_W'),
                    'M' => array('id' => 0, 'title' => $player->translator->translateLFS('SHORT_MOUSE'), 'state' => 0, 'key' => 'PF_M'),
                    'KB' => array('id' => 0, 'title' => $player->translator->translateLFS('SHORT_KEYBOARD'), 'state' => 0, 'key' => 'PF_KB'),
                    'KBS' => array('id' => 0, 'title' => $player->translator->translateLFS('SHORT_KEYBOARD_STABILIZED'), 'state' => 0, 'key' => 'PF_KBS'),
                )
            ),
            'Clutch' => array(
                'id' => 0,
                'title' => $player->translator->translateLFS('CLUTCH'),
                'values' => array(
                    'Auto' => array('id' => 0, 'title' => $player->translator->translateLFS('CLUTCH_AUTO'), 'state' => 0, 'key' => 'PF_AC'),
                    'Button' => array('id' => 0, 'title' => $player->translator->translateLFS('CLUTCH_BUTTON'), 'state' => 0, 'key' => 'PF_BC'),
                    'Axis' => array('id' => 0, 'title' => $player->translator->translateLFS('CLUTCH_AXIS'), 'state' => 0, 'key' => 'PF_AxisC'),
                )
            ),
            'Gear' => array(
                'id' => 0,
                'title' => $player->translator->translateLFS('GEAR'),
                'values' => array(
                    'Sequential' => array('id' => 0, 'title' => $player->translator->translateLFS('GEAR_SEQUENTIAL'), 'state' => 0, 'key' => 'PF_SQ'),
                    'H-Shifter' => array('id' => 0, 'title' => $player->translator->translateLFS('GEAR_H_SHIFTER'), 'state' => 0, 'key' => 'PF_HS'),
                    'Auto' => array('id' => 0, 'title' => $player->translator->translateLFS('GEAR_AUTO'), 'state' => 0, 'key' => 'PF_AG'),
                )
            ),
            'Setup' => array(
                'id' => 0,
                'title' => $player->translator->translateLFS('SETUP'),
                'view' => 'H',
                'values' => array(
                    'ABS' => array('id' => 0, 'title' => $player->translator->translateLFS('ABS'), 'state' => 0, 'key' => 'SF_ABS'),
                    'TC' => array('id' => 0, 'title' => $player->translator->translateLFS('TC'), 'state' => 0, 'key' => 'SF_TC'),
                )
            ),
            'Tyres' => array(
                'id' => 0,
                'title' => $player->translator->translateLFS('TYRES'),
                'view' => 'H',
                'values' => array(
                    'R1' => array('id' => 0, 'title' => $player->translator->translateLFS('TYRES_R1'), 'state' => 0, 'key' => \Insim\Types\TyreCompounds::TYRE_R1),
                    'R2' => array('id' => 0, 'title' => $player->translator->translateLFS('TYRES_R2'), 'state' => 0, 'key' => \Insim\Types\TyreCompounds::TYRE_R2),
                    'R3' => array('id' => 0, 'title' => $player->translator->translateLFS('TYRES_R3'), 'state' => 0, 'key' => \Insim\Types\TyreCompounds::TYRE_R3),
                    'R4' => array('id' => 0, 'title' => $player->translator->translateLFS('TYRES_R4'), 'state' => 0, 'key' => \Insim\Types\TyreCompounds::TYRE_R4),
                    'R Super' => array('id' => 0, 'title' => $player->translator->translateLFS('TYRES_ROAD_SUPER'), 'state' => 0, 'key' => \Insim\Types\TyreCompounds::TYRE_ROAD_SUPER),
                    'R Normal' => array('id' => 0, 'title' => $player->translator->translateLFS('TYRES_ROAD_NORMAL'), 'state' => 0, 'key' => \Insim\Types\TyreCompounds::TYRE_ROAD_NORMAL),
                    'Hybrid' => array('id' => 0, 'title' => $player->translator->translateLFS('TYRES_HYBDRID'), 'state' => 0, 'key' => \Insim\Types\TyreCompounds::TYRE_HYBRID),
                    'Knobbly' => array('id' => 0, 'title' => $player->translator->translateLFS('TYRES_KNOBBLY'), 'state' => 0, 'key' => \Insim\Types\TyreCompounds::TYRE_KNOBBLY),
                )
            ),
            'TyresPos' => array(
                'id' => 0,
                'title' => '',
                'view' => 'H',
                'values' => array(
                    'Front' => array('id' => 0, 'title' => $player->translator->translateLFS('TYRES_FRONT'), 'state' => 0, 'key' => 'FRONT'),
                    'Rear' => array('id' => 0, 'title' => $player->translator->translateLFS('TYRES_REAR'), 'state' => 0, 'key' => 'REAR'),
                )
            ),
        );

        // Help text
        $helpText = $player->translator->translateLFS('HELP_TOP');

        foreach (preg_split("/((\r?\n)|(\r\n?))/", $helpText) as $line) {
            $this->help_lines[] = $player->translator->getLocaleChar().$line;
        }
        
        parent::__construct($alias, $player);
    }

    public function setData() {
        // Clear rowa
        $this->rows = array();

        for ($i = 0; $i < $this->rows_per_page; $i++) {
            $temp = array();
            foreach ($this->columns as $key => $col) {
                $temp[$key] = '';
            }
            $this->rows[] = $temp;
        }

        // Set conditions
        $aFilter = array();
        $lap = new LapTop();
        $lap->track = $this->player->host->Track;
        $lap->layout = $this->player->host->LName;
        //$lap->clean = 1;

        foreach ($this->cars as $carName => $value) {
            if (!empty($value['state']))
                $aFilter['car'][$carName] = $value['state'];
        }

        foreach ($this->filter as $sectionName => $filters) {
            switch ($sectionName) {
                default:
                    foreach ($filters['values'] as $filterName => $filter) {
                        if (!empty($filter['state']))
                            $aFilter[$sectionName][$filter['key']] = $filter['state'];
                    }
            }
        }
        
        $paginator = new Paginator(new PaginatorIterator($this->player->lapsService->findLapByConditions($lap, true, $aFilter, $this->search)));
        $paginator->setCurrentPageNumber($this->current_page)
                ->setItemCountPerPage($this->items_per_page)
                ->setPageRange(8);

        $this->max_page = count($paginator);
        $this->status_line = $this->current_page . ' / ' . $this->max_page;

        $index = 0;
        foreach ($paginator as $lap) {
            // Not nice
            if (!is_a($lap, 'LapTop')) {
                $temp_data = $lap;
                $lap = new LapTop();
                $lap->exchangeArray($temp_data);
            }

            $number = (($this->current_page - 1) * $this->items_per_page) + ($index + 1);

            $this->rows[$index] = array(
                'n' => $number . '.',
                'lap' => '^7' . $lap->time_string . '^8',
                'name' => trim($lap->PName) . ' ^8(' . $lap->car . ')',
                'splits' => !empty($lap->splits) ? implode('  |  ', $lap->splits) : '',
            );
            $index++;
        }

        // Own time
        $ownTime = new LapTop();
        $ownTime->player_id = $this->player->player_id;
        $ownTime->track = $this->player->host->Track;
        $ownTime->layout = $this->player->host->LName;
        //$ownTime->clean = 1;

        $ownTime = $this->player->lapsService->findLapByConditions($ownTime, false, $aFilter);

        if ($ownTime) {
            $this->own_time = array(
                'n' => '',
                'lap' => '^7' . $ownTime->time_string . '^8',
                'name' => trim($ownTime->PName) . ' ^8(' . $ownTime->car . ')',
                'splits' => !empty($ownTime->splits) ? implode('  |  ', $ownTime->splits) : '',
            );
        } else {
            $this->own_time = array(
                'n' => '---',
                'lap' => '^7---^8',
                'name' => $this->player->translator->translateLFS('PB_NOT_FOUND'),
                'splits' => '',
            );
        }
    }

    public function show($showBase = true) {
        $this->setData();

        if ($showBase)
            $this->showBase();

        $this->button_content_min = $this->id_current + 1;

        $this->showTopActions();

        // Filter bg
        $button = new isBTN();
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 2 + 12;
        $button->H = 80;
        $button->W = 14;
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);

        // Filter
        $height = 0;
        $top = $this->top + 2 + 12;
        foreach ($this->filter as $sectionName => $filters) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $this->left + 1;
            $button->T = $top + $height;
            $button->H = 4;
            $button->W = 14;
            $button->BStyle = ButtonStyles::ISB_LEFT + ButtonStyles::ISB_CLICK + ButtonStyles::ISB_LIGHT;
            $button->Text = '^7' . $this->filter[$sectionName]['title'];

            $left = $button->L;
            $viewMode = isset($this->filter[$sectionName]['view']) ? $this->filter[$sectionName]['view'] : 'V';

            if (!empty($this->filter[$sectionName]['title'])) {
                $height = 4;
                $top = $button->T + 1;
                $this->send($button);
            }else{
                $top += 1;
            }

            $this->filter[$sectionName]['id'] = $button->ReqI;

            if ($viewMode == 'H') {
                $btnWidth = 7;
            }
            if ($viewMode == 'V') {
                $btnWidth = 14;
            }

            $fIndex = 0;
            foreach ($filters['values'] as $filterName => $filter) {
                if ($filter['state'] == -1) {
                    $color = UI::COL_DISABLED;
                } elseif ($filter['state'] == 1) {
                    $color = UI::COL_ENABLED;
                } else {
                    $color = UI::COL_NEUTRAL;
                }

                $height = 3;
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = $left;
                $button->T = $top + $height;
                $button->H = 3;
                $button->W = $btnWidth;
                $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::ISB_CLICK;
                $button->Text = $color . $filter['title'];

                $this->send($button);
                $this->filter[$sectionName]['values'][$filterName]['id'] = $button->ReqI;

                if ($viewMode == 'H') {
                    $left += $btnWidth;
                    if ($fIndex % 2) {
                        $left = $this->left + 1;
                        $top = $button->T;
                    }
                }
                if ($viewMode == 'V') {
                    $top = $button->T;
                }

                $fIndex++;
            }

            $height = 4;
            $top = $button->T;
        }

        // Filter cars bg
        $cars_pos_top = $this->top + 2 + 12 + 2 + 80;
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $cars_pos_top;
        $button->H = 8;
        $button->W = $this->width - 2;
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);


        $this->showFooter();

        // Cars Filter
        $width = ceil(($this->width - 2) / (count($this->cars) / 2));
        $index = 0;

        foreach ($this->cars as $car => $value) {
            if ($value['state'] == -1) {
                $color = UI::COL_DISABLED;
            } elseif ($value['state'] == 1) {
                $color = UI::COL_ENABLED;
            } else {
                $color = UI::COL_NEUTRAL;
            }

            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = !$index ? $this->left + 1 : $button->L + $button->W;
            if ($index == ceil((count($this->cars) / 2)))
                $button->L = $this->left + 1;
            $button->T = $index >= ceil((count($this->cars) / 2)) ? $cars_pos_top + 4 : $cars_pos_top;
            $button->H = 4;
            $button->W = $width;
            $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::ISB_CLICK;
            $button->Text = $color . $car;

            $this->send($button);

            $this->cars[$car]['id'] = $button->ClickID;

            $index++;
        }

        // Times
        $newLine = 0;
        foreach ($this->rows as $key => $row) {
            $rowIndex = 0;
            foreach ($this->columns as $keyCol => $column) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = !$rowIndex ? ($this->left + $this->content_left_offset) + 1 : $button->L + $button->W;
                $button->T = $this->top + 2 + 12 + $newLine;
                $button->H = 5;
                $button->W = $column['width'];
                $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
                $button->Text = isset($row[$keyCol]) ? $row[$keyCol] : '';

                $this->send($button);
                $this->rows_buttons[$key][$keyCol] = $button->ClickID;
                $rowIndex++;
            }
            $newLine += 5;
        }

        // Own time
        $newLine += 5;
        $rowIndex = 0;
        foreach ($this->columns as $keyCol => $column) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = !$rowIndex ? ($this->left + $this->content_left_offset) + 1 : $button->L + $button->W;
            $button->T = $this->top + 2 + 12 + $newLine;
            $button->H = 5;
            $button->W = $column['width'];
            $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
            $button->Text = $this->own_time[$keyCol];

            $this->send($button);
            $this->own_buttons[$keyCol] = $button->ClickID;
            //$this->rows_buttons[$key][$keyCol] = $button->ClickID;
            $rowIndex++;
        }
        $newLine += 5;

        $this->button_content_max = $this->id_current;

        parent::show();
    }

    public function showTopActions() {
        $button = new isBTN();

        $left = $this->left + 3;
        foreach ($this->topActions as $key => $action) {
            $style = isset($action['style']) ? $action['style'] : 0;

            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $left;
            $button->T = $this->top + 2 + 6;
            $button->H = 4;
            $button->W = $action['width'];
            $button->BStyle = ButtonStyles::COLOUR_LIGHT_GREY + ButtonStyles::ISB_CLICK + $style;
            $button->Text = $action['title'];
            if (isset($action['typein'])) {
                $button->TypeIn = $action['typein'];
            }else{
                $button->TypeIn = 0;
            }

            $this->send($button);

            $left += $button->W + 1;
            $this->topActions[$key]['id'] = $button->ClickID;
        }
    }

    public function redrawContent() {
        $this->setData();

        $button = new isBTN();
        $button->ClickID = $this->button_id_status_line;
        $button->Text = $this->status_line;
        $this->send($button);

        foreach ($this->rows as $row_id => $row) {
            $buttons = $this->rows_buttons[$row_id];

            foreach ($buttons as $key => $id) {
                $button->ClickID = $id;
                $button->Text = $this->rows[$row_id][$key];
                $this->send($button);
            }
        }

        parent::redrawContent();
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);

        // car filter click
        foreach ($this->cars as $carName => $value) {
            if ($value['id'] == $packet->ClickID) {
                $this->setCarFilterButton($packet, $carName);
                return;
            }
        }

        // filter click
        foreach ($this->filter as $sectionName => $filters) {
            if ($filters['id'] == $packet->ClickID) {
                $this->clearFilterGroup($packet, $sectionName);
                return;
            }

            foreach ($filters['values'] as $filterName => $filter) {
                if ($filter['id'] == $packet->ClickID) {
                    $this->setFilter($packet, $sectionName, $filterName);
                    return;
                }
            }
        }

        // top action click
        foreach ($this->topActions as $key => $action) {
            if ($action['id'] == $packet->ClickID && !isset($action['typein'])) {
                $this->{$action['callback']}();
                return;
            }
        }

        // Lap time copy
        if ($packet->CFlags == isBTC::ISB_RMB) {
            foreach ($this->rows_buttons as $row_id => $buttons) {
                foreach ($buttons as $buttonID) {
                    if ($buttonID == $packet->ClickID) {
                        $this->actionCopyTime($this->rows[$row_id]);
                        return;
                    }
                }
            }
        }
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);

        foreach ($this->topActions as $key => $action) {
            if ($action['id'] == $packet->ClickID && @$action['typein'] > 0) {
                $this->{$action['callback']}($packet);
                return;
            }
        }

        //$this->topActions[$key]['id']
    }

    public function setCarFilterButton(isBTC $packet, $car) {
        $this->current_page = 1;
        $fltr = $this->cars[$car];

        $button = new isBTN();

        if (!$packet->isCTRL()) {
            foreach ($this->cars as $carName => $value) {
                $this->cars[$carName]['state'] = false;
                $button->ClickID = $value['id'];
                $button->Text = $carName;
                $this->send($button);
            }
        }

        $color = '';
        if ($packet->isRMB()) {
            if ($fltr['state'] > -1) {
                $fltr['state'] = -1;
                $color = UI::COL_DISABLED;
            } else {
                $fltr['state'] = 0;
                $color = UI::COL_NEUTRAL;
            }
        } elseif ($packet->isLMB()) {
            if ($fltr['state'] < 1) {
                $fltr['state'] = 1;
                $color = UI::COL_ENABLED;
            } else {
                $fltr['state'] = 0;
                $color = UI::COL_NEUTRAL;
            }
        }

        $this->cars[$car]['state'] = $fltr['state'];
        $button->ClickID = $fltr['id'];
        $button->Text = $color . $car;
        $this->send($button);

        $this->redrawContent();
        $this->updateOwnTime();
    }

    public function setCarFilter($car) {
        $this->current_page = 1;
        $fltr = $this->cars[$car];

        $button = new isBTN();

        // Clear all
        foreach ($this->cars as $carName => $value) {
            $this->cars[$carName]['state'] = false;
            $button->ClickID = $value['id'];
            $button->Text = $carName;
            $this->send($button);
        }

        $fltr['state'] = 1;
        $color = UI::COL_ENABLED;

        $this->cars[$car]['state'] = $fltr['state'];
        $button->ClickID = $fltr['id'];
        $button->Text = $color . $car;
        $this->send($button);

        $this->redrawContent();
        $this->updateOwnTime();
    }

    public function setFilter(isBTC $packet, $sectionName, $filterName) {
        $this->current_page = 1;
        $fltr = $this->filter[$sectionName]['values'][$filterName];

        $button = new isBTN();

        if (!$packet->isCTRL()) {
            foreach ($this->filter[$sectionName]['values'] as $key => $value) {
                $this->filter[$sectionName]['values'][$key]['state'] = false;
                $button->ClickID = $value['id'];
                $button->Text = $value['title'];
                $this->send($button);
            }
        }

        $color = '';
        if ($packet->isRMB()) {
            if (empty($fltr['state']) OR $fltr['state'] > -1) {
                $fltr['state'] = -1;
                $color = UI::COL_DISABLED;
            } else {
                $fltr['state'] = 0;
                $color = UI::COL_NEUTRAL;
            }
        } elseif ($packet->isLMB()) {
            if (empty($fltr['state']) OR $fltr['state'] < 1) {
                $fltr['state'] = 1;
                $color = UI::COL_ENABLED;
            } else {
                $fltr['state'] = 0;
                $color = UI::COL_NEUTRAL;
            }
        }

        $this->filter[$sectionName]['values'][$filterName]['state'] = $fltr['state'];
        $button->ClickID = $fltr['id'];
        $button->Text = $color . $this->filter[$sectionName]['values'][$filterName]['title'];
        $this->send($button);

        $this->redrawContent();
        $this->updateOwnTime();
    }

    public function clearFilterGroup(isBTC $packet, $sectionName) {
        //Debug::dump($sectionName);
    }

    public function updateOwnTime() {
        $own = new LapTop();
        $own->player_id = $this->player->player_id;
        $own->track = $this->player->host->Track;
        $own->layout = $this->player->host->LName;

        $aFilter = array();

        foreach ($this->cars as $carName => $value) {
            if (!empty($value['state']))
                $aFilter['car'][$carName] = $value['state'];
        }

        foreach ($this->filter as $sectionName => $filters) {
            switch ($sectionName) {
                default:
                    foreach ($filters['values'] as $filterName => $filter) {
                        if (!empty($filter['state']))
                            $aFilter[$sectionName][$filter['key']] = $filter['state'];
                    }
            }
        }

        $own = $this->player->lapsService->findLapByConditions($own, false, $aFilter);

        if ($own) {
            $this->own_time = array(
                'n' => '',
                'lap' => '^7' . $own->time_string . '^8',
                'name' => trim($own->PName) . ' ^8(' . $own->car . ')',
                'splits' => !empty($own->splits) ? implode(' | ', $own->splits) : '',
            );
        } else {
            $this->own_time = array(
                'n' => '---',
                'lap' => '^7---^8',
                'name' => $this->player->translator->translateLFS('PB_NOT_FOUND'),
                'splits' => '',
            );
        }

        $button = new isBTN();
        foreach ($this->own_time as $key => $value) {
            $button->ClickID = $this->own_buttons[$key];
            $button->Text = $value;
            $this->send($button);
        }
    }

    public function actionCopyTime($data) {
        if (empty($data)) {
            return;
        }

        $text = "^7{$data['n']} {$data['name']} - {$data['lap']} (" . (str_replace('  |  ', ' | ', $data['splits'])) . ")";
        $msg = new isMTC();
        $msg->UCID = 255;
        $msg->Text = $text;
        $this->send($msg);
    }

    public function actionSearch(isBTT $packet) {
        $search = trim($packet->Text);

        $this->search['PName_utf'] = $search;
        $this->redrawContent();
    }

    public function actionLayout(){
        
    }

    public function actionShowAllPlayers() {
        $button = new isBTN();

        $this->search['PName_utf'] = '';

        foreach ($this->filter as $sectionName => $filters) {
            foreach ($filters['values'] as $key => $value) {
                $this->filter[$sectionName]['values'][$key]['state'] = false;
                $button->ClickID = $value['id'];
                $button->Text = $value['title'];
                $this->send($button);
            }
        }

        $this->redrawContent();
    }

    public function actionShowTeam() {
        
    }

}
