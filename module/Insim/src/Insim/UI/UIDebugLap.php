<?php

namespace Insim\UI;

use Insim\Model\LapTop;
use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Packets\isMTC;
use Insim\Types\ButtonStyles;
use Zend\Debug\Debug;
use Zend\Paginator\Adapter\Iterator as PaginatorIterator;
use Zend\Paginator\Paginator;

class UIDebugLap extends UI {

    public $columns = array();
    public $rows = array();
    public $cars = array();
    public $filter = array();
    public $search = array();
    public $topActions = array();
    public $own_time = array();
    public $own_buttons = array();
    public $rows_buttons = array();
    public $rows_per_page = 8;
    public $items_per_page = 8;
    public $show_help = false;
    public $button_lap;
    public $button_lap_next;
    public $button_hlvc;
    public $lap = 'This lap: ^2OK';
    public $lap_next = 'Next lap: ^2OK';
    public $last_hlvc = 'HLVC: ^2OK';

    function __construct($alias, PlayerClass &$player, $width = 40, $height = 34, $top = 60) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = (200 - $width) - 2;

        // columns
        $this->columns = array(
            'name' => array('width' => ($this->width - 2), 'name' => 'name', 'style' => ButtonStyles::ISB_LEFT - ButtonStyles::ISB_DARK),
                //'distance' => array('width' => 8, 'name' => 'distance', 'style' => ButtonStyles::ISB_LEFT - ButtonStyles::ISB_DARK),
                //'angle' => array('width' => 8, 'name' => 'CMD', 'style' => ButtonStyles::ISB_LEFT - ButtonStyles::ISB_DARK),
                //'status' => array('width' => 8, 'name' => 'CMD', 'style' => ButtonStyles::ISB_LEFT - ButtonStyles::ISB_DARK),
        );

        parent::__construct($alias, $player);
    }

    public function update($data = array()) {
        //Debug::dump($data);

        $this->rows = array();

        $index = 0;

        foreach ($data as $value) {
            $this->rows[] = array(
                'name' => "{$value['name']} ^8" . (number_format((float) $value['distance'], 3)) . "s " . (number_format((float) $value['angle'], 3)) . ' ' . $value['status'],
            );

            $index++;
        }

        for ($i = count($this->rows); $i < $this->rows_per_page; $i++) {
            $this->rows[$i] = array('cmd' => '');
        }

        $this->redrawContent();
    }

    public function updateHLVC($data = array()) {
        if (isset($data['lap'])) {
            $this->lap = $data['lap'] ? 'This lap: ^2OK' : 'This lap: ^1NO';

            $button = new isBTN();
            $button->ClickID = $this->button_lap;
            $button->Text = $this->lap;
            $this->send($button);
        }

        if (isset($data['lap_next'])) {
            $this->lap_next = $data['lap_next'] ? 'Next lap: ^2OK' : 'Next lap: ^1NO';

            $button = new isBTN();
            $button->ClickID = $this->button_lap_next;
            $button->Text = $this->lap_next;
            $this->send($button);
        }

        if (isset($data['last_hlvc'])) {
            $this->last_hlvc = empty($data['last_hlvc']) ? 'HLVC: ^2OK' : 'HLVC: ^1' . $data['last_hlvc'];

            $button = new isBTN();
            $button->ClickID = $this->button_hlvc;
            $button->Text = $this->last_hlvc;
            $this->send($button);
        }
    }

    public function updateViewPLID() {
        $this->lap = $this->player->lapsService->clean_lap ? 'This lap: ^2OK' : 'This lap: ^1NO';
        $this->lap_next = $this->player->lapsService->clean_next_lap ? 'Next lap: ^2OK' : 'Next lap: ^1NO';
        $this->last_hlvc = empty($this->player->lapsService->last_hlvc) ? 'HLVC: ^2OK' : 'HLVC: ^1' . $this->player->lapsService->last_hlvc;

        $button = new isBTN();
        $button->ClickID = $this->button_lap;
        $button->Text = $this->lap;
        $this->send($button);

        $button->ClickID = $this->button_lap_next;
        $button->Text = $this->lap_next;
        $this->send($button);

        $button->ClickID = $this->button_hlvc;
        $button->Text = $this->last_hlvc;
        $this->send($button);
    }

    public function setData($data = array()) {
        $this->lap = $this->player->lapsService->clean_lap ? 'This lap: ^2OK' : 'This lap: ^1NO';
        $this->lap_next = $this->player->lapsService->clean_next_lap ? 'Next lap: ^2OK' : 'Next lap: ^1NO';
        $this->last_hlvc = empty($this->player->lapsService->last_hlvc) ? 'HLVC: ^2OK' : 'HLVC: ^1' . $this->player->lapsService->last_hlvc;

        $this->rows = array();

        $index = 0;
        $start = $this->current_page == 1 ? 0 : ($this->current_page - 1) * $this->rows_per_page;
        $end = $start + $this->rows_per_page - 1;

        foreach ($data as $value) {
            $this->rows[] = array(
                'cmd' => "{$value['name']}" . "^8 - {$value['distance']}s angle: {$value['angle']}°",
            );

            $index++;
        }

        for ($i = count($this->rows); $i < $this->rows_per_page; $i++) {
            $this->rows[$i] = array('cmd' => '');
        }
    }

    public function show($showBase = true) {
        $this->setData();

        if ($showBase)
            $this->showBaseSmall();

        $this->button_content_min = $this->id_current + 1;

        $button = new isBTN();

        // lap status
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 1 + 6;
        $button->H = 5;
        $button->W = 19;
        $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + ButtonStyles::ISB_LEFT;
        $button->Text = $this->lap;
        $this->button_lap = $button->ClickID;
        $this->send($button);

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1 + $button->W;
        $button->T = $this->top + 1 + 6;
        $button->H = 5;
        $button->W = 19;
        $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + ButtonStyles::ISB_LEFT;
        $button->Text = $this->lap_next;
        $this->button_lap_next = $button->ClickID;
        $this->send($button);

        // lap status
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 1 + 6 + $button->H;
        $button->H = 5;
        $button->W = 19;
        $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + ButtonStyles::ISB_LEFT;
        $button->Text = $this->last_hlvc;
        $this->button_hlvc = $button->ClickID;
        $this->send($button);

        $newLine = 10;
        foreach ($this->rows as $key => $row) {
            $rowIndex = 0;
            foreach ($this->columns as $keyCol => $column) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = !$rowIndex ? ($this->left) + 1 : $button->L + $button->W;
                $button->T = $this->top + 2 + 6 + $newLine;
                $button->H = 4;
                $button->W = $column['width'];
                $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
                $button->Text = isset($row[$keyCol]) ? $row[$keyCol] : '';

                $this->send($button);
                $this->rows_buttons[$key][$keyCol] = $button->ClickID;
                $rowIndex++;
            }
            $newLine += 4;
        }

        $this->button_content_max = $this->id_current;

        parent::show();
    }

    public function redrawContent() {
        $button = new isBTN();

        foreach ($this->rows as $row_id => $row) {
            $buttons = $this->rows_buttons[$row_id];

            foreach ($buttons as $key => $id) {
                $button->ClickID = $id;
                $button->Text = isset($this->rows[$row_id][$key]) ? $this->rows[$row_id][$key] : '';
                $this->send($button);
            }
        }

        parent::redrawContent();
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);
    }

}
