<?php

namespace Insim\UI;

use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Types\ButtonStyles;

class UIHelp extends UI {

    public $columns = array();
    public $rows = array();
    public $cars = array();
    public $filter = array();
    public $search = array();
    public $topActions = array();
    public $own_time = array();
    public $own_buttons = array();
    public $rows_buttons = array();
    public $rows_per_page = 22;
    public $items_per_page = 22;
    public $show_help = false;
    public $list = 'guest';

    function __construct($alias, PlayerClass &$player, $width = 122, $height = 101, $top = 30) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = (200 - $width) / 2;

        // columns
        $this->columns = array(
            'cmd' => array('width' => $this->width - 2, 'name' => 'CMD', 'style' => ButtonStyles::ISB_LEFT),
        );

        parent::__construct($alias, $player);
    }

    public function setData() {
        $this->rows = array();

        $index = 0;
        $start = $this->current_page == 1 ? 0 : ($this->current_page - 1) * $this->rows_per_page;
        $end = $start + $this->rows_per_page - 1;

        foreach ($this->player->commandService->commands as $cmd => $params) {
            if (isset($params['hidden']) && $params['hidden']) {
                continue;
            }
            if ($this->list && isset($params['list']) && $params['list'] != $this->list) {
                continue;
            }

            if ($index >= $start && $index <= $end) {
                $this->rows[] = array(
                    'cmd' => "^3!{$cmd}" . (!empty($params['params']) ? " {$params['params']}" : $params['params']) . "^8 - {$params['title']}",
                );
            }

            $index++;
        }

        for ($i = count($this->rows); $i < $this->rows_per_page; $i++) {
            $this->rows[$i] = array('cmd' => '');
        }

        $this->max_page = floor(count($this->player->commandService->commands) / $this->rows_per_page);
        $this->status_line = $this->current_page . ' / ' . $this->max_page;
    }

    public function show($showBase = true) {
        $this->setData();

        if ($showBase)
            $this->showBase();

        $this->button_content_min = $this->id_current + 1;

        $button = new isBTN();
        $newLine = 0;
        foreach ($this->rows as $key => $row) {
            $rowIndex = 0;
            foreach ($this->columns as $keyCol => $column) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = !$rowIndex ? ($this->left) + 1 : $button->L + $button->W;
                $button->T = $this->top + 2 + 12 + $newLine;
                $button->H = 4;
                $button->W = $column['width'];
                $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
                $button->Text = isset($row[$keyCol]) ? $row[$keyCol] : '';

                $this->send($button);
                $this->rows_buttons[$key][$keyCol] = $button->ClickID;
                $rowIndex++;
            }
            $newLine += 4;
        }


        $this->showFooter(-2);


        $this->button_content_max = $this->id_current;

        parent::show();
    }

    public function redrawContent() {
        $this->setData();

        $button = new isBTN();
        $button->ClickID = $this->button_id_status_line;
        $button->Text = $this->status_line;
        $this->send($button);

        foreach ($this->rows as $row_id => $row) {
            $buttons = $this->rows_buttons[$row_id];

            foreach ($buttons as $key => $id) {
                $button->ClickID = $id;
                $button->Text = $this->rows[$row_id][$key];
                $this->send($button);
            }
        }

        parent::redrawContent();
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);
    }

}
