<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;
use Insim\Types\MsgTypes;
use Insim\UI\Elements\BtnInput;
use Insim\UI\Elements\BtnSwitch;

class TabHostRaceRestart extends Tab {

    function __construct(PlayerClass &$player) {
        parent::__construct($player);

        $this->buttons = array(
            'race-auto-restart' => new BtnSwitch($player, 'Automaticaly restart race:', 5),
            'race-auto-restart-after' => new BtnInput($player, 'Restart after x seconds:', 5),
        );

        // Events
        $this->buttons['race-auto-restart']->eventValueChanged = function($value, $key) {
            $key = substr($key, 2);
            $this->player->host->settings->update('race-auto-restart', $value);
        };
        $this->buttons['race-auto-restart-after']->eventValueChanged = function($key, $value) {
            $val = intval($value);
            $val = $val < 10 ? 10 : $val;

            $this->player->host->settings->update('race-auto-restart-after', $val);
            return $val;
        };
    }

    function setData($data = array()) {
        parent::setData($data);

        $valuesYesNo = array(
            0 => MsgTypes::RED . 'No',
            1 => MsgTypes::GREEN . 'Yes',
        );

        $this->buttons['race-auto-restart']->setValues($valuesYesNo, $this->player->host->settings->get('race-auto-restart'));
        $this->buttons['race-auto-restart-after']->setValues(array('race-auto-restart-after' => MsgTypes::WHITE . $this->player->host->settings->get('race-auto-restart-after')));
    }

}
