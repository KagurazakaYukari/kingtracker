<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;
use Insim\Types\MsgTypes;
use Insim\UI\Elements\BtnInput;
use Insim\UI\Elements\BtnSwitch;

class TabHostSafetyGeneral extends Tab {

    function __construct(PlayerClass &$player) {
        parent::__construct($player);

        $this->buttons = array(
            'safety-idle' => new BtnSwitch($player, 'Detect players idle:', 5),
            'safety-idle-time' => new BtnInput($player, 'Maximum allowed idle time:', 5),
            'safety-idle-speed' => new BtnInput($player, 'Minimum speed:', 5),
        );

        // Events
        $this->buttons['safety-idle']->eventValueChanged = function($value, $key) {
            $key = substr($key, 2);
            $this->player->host->settings->update('safety-idle', $value);
        };

        $this->buttons['safety-idle-time']->eventValueChanged = function($key, $value) {
            $val = intval($value);

            $val = $val < 15 ? 15 : $val;

            $this->player->host->settings->update('safety-idle-time', $val);
            return $val;
        };

        $this->buttons['safety-idle-speed']->eventValueChanged = function($key, $value) {
            $val = intval($value);

            $val = $val < 5 ? 5 : $val;

            $this->player->host->settings->update('safety-idle-speed', $val);
            return $val;
        };
    }

    function setData($data = array()) {
        parent::setData($data);

        $valuesYesNo = array('0' => MsgTypes::RED . 'No', '1' => MsgTypes::GREEN . 'Yes',);

        $this->buttons['safety-idle']->setValues($valuesYesNo, $this->player->host->settings->get('safety-idle'));
        $this->buttons['safety-idle-time']->setValues(array('safety-idle-time' => MsgTypes::WHITE . $this->player->host->settings->get('safety-idle-time')));
        $this->buttons['safety-idle-speed']->setValues(array('safety-idle-speed' => MsgTypes::WHITE . $this->player->host->settings->get('safety-idle-speed')));
    }

}
