<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;
use Insim\Types\MsgTypes;
use Insim\UI\Elements\BtnInput;
use Insim\UI\Elements\BtnSwitch;

class TabHostRacePitStops extends Tab {

    function __construct(PlayerClass &$player) {
        parent::__construct($player);

        $this->buttons = array(
            'race-pits' => new BtnInput($player, 'Required pit stops:', 5),
            'race-pit-penalty' => new BtnSwitch($player, 'Penalty:', 5),
        );

        // Events
        $this->buttons['race-pit-penalty']->eventValueChanged = function($value) {
            $this->player->host->settings->update('race-pit-penalty', $value);
        };
        $this->buttons['race-pits']->eventValueChanged = function($key, $value) {
            $val = intval($value);
            $val = $val < 0 ? 0 : ($val > 4 ? 4 : $val);

            if ($val > 0) {
                $this->player->host->saveHostConfig('mustpit', 'yes');
            } else {
                $this->player->host->saveHostConfig('mustpit', 'no');
            }

            $currentPitWork = json_decode($this->player->host->settings->get('race-pit-work'), true);
            $currentPitWork = !$currentPitWork ? [] : $currentPitWork;
            ksort($currentPitWork, SORT_NUMERIC);
            $currentWindowFrom = json_decode($this->player->host->settings->get('race-pit-window-from'), true);
            $currentWindowFrom = !$currentWindowFrom ? [] : $currentWindowFrom;
            ksort($currentWindowFrom, SORT_NUMERIC);
            $currentWindowTo = json_decode($this->player->host->settings->get('race-pit-window-to'), true);
            $currentWindowTo = !$currentWindowTo ? [] : $currentWindowTo;
            ksort($currentWindowTo, SORT_NUMERIC);

            for ($i = 1; $i <= 5; $i++) {
                if ($i > $val) {
                    unset($currentPitWork[$i]);
                    unset($currentWindowFrom[$i]);
                    unset($currentWindowTo[$i]);
                }
            }
            
            $this->player->host->settings->update('race-pit-work', json_encode($currentPitWork));
            $this->player->host->settings->update('race-pit-window-from', json_encode($currentWindowFrom));
            $this->player->host->settings->update('race-pit-window-to', json_encode($currentWindowTo));
            
            $this->hide();
            $this->renderPits($val);
            $this->refresh();

            $this->player->host->settings->update('race-pits', $val);
            return $val;
        };
    }

    function renderPits($pits) {
        $valuesPtWork = array(
            'none' => MsgTypes::RED . 'None',
            'tyres' => MsgTypes::GREEN . 'Tyres',
            'fuel' => MsgTypes::GREEN . 'Fuel',
            'tyres|fuel' => MsgTypes::GREEN . 'Tyres or fuel',
        );

        $currentPitWork = json_decode($this->player->host->settings->get('race-pit-work'), true);
        $currentPitWork = !$currentPitWork ? [] : $currentPitWork;
        $currentWindowFrom = json_decode($this->player->host->settings->get('race-pit-window-from'), true);
        $currentWindowFrom = !$currentWindowFrom ? [] : $currentWindowFrom;
        $currentWindowTo = json_decode($this->player->host->settings->get('race-pit-window-to'), true);
        $currentWindowTo = !$currentWindowTo ? [] : $currentWindowTo;

        // Delete buttons
        for ($i = 1; $i <= 5; $i++) {
            unset($this->buttons["race-pit-work_{$i}"]);
            unset($this->buttons["race-pit-window_{$i}"]);
        }

        // Create new buttons
        $top = 3;
        for ($i = 1; $i <= $pits; $i++) {
            $this->buttons["race-pit-window_{$i}"] = new BtnInput($this->player, "Pit #{$i} window from - to lap :", 5, $top);

            $this->buttons["race-pit-window_{$i}"]->eventValueChanged = function($key, $value) use($i) {
                $currentWindowFrom = json_decode($this->player->host->settings->get('race-pit-window-from'), true);
                $currentWindowFrom = !$currentWindowFrom ? [] : $currentWindowFrom;
                $currentWindowTo = json_decode($this->player->host->settings->get('race-pit-window-to'), true);
                $currentWindowTo = !$currentWindowTo ? [] : $currentWindowTo;

                $val = intval($value);
                $val = $val < 0 ? 0 : $val;

                if ($key == 'race-pit-window-from') {
                    if (isset($currentWindowTo[$i - 1]) && !empty($currentWindowTo[$i - 1]) && !empty($val)) {
                        if ($val <= $currentWindowTo[$i - 1]) {
                            $val = $currentWindowTo[$i - 1] + 1;
                        }
                    }

                    $currentWindowFrom[$i] = $val;
                    $this->player->host->settings->update($key, json_encode($currentWindowFrom));
                }
                if ($key == 'race-pit-window-to') {

                    if (isset($currentWindowFrom[$i + 1]) && !empty($currentWindowFrom[$i + 1]) && !empty($val)) {
                        if ($val >= $currentWindowFrom[$i + 1]) {
                            $val = $currentWindowFrom[$i + 1] - 1;
                        }
                    }

                    $currentWindowTo[$i] = $val;
                    $this->player->host->settings->update($key, json_encode($currentWindowTo));
                }

                return $val;
            };

            $this->buttons["race-pit-window_{$i}"]->setValues(array(
                'race-pit-window-from' => MsgTypes::WHITE . (isset($currentWindowFrom[$i]) ? $currentWindowFrom[$i] : '0'),
                'race-pit-window-to' => MsgTypes::WHITE . (isset($currentWindowTo[$i]) ? $currentWindowTo[$i] : '0'))
            );

            $this->buttons["race-pit-work_{$i}"] = new BtnSwitch($this->player, 'Required work :', 5, $top);
            $this->buttons["race-pit-work_{$i}"]->width = 20;
            $this->buttons["race-pit-work_{$i}"]->setValues($valuesPtWork, isset($currentPitWork[$i]) ? $currentPitWork[$i] : 'none');
            $this->buttons["race-pit-work_{$i}"]->eventValueChanged = function($value) use($i) {
                $currentPitWork = json_decode($this->player->host->settings->get('race-pit-work'), true);
                $currentPitWork = !$currentPitWork ? [] : $currentPitWork;

                $currentPitWork[$i] = $value;
                $this->player->host->settings->update('race-pit-work', json_encode($currentPitWork));
            };

            $top += 3;
        }
    }

    function setData($data = array()) {
        parent::setData($data);

        $valuesPenalty = array(
            'none' => MsgTypes::RED . 'None',
            'dt' => MsgTypes::GREEN . 'DT',
            'sg' => MsgTypes::GREEN . 'SG',
            '30' => MsgTypes::GREEN . '30',
            '45' => MsgTypes::GREEN . '45',
        );

        $this->buttons['race-pit-penalty']->setValues($valuesPenalty, $this->player->host->settings->get('race-pit-penalty'));
        $this->buttons['race-pits']->setValues(array('race-pits' => MsgTypes::WHITE . $this->player->host->settings->get('race-pits')));

        $this->renderPits($this->player->host->settings->get('race-pits'));
    }

}
