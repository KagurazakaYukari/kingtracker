<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;
use Insim\Types\MsgTypes;
use Insim\UI\Elements\BtnSwitch;

class TabHostRaceGeneral extends Tab {
    function __construct(PlayerClass &$player) {
        parent::__construct($player);
        
        $this->buttons = array(
            'canreset' => new BtnSwitch($player, 'Allow car reset:', 5),
            'vote' => new BtnSwitch($player, 'Allow guests voting (kick/ban):', 5),
            'select' => new BtnSwitch($player, 'Allow guests select track:', 5),
            'midrace' => new BtnSwitch($player, 'Join during race:', 5),
            'fcv' => new BtnSwitch($player, 'Force cockpit view:', 5),
            'cruise' => new BtnSwitch($player, 'Allow wrong way:', 5),
            'autokick' => new BtnSwitch($player, 'Wrong way drivers:', 5),
            //'rstmin' => new BtnSwitch($player, 'Cancel restart after race start:', 5),
            //'rstend' => new BtnSwitch($player, 'Cancel restart after race finish:', 5),
        );
        
        // Events
        $this->buttons['canreset']->eventValueChanged = function($value){
            $this->player->host->saveHostConfig('canreset', $value);
        };
        $this->buttons['vote']->eventValueChanged = function($value){
            $this->player->host->saveHostConfig('vote', $value);
        };
        $this->buttons['select']->eventValueChanged = function($value){
            $this->player->host->saveHostConfig('select', $value);
        };
        $this->buttons['midrace']->eventValueChanged = function($value){
            $this->player->host->saveHostConfig('midrace', $value);
        };
        $this->buttons['fcv']->eventValueChanged = function($value){
            $this->player->host->saveHostConfig('fcv', $value);
        };
        $this->buttons['cruise']->eventValueChanged = function($value){
            $this->player->host->saveHostConfig('cruise', $value);
        };
        $this->buttons['autokick']->eventValueChanged = function($value){
            $this->player->host->saveHostConfig('autokick', $value);
        };
    }
    
    function setData($data = array()) {
        parent::setData($data);
        
        $valuesYesNo = array(
            'no' => MsgTypes::RED.'No',
            'yes' => MsgTypes::GREEN.'Yes',
        );
        
        //\Zend\Debug\Debug::dump($this->player->host->config);
        
        $this->buttons['canreset']->setValues($valuesYesNo, @$this->player->host->config['canreset']);
        $this->buttons['vote']->setValues($valuesYesNo, @$this->player->host->config['vote']);
        $this->buttons['select']->setValues($valuesYesNo, @$this->player->host->config['select']);
        $this->buttons['midrace']->setValues($valuesYesNo, @$this->player->host->config['midrace']);
        $this->buttons['fcv']->setValues($valuesYesNo, @$this->player->host->config['fcv']);
        $this->buttons['cruise']->setValues($valuesYesNo, @$this->player->host->config['cruise']);
        //$this->buttons['modified']->setValues($valuesYesNo);
        
        $this->buttons['autokick']->setValues(array(
            'no' => MsgTypes::RED.'No',
            'kick' => MsgTypes::GREEN.'Kick',
            'ban' => MsgTypes::GREEN.'Ban',
            'spec' => MsgTypes::GREEN.'Spec',
        ), @$this->player->host->config['autokick']);
                
    }
    
    
}
