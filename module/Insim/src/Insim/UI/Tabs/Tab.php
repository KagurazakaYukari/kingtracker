<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;
use Insim\Packets\isBFN;
use Insim\Packets\isBTC;
use Insim\Packets\isBTT;

class Tab {
    protected $id_current = 0;
    protected $player;
    protected $buttons;
    public $visible = false;
    public $top = 0;
    public $left = 0;
    public $width = 0;

    public function __construct(PlayerClass &$player) {
        $this->player = $player;
    }

    public function setData($data = array()) {
        
    }
    
    public function show($id_current, $tabTop, $tabLeft, $tabWidth) {
        $this->id_current = $id_current;
        $this->top = $tabTop;
        $this->left = $tabLeft;
        $this->width = $tabWidth;
        
        $tabTop += 5;
        foreach ($this->buttons as $key => $btn) {
            $btn->show($id_current, $tabTop, $tabLeft);
            $id_current = $btn->end_id;
            $tabTop += $btn->height + 1;
        }
        
        $this->visible = true;
    }

    public function hide() {
        $start = null;
        $end = 0;
        
        foreach ($this->buttons as $key => $btn) {
            if($start == null OR $start > $btn->start_id){
                $start = $btn->start_id;
            }
            
            if($end < $btn->end_id){
                $end = $btn->end_id;
            }
        }
        
        
        $this->visible = false;
        
        if($start == null){
            return;
        }
                
        $bfn = new isBFN();
        $bfn->SubT = isBFN::BFN_DEL_BTN;
        $bfn->UCID = $this->player->UCID;
        $bfn->ClickID = $start;
        $bfn->ClickMax = $end;

        $this->player->host->insim->send($bfn);
    }
    
    public function refresh(){
        $this->hide();
        
        $id_current = $this->id_current;
        $tabLeft = $this->left;
        $tabTop = $this->top;
        $tabTop += 5;
        foreach ($this->buttons as $key => $btn) {
            $btn->show($id_current, $tabTop, $tabLeft);
            $id_current = $btn->end_id;
            $tabTop += $btn->height + 1;
        }
    }

    public function eventClick(isBTC $packet){
        foreach ($this->buttons as $key => $btn) {
            $btn->eventClick($packet);
        }
    }
    
    public function eventType(isBTT $packet){
        foreach ($this->buttons as $key => $btn) {
            $btn->eventType($packet);
        }
    }
    
}

