<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;
use Insim\Types\MsgTypes;
use Insim\UI\Elements\BtnInput;
use Insim\UI\Elements\BtnLabel;
use Insim\UI\Elements\BtnSwitch;

class TabHostRaceCars extends Tab {

    protected $floatFilter = null;
    protected $cars;

    function __construct(PlayerClass &$player) {
        parent::__construct($player);

        $this->floatFilter = new \Insim\Filter\FloatFilter();

        $this->buttons = array(
            'label-demo' => new BtnLabel($player, 'Demo cars:', 5, $top = -2, $left = 0),
        );

        $this->cars = $this->player->host->hostService->carService->getOfficial();

        // Demo cars
        $leftEdge = -31;
        $top = -2;
        $left = $leftEdge;
        $count = 0;
        foreach ($this->cars['DEMO'] as $car) {
            $count++;
            
            $this->buttons['cars-' . $car->code] = new BtnSwitch($this->player, '', 6, $top, $left);
            $this->buttons['cars-' . $car->code]->eventValueChanged = function($value, $key) {
                $key = substr($key, 2);
                $this->player->host->applyAllowedCar($key, $value);
            };

            $this->buttons['cars-' . $car->code . '-res'] = new BtnInput($this->player, '', $height = 3, ($top - 1), ($left + 40), $width = 5, $car->code);
            $this->buttons['cars-' . $car->code . '-res']->eventValueChanged = function($key, $value, $uuid) {
                $val = intval($value);

                switch ($key) {
                    case 'res': $val = max(0, min(50, $val));
                        break;
                    case 'mass' : $val = max(0, min(190, $val));
                        break;
                }

                $this->player->host->applyAllowedCarRest($uuid, $key, $val);
                return $val . ($key == 'mass' ? ' kg' : ' %');
            };

            if ($count % 8 == 0) {
                $left = $leftEdge;
            } else {
                $left += 10;
                $top -= 11;
            }
        }


        // S1 cars
        $top += 12;
        $left = 0;

        $this->buttons['label-s1'] = new BtnLabel($player, 'S1 cars:', 5, $top, $left);

        $left = $leftEdge;

        $count = 0;
        foreach ($this->cars['S1'] as $car) {
            $count++;
            
            $this->buttons['cars-' . $car->code] = new BtnSwitch($this->player, '', 6, $top, $left);
            $this->buttons['cars-' . $car->code]->eventValueChanged = function($value, $key) {
                $key = substr($key, 2);
                $this->player->host->applyAllowedCar($key, $value);
            };

            $this->buttons['cars-' . $car->code . '-res'] = new BtnInput($this->player, '', $height = 3, ($top - 1), $left + 40, $width = 5, $car->code);
            $this->buttons['cars-' . $car->code . '-res']->eventValueChanged = function($key, $value, $uuid) {
                $val = intval($value);

                switch ($key) {
                    case 'res': $val = max(0, min(50, $val));
                        break;
                    case 'mass' : $val = max(0, min(190, $val));
                        break;
                }

                $this->player->host->applyAllowedCarRest($uuid, $key, $val);
                return $val . ($key == 'mass' ? ' kg' : ' %');
            };

            if ($count % 8 == 0) {
                $left = $leftEdge;
            } else {
                $left += 10;
                $top -= 11;
            }
        }

        // S2 cars
        $top += 12;
        $left = 0;

        $this->buttons['label-s2'] = new BtnLabel($player, 'S2 cars:', 5, $top, $left);

        $left = $leftEdge;

        $count = 0;
        foreach ($this->cars['S2'] as $car) {
            $count++;

            $this->buttons['cars-' . $car->code] = new BtnSwitch($this->player, '', 6, $top, $left);
            $this->buttons['cars-' . $car->code]->eventValueChanged = function($value, $key) {
                $key = substr($key, 2);
                $this->player->host->applyAllowedCar($key, $value);
            };

            $this->buttons['cars-' . $car->code . '-res'] = new BtnInput($this->player, '', $height = 3, ($top - 1), $left + 40, $width = 5, $car->code);
            $this->buttons['cars-' . $car->code . '-res']->eventValueChanged = function($key, $value, $uuid) {
                $val = intval($value);

                switch ($key) {
                    case 'res': $val = max(0, min(50, $val));
                        break;
                    case 'mass' : $val = max(0, min(190, $val));
                        break;
                }

                $this->player->host->applyAllowedCarRest($uuid, $key, $val);
                return $val . ($key == 'mass' ? ' kg' : ' %');
            };

            if ($count % 8 == 0) {
                $left = $leftEdge;
            } else {
                $left += 10;
                $top -= 11;
            }
        }
    }

    function setData($data = array()) {
        parent::setData($data);

        $allowed_cars = $this->player->host->settings->get('allowed-cars');
        
        foreach ($this->cars['DEMO'] as $car) {
            $enabled = isset($allowed_cars[$car->code]['enabled']) ? $allowed_cars[$car->code]['enabled'] : 0;
            $res = isset($allowed_cars[$car->code]['res']) ? $allowed_cars[$car->code]['res'] : 0;
            $mass = isset($allowed_cars[$car->code]['mass']) ? $allowed_cars[$car->code]['mass'] : 0;
            $this->buttons['cars-' . $car->code]->setValues(array('0' => MsgTypes::RED . $car->code, '1' => MsgTypes::GREEN . $car->code), $enabled);
            $this->buttons['cars-' . $car->code . '-res']->setValues(array('res' => MsgTypes::WHITE . "{$res} %", 'mass' => MsgTypes::WHITE . "{$mass} kg"));
        }

        foreach ($this->cars['S1'] as $car) {
            $enabled = isset($allowed_cars[$car->code]['enabled']) ? $allowed_cars[$car->code]['enabled'] : 0;
            $res = isset($allowed_cars[$car->code]['res']) ? $allowed_cars[$car->code]['res'] : 0;
            $mass = isset($allowed_cars[$car->code]['mass']) ? $allowed_cars[$car->code]['mass'] : 0;
            $this->buttons['cars-' . $car->code]->setValues(array('0' => MsgTypes::RED . $car->code, '1' => MsgTypes::GREEN . $car->code), $enabled);
            $this->buttons['cars-' . $car->code . '-res']->setValues(array('res' => MsgTypes::WHITE . "{$res} %", 'mass' => MsgTypes::WHITE . "{$mass} kg"));
        }

        foreach ($this->cars['S2'] as $car) {
            $enabled = isset($allowed_cars[$car->code]['enabled']) ? $allowed_cars[$car->code]['enabled'] : 0;
            $res = isset($allowed_cars[$car->code]['res']) ? $allowed_cars[$car->code]['res'] : 0;
            $mass = isset($allowed_cars[$car->code]['mass']) ? $allowed_cars[$car->code]['mass'] : 0;
            $this->buttons['cars-' . $car->code]->setValues(array('0' => MsgTypes::RED . $car->code, '1' => MsgTypes::GREEN . $car->code), $enabled);
            $this->buttons['cars-' . $car->code . '-res']->setValues(array('res' => MsgTypes::WHITE . "{$res} %", 'mass' => MsgTypes::WHITE . "{$mass} kg"));
        }
    }

}
