<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;
use Insim\Types\MsgTypes;
use Insim\UI\Elements\BtnInput;
use Insim\UI\Elements\BtnLabel;
use Insim\UI\Elements\BtnSwitch;

class TabHostRaceRotation extends Tab {
    protected $floatFilter = null;

    function __construct(PlayerClass &$player) {
        parent::__construct($player);
        
        $this->floatFilter = new \Insim\Filter\FloatFilter();

        $this->buttons = array(
            'track-rotation' => new BtnSwitch($player, 'Enable track rotation:', 5),
            'rotation-mode' => new BtnSwitch($player, 'Rotation mode:', 5),
            'rotation-races' => new BtnInput($player, 'Rotate after x races:', 5),
            'rotation-qualify' => new BtnInput($player, 'Qualify after track change (min):', 5),
            'label-tracks' => new BtnLabel($player, 'Available tracks:', 5, $top = 3, $left = 0),
        );

        // Tracks
        $top = 3;
        $left = -30;
        foreach ($this->player->host->hostService->trackService->tracks as $track) {
            $this->buttons['tracks-' . $track['shortName']] = new BtnSwitch($player, '', 5, $top, $left);
            $this->buttons['tracks-' . $track['shortName']]->eventValueChanged = function($value, $key) {
                $key = substr($key, 2);
                $this->player->host->settings->update("tracks-{$key}", $value);
            };
            $left += 10;
            $top -= 6;
        }

        // Track types
        $top += 9;
        $this->buttons['label-tracks-types'] = new BtnLabel($player, 'Allowed track types:', 5, $top);

        $left = 0;
        $top -= 6;
        foreach ($this->player->host->hostService->trackService->trackTypes as $type) {
            $this->buttons['tracks-types-' . $type] = new BtnSwitch($player, '', 5, $top, $left);
            $this->buttons['tracks-types-' . $type]->eventValueChanged = function($value, $key) {
                $key = substr($key, 2);
                $this->player->host->settings->update("tracks-types-{$key}", $value);
            };
            $left += 10;
            $top -= 6;
        }

        // Grid check
        $top = -57;
        $this->buttons['rotation-grid'] = new BtnSwitch($player, 'Check grid size:', 5, $top);
        $this->buttons['rotation-grid']->eventValueChanged = function($value) {
            $this->player->host->settings->update('rotation-grid', $value);
        };

        // Rotation legth
        $this->buttons['rotation-lenght'] = new BtnInput($player, 'Min Max track length (0 - unlimited):', $height = 5, $top);
        $this->buttons['rotation-lenght']->eventValueChanged = function($key, $value) {
            $val = $this->floatFilter->filter($value);
            $val = $val < 0 ? 0 : $val;

            $this->player->host->settings->update($key, $val);
            $this->player->host->playerService->eventHostSettingChanged("rotation-lenght-{$key}", $val);
            return $val;
        };

        // Track rotation
        $this->buttons['track-rotation']->eventValueChanged = function($value) {
            $this->player->host->settings->update('track-rotation', $value);
            $this->player->host->playerService->eventHostSettingChanged('track-rotation', $value);
        };
        
        // Rotation mode
        $this->buttons['rotation-mode']->eventValueChanged = function($value) {
            $this->player->host->settings->update('rotation-mode', $value);
            $this->player->host->playerService->eventHostSettingChanged('rotation-mode', $value);
        };
        
        // Rotation races
        $this->buttons['rotation-races']->eventValueChanged = function($key, $value) {
            $val = intval($value);
            $val = $val < 1 ? $this->player->host->settings->getDefault('rotation-races') : $val;

            $this->player->host->settings->update($key, $val);
            $this->player->host->playerService->eventHostSettingChanged($key, $val);
            return $val;
        };
        
        // Qualify after race
        $this->buttons['rotation-qualify']->eventValueChanged = function($key, $value) {
            $val = intval($value);
            $val = $val < 1 ? $this->player->host->settings->getDefault('rotation-qualify') : $val;

            $this->player->host->settings->update($key, $val);
            $this->player->host->playerService->eventHostSettingChanged($key, $val);
            return $val;
        };
    }

    function setData($data = array()) {
        parent::setData($data);

        $valuesYesNo = array('0' => MsgTypes::RED . 'No', '1' => MsgTypes::GREEN . 'Yes',);
        $valuesRotMode = array('manual' => MsgTypes::WHITE . 'Manual', 'random' => MsgTypes::WHITE . 'Random', 'vote' => MsgTypes::WHITE . 'Vote');

        $this->buttons['track-rotation']->setValues($valuesYesNo, $this->player->host->settings->get('track-rotation'));
        $this->buttons['rotation-mode']->setValues($valuesRotMode, $this->player->host->settings->get('rotation-mode'));
        $this->buttons['rotation-qualify']->setValues(array('rotation-qualify' => MsgTypes::WHITE . $this->player->host->settings->get('rotation-qualify')));
        $this->buttons['rotation-grid']->setValues($valuesYesNo, $this->player->host->settings->get('rotation-grid'));
        $this->buttons['rotation-races']->setValues(array('rotation-races' => MsgTypes::WHITE . $this->player->host->settings->get('rotation-races')));
        $this->buttons['rotation-lenght']->setValues(array('rotation-length-min' => MsgTypes::WHITE . $this->player->host->settings->get('rotation-length-min'), 'rotation-length-max' => MsgTypes::WHITE . $this->player->host->settings->get('rotation-length-max')));

        foreach ($this->player->host->hostService->trackService->tracks as $track) {
            $this->buttons['tracks-' . $track['shortName']]->setValues(array('0' => MsgTypes::RED . $track['shortName'], '1' => MsgTypes::GREEN . $track['shortName']), $this->player->host->settings->get('tracks-' . $track['shortName']));
        }

        foreach ($this->player->host->hostService->trackService->trackTypes as $type) {
            $this->buttons['tracks-types-' . $type]->setValues(array('0' => MsgTypes::RED . $type, '1' => MsgTypes::GREEN . $type), $this->player->host->settings->get('tracks-types-' . $type));
        }
    }

}
