<?php

namespace Insim\Packets;


/**
 * Button
 */
class isBTN extends Packet {

    const PACK = 'CCCCCCCCCCCCa240';
    const UNPACK = 'CSize/CType/CReqI/CUCID/CClickID/CInst/CBStyle/CTypeIn/CL/CT/CW/CH/a240Text';
    const IS_X_MIN = 0;
    const IS_X_MAX = 110;
    const IS_Y_MIN = 30;
    const IS_Y_MAX = 170;

    protected $Size = 252;      # 12 + TEXT_SIZE (a multiple of 4)
    protected $Type = Packet::ISP_BTN;  # ISP_BTN
    public $ReqI = 255;         # non-zero (returned in IS_BTC and IS_BTT packets)
    public $UCID = 255;         # connection to display the button (0 = local / 255 = all)
    public $ClickID = 0;        # button ID (0 to 239)
    public $Inst = null;        # some extra flags - see below
    public $BStyle = null;      # button style flags - see below
    public $TypeIn = null;      # max chars to type in - see below
    public $L = isBTN::IS_X_MIN;       # left   : 0 - 200
    public $T = isBTN::IS_Y_MIN;       # top    : 0 - 200
    public $W = 0;              # width  : 0 - 200
    public $H = 0;              # height : 0 - 200
    public $Text = '';          # 0 to 240 characters of text

    public function pack() {
        if (strLen($this->Text) > 239) {
            $this->Text = subStr($this->Text, 0, 239);
        }

        return parent::pack();
    }

}
