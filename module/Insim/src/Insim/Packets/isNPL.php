<?php

namespace Insim\Packets;

/**
 * New PLayer joining race (if PLID already exists, then leaving pits)
 */
class isNPL extends Packet {

    const PACK = 'CCCCCCva24a8a4a16C4CCCCVCCxx';
    const UNPACK = 'CSize/CType/CReqI/CPLID/CUCID/CPType/vFlags/a24PName/A8Plate/a4CName/a16SName/C4Tyres/CH_Mass/CH_TRes/CModel/CPass/VSpare/CSetF/CNumP/CSp2/CSp3';

    protected $Size = 76;       # 76
    protected $Type = Packet::ISP_NPL;  # ISP_NPL
    public $ReqI;               # 0 unless this is a reply to an TINY_NPL request
    public $PLID;               # player's newly assigned unique id
    public $UCID;               # connection's unique id
    public $PType;              # bit 0 : female / bit 1 : AI / bit 2 : remote
    public $Flags;              # player flags
    public $PName;              # nickname
    public $Plate;              # number plate - NO ZERO AT END!
    public $CName;              # car name
    public $SName;              # skin name - MAX_CAR_TEX_NAME
    public $Tyres = array();    # compounds
    public $H_Mass;             # added mass (kg)
    public $H_TRes;             # intake restriction
    public $Model;              # driver model
    public $Pass;               # passengers byte
    protected $Spare;
    public $SetF;               # setup flags (see below)
    public $NumP;               # number in race (same when leaving pits, 1 more if new)
    protected $Sp2;
    protected $Sp3;

    public $Flags_string = '';


    public function unpack($rawPacket) {
        $pkClass = unpack($this::UNPACK, $rawPacket);

        for ($Tyre = 1; $Tyre <= 4; ++$Tyre) {
            $pkClass['Tyres'][] = $pkClass["Tyres{$Tyre}"];
            unset($pkClass["Tyres{$Tyre}"]);
        }

        foreach ($pkClass as $property => $value) {
            if (is_string($value)) {
                $value = trim($value);
            }
            $this->$property = $value;
        }

        return $this;
    }

    public function isFemale() {
        return ($this->PType & 1);
    }

    public function isAI() {
        return ($this->PType & 2);
    }

    public function isRemote() {
        return ($this->PType & 4);
    }
    
    public function exchangeObject() {
        $data = array();
        $data['UCID'] = (!empty($this->UCID)) ? $this->UCID : null;
        $data['PLID'] = (!empty($this->PLID)) ? $this->PLID : null;
        $data['PName'] = (!empty($this->PName)) ? $this->PName : null;
        $data['PType'] = (!empty($this->PType)) ? $this->PType : null;
        $data['Flags'] = (!empty($this->Flags)) ? $this->Flags : null;
        $data['Flags_string'] = (!empty($this->Flags_string)) ? $this->Flags_string : null;
        $data['CName'] = (!empty($this->CName)) ? $this->CName : null;
        $data['SetF'] = (!empty($this->SetF)) ? $this->SetF : null;
        return $data;
    }
    
    public function getCarName(){
        return $this->CName;
    }

}
