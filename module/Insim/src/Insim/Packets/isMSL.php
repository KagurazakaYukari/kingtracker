<?php

namespace Insim\Packets;

/**
 * Message
 */
class isMSL extends Packet {

    const PACK = 'CCxCa128';
    const UNPACK = 'CSize/CType/CReqI/CSound/a128Msg';

    protected $Size = 132;      # 132
    protected $Type = Packet::ISP_MSL;  # ISP_MSL
    protected $ReqI = 0;        # 0
    public $Sound = 0; # sound effect (see Message Sounds below)
    public $Msg;                # last byte must be zero

    public function pack() {
        if (strLen($this->Msg) > 127) {
            $lastColor = '^8';
            foreach (explode("\n", wordwrap($this->Msg, 127, "\n", true)) as $Msg) {
                $this->Msg($lastColor . $Msg)->Send();
                $lastColorPosition = strrpos($Msg, '^');
                if ($lastColorPosition !== false) {
                    $lastColor = substr($Msg, $lastColorPosition, 2);
                }
                $this->Msg('');
            }
        }

        return parent::pack();
    }

}
