<?php

namespace Insim\Packets;

use Insim\Types\InSimTypes;

/**
 * InSim Init - packet to initialise the InSim system
 */
class isISI extends Packet {

    const PACK = 'CCCxvvCCva16a16';
    const UNPACK = 'CSize/CType/CReqI/CZero/vUDPPort/vFlags/CInSimVer/CPrefix/vInterval/a16Admin/a16IName';

    protected $Size = 44;       # 44
    protected $Type = Packet::ISP_ISI;
    public $ReqI;               # If non-zero LFS will send an IS_VER packet
    protected $Zero = null;     # 0
    public $UDPPort;            # Port for UDP replies from LFS (0 to 65535)
    public $Flags;              # Bit flags for options (see below)
    public $InSimVer = InSimTypes::INSIM_VERSION;
    public $Prefix;             # Special host message prefix character
    public $Interval;           # Time in ms between NLP or MCI (0 = none)
    public $Admin;              # Admin password (if set in LFS)
    public $IName;              # A short name for your program

    public function __conStruct($rawPacket = null) {
        parent::__conStruct($rawPacket);
    }
}
