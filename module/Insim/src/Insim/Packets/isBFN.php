<?php

namespace Insim\Packets;

/**
 * Button function
 */
class isBFN extends Packet {

    const PACK = 'CCxCCCCx';
    const UNPACK = 'CSize/CType/CReqI/CSubT/CUCID/CClickID/CClickMax/CSp3';
    
    const BFN_DEL_BTN = 0;     //  0 - inStruction     : delete one button (must set ClickID)
    const BFN_CLEAR = 1;     //  1 - inStruction        : clear all buttons made by this insim instance
    const BFN_USER_CLEAR = 2;     //  2 - info            : user cleared this insim instance's buttons
    const BFN_REQUEST = 3;     //  3 - user request    : SHIFT+B or SHIFT+I - request for buttons

    protected $Size = 8;        # 8
    protected $Type = Packet::ISP_BFN;  # ISP_BFN
    protected $ReqI;            # 0
    public $SubT;               # subtype, from BFN_ enumeration (see below)
    public $UCID;               # connection to send to or from (0 = local / 255 = all)
    public $ClickID;            # ID of button to delete (if SubT is BFN_DEL_BTN)
    public $ClickMax;               # used internally by InSim
    protected $Sp3;

    function isUserRequest(){
        return $this->SubT == isBFN::BFN_REQUEST;
    }
    function isUserClear(){
        return $this->SubT == isBFN::BFN_USER_CLEAR;
    }
}
