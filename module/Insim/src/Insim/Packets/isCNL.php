<?php

namespace Insim\Packets;

/**
 * Connection leave
 */
class isCNL extends Packet {

    const LEAVR_DISCO = 0;          // none
    const LEAVR_TIMEOUT = 1;        // timed out
    const LEAVR_LOSTCONN = 2;       // lost connection
    const LEAVR_KICKED = 3;         // kicked
    const LEAVR_BANNED = 4;         // banned
    const LEAVR_SECURITY = 5;       // security
    const LEAVR_CPW = 6;            // cheat protection wrong
    const LEAVR_OOS = 7;            // out of sync with host
    const LEAVR_JOOS = 8;           // join OOS (initial sync failed)
    const LEAVR_HACK = 9;           // invalid packet
    const PACK = 'CCxCCCxx';
    const UNPACK = 'CSize/CType/CReqI/CUCID/CReason/CTotal/CSp2/CSp3';

    protected $Size = 8;        # 8
    protected $Type = Packet::ISP_CNL;  # ISP_CNL
    public $ReqI;               # 0
    public $UCID;               # unique id of the connection which left
    public $Reason;             # leave reason (see below)
    public $Total;              # number of connections including host
    protected $Sp2;
    protected $Sp3;

    public static function reasonToString($reson) {
        switch ($reson) {
            case isCNL::LEAVR_DISCO:
                return 'Disconnected';
            case isCNL::LEAVR_TIMEOUT:
                return 'Timed out';
            case isCNL::LEAVR_LOSTCONN:
                return 'Lost connection';
            case isCNL::LEAVR_KICKED:
                return 'Kicked';
            case isCNL::LEAVR_BANNED:
                return 'Banned';
            case isCNL::LEAVR_SECURITY:
                return 'Security';
            case isCNL::LEAVR_CPW:
                return 'Cheat protection';
            case isCNL::LEAVR_OOS:
                return 'Out of sync with host';
            case isCNL::LEAVR_JOOS:
                return 'Join OOS (initial sync failed)';
            case isCNL::LEAVR_HACK:
                return 'Hack (invalid packet)';
        }
    }
    

}
