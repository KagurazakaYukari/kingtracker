<?php

namespace Insim\Packets;

/**
 * Reorder packet
 */
class isREO extends Packet {

    const PACK = 'CCCCC40';
    const UNPACK = 'CSize/CType/CReqI/CNumP/C40PLID';

    protected $Size = 44;       # 44
    protected $Type = self::ISP_REO;  # ISP_REO
    public $ReqI = 0;               # 0 unless this is a reply to an TINY_REO request
    public $NumP;               # number of players in race
    public $PLID;               # all PLIDs in new order

    public function unpack($rawPacket) {
        $pkClass = unpack($this::UNPACK, $rawPacket);
        $pkClass['PLID'] = array();

        for ($Pos = 1; $Pos <= 40; ++$Pos) {
            if ($pkClass["PLID{$Pos}"] != 0) {
                $pkClass['PLID'][$Pos] = $pkClass["PLID{$Pos}"];
            }
            unset($pkClass["PLID{$Pos}"]);
        }

        foreach ($pkClass as $property => $value) {
            if (is_string($value)) {
                $value = trim($value);
            }
            $this->$property = $value;
        }

        return $this;
    }

}
