<?php

namespace Insim\Packets;

/**
 * Button type
 */
class isBTT extends Packet {

    const PACK = 'CCCCCCCxa96';
    const UNPACK = 'CSize/CType/CReqI/CUCID/CClickID/CInst/CTypeIn/CSp3/a96Text';

    protected $Size = 104;      # 104
    protected $Type = Packet::ISP_BTT;  # ISP_BTT
    public $ReqI;               # ReqI as received in the IS_BTN
    public $UCID;               # connection that typed into the button (zero if local)
    public $ClickID;            # button identifier originally sent in IS_BTN
    public $Inst;               # used internally by InSim
    public $TypeIn;             # from original button specification
    protected $Sp3;
    public $Text;               # typed text, zero to TypeIn specified in IS_BTN

}
