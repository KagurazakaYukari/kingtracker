<?php

namespace Insim\Service;

use Application\Service\CoreService;
use cURL\Request;
use Insim\Helper\InSimHelper;
use Insim\Model\LFSWPB;
use Insim\Model\LFSWWR;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;

class LFSWService extends CoreService {

    const LFSW_DELAY = 5;               // LFSW request delay
    const WR_CACHE_LIFETIME = 3600;

    public $tableWR;
    public $tablePB;
    public $lfswUrl;
    public $lfswConfig;
    public $lfswKeys;
    public $lfswKeysCount;
    public $lfswLastKey = 0;
    public $delay;
    protected $lastWRrequest = 0;
    protected $WRcache = array();

    public function __construct(TableGateway $tableGateway) {
        parent::__construct($tableGateway);
        $this->debug = true;

        $this->lfswUrl = 'http://www.lfsworld.net/pubstat/get_stat2.php';
        $this->lfswConfig = array(
            's' => 1, // json
            'c' => 3, // gzip encode
        );

        $this->getKeys();
    }

    public function getRequest($params, &$req = null) {
        if ($this->lfswKeysCount < 1) {
            $this->log('LFSW request error: No ident keys found.');
            return false;
        }

        // Check
        switch ($params['params']['action']) {
            case 'wr':
                $wrDB = $this->getBy(array('track' => $params['params']['track']), $this->tableWR);
                if ($wrDB) {
                    if ((time() - strtotime($wrDB->last_update)) < LFSWService::WR_CACHE_LIFETIME) {
                        $this->log('LFSW cached request WR ' . $params['params']['track']);
                        return false;
                    }
                }
                $this->log('LFSW request WR ' . $params['params']['track']);
                break;
            case 'pb':
                $pbDB = $this->getBy(array('UName' => $params['params']['racer']), $this->tablePB);
                if ($pbDB) {
                    if ((time() - strtotime($pbDB->last_update)) < LFSWService::WR_CACHE_LIFETIME) {
                        $this->log('LFSW cached request PB ' . $params['params']['racer']);
                        return false;
                    }
                }
                $this->log('LFSW request PB ' . $params['params']['racer']);
                break;
            default:
                $this->log('LFSW request error: Unknown action.');
                return false;
        }


        $query_array = array_merge($this->lfswConfig, $params['params']);

        $this->lfswLastKey++;
        if ($this->lfswLastKey >= $this->lfswKeysCount) {
            $this->lfswLastKey = 0;
        }

        $query_array['idk'] = $this->lfswKeys[$this->lfswLastKey];

        $query = http_build_query($query_array);

        $url = $this->lfswUrl . '?' . $query;
        $req = new Request($url);

        return true;
    }

    public function processResponse($req, $json) {
        switch ($req['params']['action']) {
            case 'wr':
                $this->saveWRs($json);
                break;
            case 'pb':
                $this->savePBs($json, $req['params']['racer']);
                break;
        }
    }

    public function saveWRs($json) {
        if (empty($json)) {
            $this->log('LFSW WR response error');
            return;
        }

        $wr = new LFSWWR();
        foreach ($json as $value) {
            $wr->UName = $value['racername'];
            $wr->car = $value['car'];
            $wr->flags = $value['flags_hlaps'];
            $wr->id_wr = $value['id_wr'];
            $wr->last_update = date('Y-m-d H:i:s');

            $wr->splits = array();
            $wr->splits_string = array();

            if (!empty($value['split1'])) {
                $wr->splits[] = $value['split1'];
                $wr->splits_string[] = InSimHelper::timeToString($value['split1']);
            }

            if (!empty($value['split2'])) {
                $wr->splits[] = $value['split2'];
                $wr->splits_string[] = InSimHelper::timeToString($value['split2']);
            }

            if (!empty($value['split3'])) {
                $wr->splits[] = $value['split3'];
                $wr->splits_string[] = InSimHelper::timeToString($value['split3']);
            }

            $wr->time = $value['laptime'];
            $wr->time_string = InSimHelper::timeToString($value['laptime']);
            $wr->timestamp = $value['timestamp'];
            $wr->track = $value['track'];

            $wrDB = $this->getBy(array('track' => $wr->track, 'car' => $wr->car), $this->tableWR);
            if ($wrDB) {
                $wr->id = $wrDB->id;
                $this->save($wr, $this->tableWR);
            } else {
                $this->save($wr, $this->tableWR);
            }
        }
    }

    public function savePBs($json, $UName) {
        if (empty($json)) {
            $this->log('LFSW PB response error: ');
            return;
        }

        $pb = new LFSWPB();
        foreach ($json as $value) {
            $pb->UName = $UName;
            $pb->car = $value['car'];
            $pb->track = $value['track'];
            $pb->fuel = $value['fuel'];
            $pb->lapcount = $value['lapcount'];

            $pb->splits = array();
            $pb->splits_string = array();

            if (!empty($value['split1'])) {
                $pb->splits[] = $value['split1'];
                $pb->splits_string[] = InSimHelper::timeToString($value['split1']);
            }

            if (!empty($value['split2'])) {
                $pb->splits[] = $value['split2'];
                $pb->splits_string[] = InSimHelper::timeToString($value['split2']);
            }

            if (!empty($value['split3'])) {
                $pb->splits[] = $value['split3'];
                $pb->splits_string[] = InSimHelper::timeToString($value['split3']);
            }

            $pb->time = $value['laptime'];
            $pb->time_string = $pb->time ? InSimHelper::timeToString($value['laptime']) : 'Deleted';
            $pb->timestamp = $value['timestamp'];
            $pb->last_update = date('Y-m-d H:i:s');

            $pbDB = $this->getBy(array('track' => $pb->track, 'car' => $pb->car, 'UName' => $pb->UName), $this->tablePB);

            if ($pbDB) {
                $pb->id = $pbDB->id;
                $this->save($pb, $this->tablePB);
            } else {
                $this->save($pb, $this->tablePB);
            }
        }
    }

    public function getKeys() {
        $this->lfswKeys = array();

        $pubstatKeys = $this->fetchAll(false);
        foreach ($pubstatKeys as $key) {
            $this->lfswKeys[] = $key->key;
        }

        $this->lfswKeysCount = count($this->lfswKeys);
        if ($this->lfswKeysCount > 0) {
            $this->delay = ceil(LFSWService::LFSW_DELAY / $this->lfswKeysCount);
        } else {
            $this->delay = 5;
        }
    }

    function findWR($track, $car) {
        $where = array();

        if (isset($car))
            $where['car'] = $car;

        if (isset($track))
            $where['track'] = $track;

        // TODO:Fix notice if no WR data loaded
        if (!isset($this->WRcache[$track][$car]) OR ( time() - $this->lastWRrequest) >= LFSWService::WR_CACHE_LIFETIME) {
            $wr = $this->getBy($where, $this->tableWR);

            if ($wr) {
                $this->WRcache[$wr->track][$wr->car] = $wr;
            }
            $this->lastWRrequest = time();
        }

        return $this->WRcache[$track][$car];
    }

    function fetchWRs($updateCallBack = null, $track = null, $car = null, $UName = null) {
        $wrs = array();
        $where = array();

        if (isset($car))
            $where['car'] = $car;

        if (isset($track))
            $where['track'] = $track;

        if (isset($UName))
            $where['UName'] = $UName;

        $wrs = $this->fetchAllBy($where, $buffered = true, $this->tableWR, 'time ASC');

        if (count($wrs) < 1) {
            if ($updateCallBack)
                $updateCallBack();
        }else {
            $wr = $wrs->current();
            if ((time() - strtotime($wr->last_update)) >= LFSWService::WR_CACHE_LIFETIME) {
                if ($updateCallBack)
                    $updateCallBack();
            }
        }

        return $wrs;
    }

    /**
     * Fetch the best PBs (group by UName)
     * @param array $where = [UName, track, car = []]
     * @param type $updateCallBack
     * @return type
     */
    function fetchBestPBs($where = array(), $conns = null, $updateCallBack = null) {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('lfsw_pb');

        if (isset($where['track'])) {
            $select->where->and->equalTo('track', $where['track']);
        }

        if (isset($where['UName'])) {
            $select->where->and->equalTo('lfsw_pb.UName', $where['UName']);
        }

        if (isset($where['car'])) {
            if (count($where['car']) > 1) {
                $predicate = $select->where->and->NEST;
                foreach ($where['car'] as $key => $state) {
                    if ($state == 1)
                        $predicate->or->equalTo('car', $key);
                    elseif ($state == -1)
                        $predicate->and->notEqualTo('car', $key);
                }
                $predicate->UNNEST;
            }else {
                foreach ($where['car'] as $key => $state) {
                    if ($state == 1)
                        $select->where->and->equalTo('car', $key);
                    elseif ($state == -1)
                        $select->where->and->notEqualTo('car', $key);

                    break;
                }
            }
        }else {
            $select->where->and->greaterThan('time', 0);
        }

        $select->join(array('pl' => 'players'), 'lfsw_pb.UName = pl.UName', array('PName' => 'PName'), Select::JOIN_LEFT);

        if ($conns)
            $select->join(array('conns' => 'hosts_conns'), 'lfsw_pb.UName = conns.UName', array(), Select::JOIN_RIGHT);

        $select->order('time ASC');

        if (isset($where['UName'])) {
            $resultSet = $this->tablePB->selectWith($select);
            return $resultSet->current();
        } else {
            $adapter = $this->tablePB->getAdapter();
            $query = $select->getSqlString($adapter->getPlatform());

            $resultSet = $adapter->query('SELECT * FROM ( ' . $query . ' ) AS tmp_table GROUP BY `UName` ORDER BY `time` ASC', Adapter::QUERY_MODE_EXECUTE);
            $resultSet->buffer();

            // TODO: LFSW request PB update if cached is too old
            if ($conns) {
                foreach ($conns as $conn) {
                    //\Zend\Debug\Debug::dump($conn);
                }
            }
        }

        return $resultSet;
    }

}
