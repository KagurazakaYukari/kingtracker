<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Exception;
use Insim\Model\Host;
use Insim\Model\PlayerClass;
use Insim\Packets\isISI;
use Insim\Packets\isTINY;
use Insim\Packets\Packet;
use Insim\Types\InSimTypes;

class InsimService extends CoreService {

    public $host = false;
    // TCP stream buffer
    private $streamBuf = '';
    private $streamBufLen = 0;
    // Player based packets queue
    private $sendQueue = array();
    // Time (ms) of last sent packet to player
    private $lastSent = array();
    // Host packets queue
    private $hostQueue = array();
    // Time (ms) of last sent packet to host
    private $lastSentHost = 0;
    private $tLastDebug = 0;
    public $hostService;
    public $playerService;
    public $messageService;
    public $requestsService;
    public $uiService;

    public function __construct() {
        $this->debug = true;
    }

    /**
     * Called when socket connection to host was established
     * @param Host $host
     * @return boolean
     */
    public function connect(Host &$host) {
        $this->log('Connecting to: ' . $host->name);
        $this->host = $host;

        // Create socket
        $this->host->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if ($this->host->socket === false) {
            return false;
        }

        socket_set_nonblock($this->host->socket);

        try {
            $this->host->socket = $this->socket_connect_timeout($this->host->ip, $this->host->port, $timeout = 100);
            $this->host->status = InSimTypes::CONN_CONNECTING;
        } catch (Exception $ex) {
            $this->log($this->host->name . ' - ' . $ex->getMessage());
            $this->hostService->host = &$this->host;
            $this->hostService->timeout($ex->getMessage());
            return false;
        }

        // Services
        $this->hostService->host = &$this->host;
        $this->playerService->host = &$this->host;
        $this->host->playerService = &$this->playerService;
        $this->host->hostService = &$this->hostService;
        $this->host->insim = &$this;

        // Send insim init packet
        $ISP = new isISI();
        $ISP->ReqI = true;
        $ISP->UDPPort = 0;
        $ISP->Flags = InSimTypes::ISF_MCI + InSimTypes::ISF_HLV + InSimTypes::ISF_CON + InSimTypes::ISF_OBH;

        if ($host->local)
            $ISP->Flags += 4;

        $ISP->Prefix = ord('!'); //_
        $ISP->Interval = 250;
        $ISP->Admin = $this->host->passwd;
        $ISP->IName = 'KingTracker';

        $this->send($ISP);

        return true;
    }

    /**
     * Send packet
     * @param Packet $packet
     */
    public function send(Packet $packet) {
        //$this->hostQueue[] = $packet->pack();
        socket_write($this->host->socket, $packet->pack());
    }

    /**
     * Add packet to send queue
     * @param Packet $packet
     * @param PlayerClass $player
     */
    public function sendQued(Packet $packet, PlayerClass $player) {
        $this->sendQueue[$player->UCID][] = $packet;
    }

    /**
     * Main loop
     * @return type
     */
    public function run() {
        if (!($this->host->status == InSimTypes::CONN_CONNECTED OR $this->host->status == InSimTypes::CONN_CONNECTING))
            return;

        //$this->tLastDebug = time();

        $read = array($this->host->socket);
        $write = array();
        $except = array($this->host->socket);

        $num_changed_sockets = @socket_select($read, $write, $except, 0);

        if ($num_changed_sockets === false) {
            $message = str_replace(PHP_EOL, '', socket_strerror(socket_last_error($this->host->socket)));
            $this->log('Socket error: ' . $this->host->name . ' - ' . $message);
            $this->hostService->timeout($message);
            $this->sendQueue = array();
            $this->lastSent = array();
            $this->hostQueue = array();
            $this->lastSentHost = 0;
            return;
        } else if ($num_changed_sockets > 0) {
            if (($rawPacket = @socket_read($this->host->socket, 8192)) !== FALSE) {
                $this->appendToBuffer($rawPacket);
                while (true) {
                    $packet = $this->findNextPacket();
                    if (!$packet)
                        break;

                    // Handle received packet
                    $this->handlePacket($packet);
                }
            } else {
                $message = str_replace(PHP_EOL, '', socket_strerror(socket_last_error($this->host->socket)));
                $this->log('Socket error: ' . $this->host->name . ' - ' . $message);
                $this->hostService->timeout($message);
                $this->sendQueue = array();
                $this->lastSent = array();
                $this->hostQueue = array();
                $this->lastSentHost = 0;
                return;
            }
        }

        // TODO: BUG Send queued packets to host
        /* foreach ($this->hostQueue as $key => $packet) {
          if ((microtime(true) * 1000) - $this->lastSentHost >= InSimTypes::DELAY_PACKET_SEND) {
          socket_write($this->host->socket, $packet);
          unset($this->hostQueue[$key]);
          $this->lastSentHost = microtime(true) * 1000;
          }
          } */

        // Send queued packets to players
        foreach ($this->sendQueue as $ucid => $que) {
            if (count($que) > 0) {
                $lastSent = isset($this->lastSent[$ucid]) ? $this->lastSent[$ucid] : 0;
                if ((microtime(true) * 1000) - $lastSent >= InSimTypes::DELAY_PACKET_SEND) {
                    $temp = array_shift($this->sendQueue[$ucid]);
                    socket_write($this->host->socket, $temp->pack());

                    $this->lastSent[$ucid] = microtime(true) * 1000;
                }
            }
        }

        $this->playerService->update();
        $this->hostService->update();
    }

    /**
     * Dispatch received packets to services
     * @param type $packet
     * @return boolean
     */
    public function handlePacket($packet) {
// Check packet size
        if ((strlen($packet) % 4) > 0) {
            $this->clearBuffer();
            return false;
        }

# Parse Packet Header
        $header = unpack('CSize/CType/CReqI/CSubT', $packet);

        $this->maintainConnection($header, $packet);

// Debug recived packet
        if ($header['Type'] != 38 && $header['Type'] != 3) {
            $this->log('Packet received: ' . Packet::getPacketType($header['Type']) . ' (' . $this->host->name . ')');
        }


        $this->host->playerService->handlePacket($header, $packet);
        $this->hostService->handlePacket($header, $packet);
    }

    /**
     * Maintain insim connection
     * @param type $header
     * @param type $rawPacket
     */
    private function maintainConnection($header, $rawPacket) {
        switch ($header['Type']) {
            case Packet::ISP_VER:
                $this->hostService->connected($this->host, $rawPacket, $this);
                break;
            case Packet::ISP_TINY:
                if ($header['SubT'] == isTINY::TINY_NONE) {   // Ping back
                    $ping = new isTINY();
                    $ping->SubT = isTINY::TINY_NONE;
                    $this->send($ping);

                    $this->host->lastPing = time();
                }
                break;
        }

// TimeOut detecion
        if ($this->host->lastPing != 0 && (time() - $this->host->lastPing) >= InSimTypes::KEEP_ALIVE_TIME) {
// TODO: Server missed ping, try to recover
            $this->getHostService()->timeout('Timeout');
        }
    }

    /**
     * Try to open socket connection to host
     * @param type $host
     * @param type $port
     * @param type $timeout
     * @return type
     * @throws Exception
     */
    private function socket_connect_timeout($host, $port, $timeout = 100) {
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        /**
         * Set the send and receive timeouts super low so that socket_connect
         * will return to us quickly. We then loop and check the real timeout
         * and check the socket error to decide if its conected yet or not.
         */
        $connect_timeval = array(
            "sec" => 0,
            "usec" => 100
        );
        socket_set_option(
                $socket, SOL_SOCKET, SO_SNDTIMEO, $connect_timeval
        );
        socket_set_option(
                $socket, SOL_SOCKET, SO_RCVTIMEO, $connect_timeval
        );
        $now = microtime(true);
        /**
         * Loop calling socket_connect. As long as the error is 115 (in progress)
         * or 114 (already called) and our timeout has not been reached, keep
         * trying.
         */
        $err = null;
        $socket_connected = false;
        do {
            socket_clear_error($socket);
            $socket_connected = @socket_connect($socket, $host, $port);
            $err = socket_last_error($socket);
            $elapsed = (microtime(true) - $now) * 1000;
        } while (($err === 115 || $err === 114) && $elapsed < $timeout);
        /**
         * For some reason, socket_connect can return true even when it is
         * not connected. Make sure it returned true the last error is zero
         */
        $socket_connected = $socket_connected && $err === 0;
        if ($socket_connected) {
            /**
             * Set keep alive on so the other side does not drop us
             */
            socket_set_option($socket, SOL_SOCKET, SO_KEEPALIVE, 1);
            /**
             * set the real send/receive timeouts here now that we are connected
             */
            $timeval = array(
                "sec" => 0,
                "usec" => 0
            );
            if ($timeout >= 1000) {
                $ts_seconds = $timeout / 1000;
                $timeval["sec"] = floor($ts_seconds);
                $timeval["usec"] = ($ts_seconds - $timeval["sec"]) * 1000000;
            } else {
                $timeval["usec"] = $timeout * 1000;
            }
            socket_set_option(
                    $socket, SOL_SOCKET, SO_SNDTIMEO, $timeval
            );
            socket_set_option(
                    $socket, SOL_SOCKET, SO_RCVTIMEO, $timeval
            );
        } else {
            $elapsed = round($elapsed, 4);
            if (!is_null($err) && $err !== 0 && $err !== 114 && $err !== 115) {
                $msg = str_replace(PHP_EOL, '', socket_strerror($err));
                $message = "Failed to connect to {$this->host->name}. ({$err}: {$msg})";
            } else {
                $message = "Failed to connect to {$this->host->name}. (timed out after {$elapsed}ms)";
            }
            throw new Exception($message);
        }
        return $socket;
    }

    public function appendToBuffer(&$data) {
        $this->streamBuf .= $data;
        $this->streamBufLen = strlen($this->streamBuf);
    }

    public function clearBuffer() {
        $this->streamBuf = '';
        $this->streamBufLen = 0;
    }

    public function findNextPacket() {
        if ($this->streamBufLen == 0)
            return FALSE;

        $sizebyte = ord($this->streamBuf[0]);
        if ($sizebyte == 0) {
            return FALSE;
        } else if ($this->streamBufLen < $sizebyte) {
//console('Split packet ...');
            return FALSE;
        }

// We should have a whole packet in the buffer now
        $packet = substr($this->streamBuf, 0, $sizebyte);
        $packetType = ord($packet[1]);

// Cleanup streamBuffer
        $this->streamBuf = substr($this->streamBuf, $sizebyte);
        $this->streamBufLen = strlen($this->streamBuf);

        return $packet;
    }

    public function setRequestService(&$requestsService) {
        $this->requestsService = $requestsService;
        $this->requestsService->insim = &$this;
    }

    public function setHostService(HostService $service) {
        $this->hostService = clone $service;
    }

    public function setPlayersService(PlayerService $service) {
        $this->playerService = clone $service;
    }

}
