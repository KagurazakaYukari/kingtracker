<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Insim\Helper\InSimHelper;
use Insim\Model\LapTop;
use Insim\Model\Stint;
use Insim\Packets\isCON;
use Insim\Packets\isCRS;
use Insim\Packets\isHLV;
use Insim\Packets\isLap;
use Insim\Packets\isOBH;
use Insim\Packets\isSPX;
use Insim\Types\MsgTypes;
use Insim\Types\NotifyTypes;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;

class LapsService extends CoreService {

    public $player;
    public $current_splits = array();
    public $splits_count;
    public $clean_lap;
    public $clean_next_lap;
    public $last_hlvc = '';
    public $next_lap_check = false;
    public $stint;
    public $LFSWService;
    public $debugSQL = false;

    public function __construct(TableGateway $tableGateway) {
        parent::__construct($tableGateway);
        $this->debug = true;
    }

    public function newPlayerEvent() {
        if (!isset($this->stint)) {
            $this->stint = new Stint();
            $this->stint->player = &$this->player;
        }

        $this->splits_count = 0;

        $this->clean_lap = true;
        $this->clean_next_lap = true;
        $this->next_lap_check = false;
        $this->last_hlvc = '';

        // Update debug UI
        if (isset($this->player->ui->ui_list['debug_lap']) && $this->player->ui->ui_list['debug_lap']->displayed) {
            $this->player->ui->ui_list['debug_lap']->updateHLVC(array(
                'lap' => $this->clean_lap,
                'lap_next' => $this->clean_next_lap,
                'last_hlvc' => $this->last_hlvc
            ));
        }
    }

    public function playerLeaveEvent() {
        $this->stint->eventPlayersSpectated();
    }

    public function splitEvent(isSPX $split) {
        if ($split->STime == 3600000) {
            // Warmup lap
        } else {
            $this->current_splits[$split->Split] = $split->STime;

            //TODO: Open tracks compare with best local time on layout;
            $wr = $this->LFSWService->findWR($this->player->host->TrackInfo->LFSWid, $this->player->pll->getCarName());
            if ($wr && isset($wr->splits[$split->Split - 1]) && !$this->player->host->TrackInfo->isOpen) {
                $rating = $this->compareToWR($split->STime, $wr->splits[$split->Split - 1], false);
                if (!empty($rating)) {
                    $car = $this->player->pll->getCarName();
                    $time = InSimHelper::timeToString($split->STime);

                    // Show split notification
                    $rt = strtolower($rating);
                    if (isset($this->player->playerService->players[$this->player->host->id])) {
                        foreach ($this->player->playerService->players[$this->player->host->id] as $pl) {
                            $rating_string = $pl->translator->translateLFS(strtoupper('SPLIT_' . $rating));
                            $msg = "^8{$rating_string} {$split->Split} - " . $this->player->name . " ^7: {$time} ^8($car)";

                            switch ($pl->settings->get('ntf_split_' . $rt)) {
                                case NotifyTypes::YES:
                                    $pl->sendMsg($msg);
                                    break;
                                case NotifyTypes::OWN:
                                    if ($pl->PLID == $split->PLID) {
                                        $pl->sendMsg($msg);
                                    }
                                    break;
                            }
                        }
                    }
                }
            }

            // Stint
            if (isset($this->stint)) {
                $this->stint->addSplit($split);
            }

            // Results event
            $this->player->host->results->addEventSplit($split, $this->player->UName);
        }

        // Check next lap clean validity after last sector
        if ($split->Split == $this->player->host->splitsCount) {
            $this->next_lap_check = true;
        }
    }

    public function lapEvent(isLap $packet) {
        $time = InSimHelper::timeToString($packet->LTime);
        $saveLap = true;

        // Warmup lap
        if ($packet->LTime == 3600000) {
            $saveLap = false;
        }

        // Wind check
        if ($this->player->host->Wind > 0) {
            $saveLap = false;
        }

        // Check if all splits count matches
        if ($this->player->host->splitsCount != count($this->current_splits)) {
            $saveLap = false;
        }

        if ($saveLap) {
            // Save lap
            $lap = new LapTop();
            $lap->player_id = $this->player->player_id;
            $lap->track = $this->player->host->Track;
            $lap->layout = $this->player->host->LName;
            $lap->PFlags = $this->player->pll->Flags;
            $lap->car = $this->player->pll->CName;
            $lap->H_Mass = $this->player->pll->H_Mass;
            $lap->H_TRes = $this->player->pll->H_TRes;
            $lap->SFlags = $this->player->pll->SetF;
            $lap->Tyres = implode('-', $this->player->pll->Tyres);
            $lap->setPFlags($lap->PFlags);
            $lap->setSFlags($lap->SFlags);

            $sectors = array();
            $splits = array();

            foreach ($this->current_splits as $key => $t) {
                $splits[$key] = InSimHelper::timeToString($t);

                if ($key == 1) {
                    @$sectors[$key] = InSimHelper::timeToString($t);
                } else {
                    @$sectors[$key] = InSimHelper::timeToString($t - $this->current_splits[$key - 1]);
                }
            }

            $lap->clean = $this->clean_lap;
            $lap->non_clean_reason = '';

            $lap->sectors = $sectors;
            $lap->splits = $splits;
            $lap->time = $packet->LTime;
            $lap->time_string = $time;
            $lap->updated = date('Y-m-d H:i:s');

            // Find player's lap with same conditions
            $lapDB = $this->findLapByConditions($lap, false);

            if ($lapDB) {
                if ($lap->time < $lapDB->time) {
                    // Better lap then saved - update
                    $lap->id = $lapDB->id;
                    $this->save($lap);
                    if ($this->clean_lap)
                        $this->player->sendTranslatedMsg('MSG_LAP_TIME_UPDATE', MsgTypes::NOTIFY);
                } else {
                    // Worst or same lap like saved
                }
            } else {
                // First PB in top laps
                $this->save($lap);
                if ($this->clean_lap)
                    $this->player->sendTranslatedMsg('MSG_LAP_TIME_SAVED', MsgTypes::NOTIFY);
            }

            $wr = $this->LFSWService->findWR($this->player->host->TrackInfo->LFSWid, $this->player->pll->getCarName());
            if ($wr && !$this->player->host->TrackInfo->isOpen) {
                $rating = $this->compareToWR($packet->LTime, $wr->time, true);
                if (!empty($rating)) {
                    $car = $this->player->pll->getCarName();

                    // Show split notification
                    $rt = strtolower($rating);
                    if (isset($this->player->playerService->players[$this->player->host->id])) {
                        foreach ($this->player->playerService->players[$this->player->host->id] as $pl) {
                            $rating_string = $pl->translator->translateLFS(strtoupper('LAP_' . $rating));
                            $msg = "^8{$rating_string} - " . $this->player->name . " ^7: {$lap->time_string} ^8($car)";

                            switch ($pl->settings->get('ntf_lap_' . $rt)) {
                                case NotifyTypes::YES:
                                    $pl->sendMsg($msg);
                                    break;
                                case NotifyTypes::OWN:
                                    if ($pl->PLID == $lap->PLID) {
                                        $pl->sendMsg($msg);
                                    }
                                    break;
                            }
                        }
                    }
                }
            }

            // Results event
            $this->player->host->results->addEventLap($packet, $this->player->UName);
        }

        // Stint
        if (isset($this->stint) && $packet->LTime != 3600000) {
            $this->stint->addLap($packet);
        }


        // Next lap
        $this->current_splits = array();
        $this->clean_lap = $this->clean_next_lap;
        $this->clean_next_lap = true;
        $this->next_lap_check = false;
        $this->last_hlvc = '';

        // Update debug UI
        if (isset($this->player->ui->ui_list['debug_lap']) && $this->player->ui->ui_list['debug_lap']->displayed) {
            $this->player->ui->ui_list['debug_lap']->updateHLVC(array(
                'lap' => $this->clean_lap,
                'lap_next' => $this->clean_next_lap,
                'last_hlvc' => $this->last_hlvc
            ));
        }
    }

    public function hlvcEvent(isHLV $hlvc) {
        switch ($hlvc->HLVC) {
            case isHLV::HLVC_GROUND:
                $this->hlvcGround($hlvc);
                break;
            case isHLV::HLVC_WALL:
                $this->hlvcWall($hlvc);
                break;
            case isHLV::HLVC_SPEEDING:
                $this->hlvcPitSpeeding($hlvc);
                break;
            case isHLV::HLVC_BOUNDS:
                $this->hlvcBounds($hlvc);
                break;
        }

        if ($this->next_lap_check && $this->clean_next_lap && $this->player->settings->get('ntf_hlvc')) {
            $this->player->sendMsg('HLVC NEXT LAP invalid', MsgTypes::NOTIFY);
        }

        // Invalidate this lap
        $this->clean_lap = false;
        $this->last_hlvc = $hlvc->typeToString();

        // Invalidate next lap
        if ($this->next_lap_check)
            $this->clean_next_lap = false;

        // Update debug UI
        if (isset($this->player->ui->ui_list['debug_lap']) && $this->player->ui->ui_list['debug_lap']->displayed) {
            $this->player->ui->ui_list['debug_lap']->updateHLVC(array(
                'lap' => $this->clean_lap,
                'lap_next' => $this->clean_next_lap,
                'last_hlvc' => $this->last_hlvc
            ));
        }
    }

    public function hlvcPitSpeeding(isHLV $hlvc) {
        if ($this->clean_lap && $this->player->settings->get('ntf_hlvc')) {
            $this->player->sendMsg('HLVC invalid lap (pit speeding)', MsgTypes::NOTIFY);
        }
    }

    public function hlvcWall(isHLV $hlvc) {
        if ($this->clean_lap && $this->player->settings->get('ntf_hlvc')) {
            $this->player->sendMsg('HLVC invalid lap (wall contact)', MsgTypes::NOTIFY);
        }
    }

    public function hlvcGround(isHLV $hlvc) {
        if ($this->clean_lap && $this->player->settings->get('ntf_hlvc')) {
            $this->player->sendMsg('HLVC invalid lap (ground)', MsgTypes::NOTIFY);
        }
    }

    public function hlvcBounds(isHLV $hlvc) {
        if ($this->clean_lap && $this->player->settings->get('ntf_hlvc')) {
            $this->player->sendMsg('HLVC invalid lap (track bounds)', MsgTypes::NOTIFY);
        }
    }

    public function draftEvent() {
        if ($this->clean_lap && $this->player->settings->get('ntf_hlvc')) {
            $this->player->sendMsg('HLVC invalid lap (slipstream)', MsgTypes::NOTIFY);
        }

        if ($this->next_lap_check && $this->clean_next_lap && $this->player->settings->get('ntf_hlvc')) {
            $this->player->sendMsg('HLVC NEXT LAP invalid', MsgTypes::NOTIFY);
        }

        // Invalidate this lap
        $this->clean_lap = false;

        // Invalidate next lap
        if ($this->next_lap_check)
            $this->clean_next_lap = false;

        // Update debug UI
        if (isset($this->player->ui->ui_list['debug_lap']) && $this->player->ui->ui_list['debug_lap']->displayed) {
            $this->player->ui->ui_list['debug_lap']->updateHLVC(array(
                'lap' => $this->clean_lap,
                'lap_next' => $this->clean_next_lap,
                'last_hlvc' => $this->last_hlvc
            ));
        }
    }

    public function contactCarEvent(isCON $con) {
        if ($this->clean_lap && $this->player->settings->get('ntf_hlvc')) {
            $this->player->sendMsg('HLVC invalid lap (car contact)', MsgTypes::NOTIFY);
        }

        if ($this->next_lap_check && $this->clean_next_lap && $this->player->settings->get('ntf_hlvc')) {
            $this->player->sendMsg('HLVC NEXT LAP invalid', MsgTypes::NOTIFY);
        }

        // Invalidate this lap
        $this->clean_lap = false;

        // Invalidate next lap
        if ($this->next_lap_check)
            $this->clean_next_lap = false;

        // Update debug UI
        $this->last_hlvc = 'car contact';
        if (isset($this->player->ui->ui_list['debug_lap']) && $this->player->ui->ui_list['debug_lap']->displayed) {
            $this->player->ui->ui_list['debug_lap']->updateHLVC(array(
                'lap' => $this->clean_lap,
                'lap_next' => $this->clean_next_lap,
                'last_hlvc' => $this->last_hlvc
            ));
        }
    }

    public function carResetEvent(isCRS $crs) {
        if ($this->clean_lap && $this->player->settings->get('ntf_hlvc')) {
            $this->player->sendMsg('HLVC invalid lap (car reset)', MsgTypes::NOTIFY);
        }

        if (isset($this->stint)) {
            $this->stint->eventCarResetEvent();
        }

        // Invalidate this lap
        $this->clean_lap = false;

        // Update debug UI
        $this->last_hlvc = 'car reset';
        if (isset($this->player->ui->ui_list['debug_lap']) && $this->player->ui->ui_list['debug_lap']->displayed) {
            $this->player->ui->ui_list['debug_lap']->updateHLVC(array(
                'lap' => $this->clean_lap,
                'lap_next' => $this->clean_next_lap,
                'last_hlvc' => $this->last_hlvc
            ));
        }
    }

    public function objectContactEvent(isOBH $obh) {
        // Invalidate only if object wasnt moving
        if ($obh->wasMoving()) {
            return;
        }

        if ($this->clean_lap && $this->player->settings->get('ntf_hlvc')) {
            $this->player->sendMsg('HLVC invalid lap (object contact)', MsgTypes::NOTIFY);
        }

        if ($this->next_lap_check && $this->clean_next_lap) {
            $this->player->sendMsg('HLVC NEXT LAP invalid', MsgTypes::NOTIFY);
        }

        // Invalidate this lap
        $this->clean_lap = false;

        // Invalidate next lap
        if ($this->next_lap_check)
            $this->clean_next_lap = false;

        // Update debug UI
        $this->last_hlvc = 'object contact';
        if (isset($this->player->ui->ui_list['debug_lap']) && $this->player->ui->ui_list['debug_lap']->displayed) {
            $this->player->ui->ui_list['debug_lap']->updateHLVC(array(
                'lap' => $this->clean_lap,
                'lap_next' => $this->clean_next_lap,
                'last_hlvc' => $this->last_hlvc
            ));
        }
    }

    public function eventTrackChanged() {
        if (isset($this->stint)) {
            $this->stint->eventTrackChanged();
        }
    }

    public function eventRaceStarted() {
        if (!isset($this->stint)) {
            $this->stint = new Stint();
            $this->stint->player = &$this->player;
            $this->stint->splits_count = $this->player->host->splitsCount;
        }

        $this->stint->eventRaceStarted();
    }

    function compareToWR($timeA, $timeB, $lapTime = false) {
        $ntf_good = $this->player->host->settings->get('ntf_lap_good');
        $ntf_great = $this->player->host->settings->get('ntf_lap_great');
        $ntf_top = $this->player->host->settings->get('ntf_lap_top');
        $ntf_wr = $this->player->host->settings->get('ntf_lap_wr');

        $ratio = round(((int) $timeA / (int) $timeB), 4) - 1;

        switch (true) {
            case ($ratio < $ntf_good && $ratio >= $ntf_great):
                $string = 'Good';
                break;
            case ($ratio < $ntf_great && $ratio >= $ntf_top):
                $string = 'Great';
                break;
            case ($ratio < $ntf_top && $ratio >= $ntf_wr):
                $string = 'TOP';
                break;
            case ($ratio < $ntf_wr):
                $string = 'WR';
                break;
            default:
                $string = '';
        }

        return $string;
    }

    // DB functions

    /**
     * Fetch all laps by conditions
     * @param LapTop $lap - conditions
     * @param type $all
     * @param type $filter
     * @param type $search
     * @return type
     */
    function findLapByConditions(LapTop $lap = null, $all = true, $filter = array(), $search = array()) {
        $condition = array();

        if ($lap)
            $condition = $lap->exchangeObject();

        $colums = array('*');
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('laps');

        foreach ($condition as $key => $value) {
            if (empty($value)) {
                continue;
            }
            switch ($key) {
                case 'player_id':
                case 'track':
                case 'layout':
                case 'H_Mass':
                case 'H_TRes':
                case 'PFlags':
                case 'SFlags':
                //case 'Tyres':
                case 'clean':
                    $select->where->and->equalTo($key, $value);
                    break;
            }
        }

        if (count($filter) > 0) {

            if (isset($filter['TyresPos'])) {
                $temp = $filter['TyresPos'];

                if (isset($temp['FRONT'])) {
                    $tyresPos = 'FRONT';
                }
                if (isset($temp['REAR'])) {
                    $tyresPos = 'REAR';
                }

                if (count($temp) == 2) {
                    $tyresPos = 'ALL';
                }

                unset($filter['TyresPos']);
            } else {
                $tyresPos = 'ANY';
            }

            if (isset($filter['Tyres'])) {
                $tyres = $filter['Tyres'];
                unset($filter['Tyres']);
            }

            if (isset($tyres)) {
                foreach ($tyres as $value => $state) {
                    $not = $state == -1 ? 'NOT_' : '';
                    switch ($tyresPos) {
                        case 'ALL':

                            $filter['Tyres']["{$not}EQUAL"][] = "{$value}-{$value}-{$value}-{$value}";
                            break;
                        case 'FRONT':
                            $filter['Tyres']["{$not}LIKE"][] = "%-{$value}-{$value}";
                            break;
                        case 'REAR':
                            $filter['Tyres']["{$not}LIKE"][] = "{$value}-{$value}-%";
                            break;
                        case 'ANY':
                            $filter['Tyres']["{$not}LIKE"][] = "%{$value}-{$value}%";
                            break;
                    }
                }
            }

            //\Zend\Debug\Debug::dump($filter);

            foreach ($filter as $section => $keys) {
                if ($section == 'car') {
                    if (count($keys) > 1) {
                        $predicate = $select->where->and->NEST;
                        foreach ($keys as $key => $state) {
                            if ($state == 1)
                                $predicate->or->equalTo('car', $key);
                            elseif ($state == -1)
                                $predicate->and->notEqualTo('car', $key);
                        }
                        $predicate->UNNEST;
                    }else {
                        foreach ($keys as $key => $state) {
                            if ($state == 1)
                                $select->where->and->equalTo('car', $key);
                            elseif ($state == -1)
                                $select->where->and->notEqualTo('car', $key);

                            break;
                        }
                    }
                }
                elseif ($section == 'Tyres') {
                    $predicate = $select->where->and->NEST;

                    if (isset($keys['EQUAL'])) {
                        $prTyres = $predicate->and->NEST;
                        foreach ($keys['EQUAL'] as $value) {
                            $prTyres->or->equalTo('Tyres', $value);
                        }
                        $prTyres->UNNEST;
                    }

                    if (isset($keys['NOT_EQUAL'])) {
                        $prTyres = $predicate->and->NEST;
                        foreach ($keys['NOT_EQUAL'] as $value) {
                            $prTyres->or->notEqualTo('Tyres', $value);
                        }
                        $prTyres->UNNEST;
                    }

                    if (isset($keys['LIKE'])) {
                        $prTyres = $predicate->and->NEST;
                        foreach ($keys['LIKE'] as $value) {
                            $prTyres->or->like('Tyres', $value);
                        }
                        $prTyres->UNNEST;
                    }

                    if (isset($keys['NOT_LIKE'])) {
                        $prTyres = $predicate->and->NEST;
                        foreach ($keys['NOT_LIKE'] as $value) {
                            $prTyres->or->notLike('Tyres', $value);
                        }
                        $prTyres->UNNEST;
                    }

                    $predicate->UNNEST;
                } else {
                    if (count($keys) > 1) {
                        $predicate = $select->where->and->NEST;
                        foreach ($keys as $key => $state) {
                            if ($state == 1)
                                $predicate->or->equalTo($key, 1);
                            elseif ($state == -1)
                                $predicate->and->equalTo($key, 0);
                        }
                        $predicate->UNNEST;
                    }else {
                        foreach ($keys as $key => $state) {
                            if ($state == 1)
                                $select->where->and->equalTo($key, 1);
                            elseif ($state == -1)
                                $select->where->and->equalTo($key, 0);

                            break;
                        }
                    }
                }
            }
        }

        if (count($search) > 0) {
            if (isset($search['PName_utf'])) {
                $select->where->and->like('PName_utf', '%' . $search['PName_utf'] . '%');
            }
        }


        $select->join(array('pl' => 'players'), 'laps.player_id = pl.id', array('PName' => 'PName'), Select::JOIN_LEFT);

        $select->columns($colums);
        $select->order('laps.time ASC');
        //$select->group('player_id');
        //$resultSet = $this->tableGateway->selectWith($select);



        if ($all) {
            $adapter = $this->tableGateway->getAdapter();
            $query = $select->getSqlString($adapter->getPlatform());
            $final_query = "SELECT * FROM ({$query}) AS tmp_table GROUP BY `player_id` ORDER BY `time` ASC";
            if ($this->debugSQL) {
                \Zend\Debug\Debug::dump($final_query);
            }
            $resultSet = $adapter->query('SELECT * FROM ( ' . $query . ' ) AS tmp_table GROUP BY `player_id` ORDER BY `time` ASC', Adapter::QUERY_MODE_EXECUTE);
            return $resultSet;
        } else {
            $resultSet = $this->tableGateway->selectWith($select);
            return $resultSet->current();
        }
    }

    /**
     * Fetch all laps by
     * @param type $where
     * @param type $buffered
     * @param type $tableGateway
     * @param type $order
     * @param type $limit
     */
    function fetchAllBy($where = array(), $buffered = false, $tableGateway = false, $order = null, $limit = null) {
        $table = $tableGateway ? $tableGateway : $this->tableGateway;
        $resultSet = $table->select(function(Select $select) use ($where, $order, $limit) {
            foreach ($where as $key => $value) {
                switch ($key) {
                    case 'updated':
                    case 'added':
                        if (strpos($value, '=') !== false OR strpos($value, '<') !== false OR strpos($value, '>') !== false) {
                            $select->where->and->expression("{$key} {$value} ?", '');
                        } else {
                            $select->where->and->equalTo($key, $value);
                        }
                        break;
                    default:
                        $select->where->and->equalTo($key, $value);
                }
            }

            $select->join(array('pl' => 'players'), 'laps.player_id = pl.id', array('PName' => 'PName'), Select::JOIN_LEFT);

            if ($order)
                $select->order($order);

            if ($limit)
                $select->limit($limit);

            if ($this->debugSQL) {
                \Zend\Debug\Debug::dump($select->getSqlString($this->tableGateway->getAdapter()->getPlatform()));
            }
        });

        if ($buffered)
            $resultSet->buffer();
        return $resultSet;
    }

    function getPBs() {
        
    }

    function getPB() {
        return false;
    }

}
