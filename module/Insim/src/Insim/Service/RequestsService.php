<?php

namespace Insim\Service;

use Application\Service\CoreService;

class RequestsService extends CoreService {

    const IP_LOC_DELAY = 1;     // IP location 60 request per minute
    const DOWNLOAD_DELAY = 10;  // Delay between dowloading

    public $insim = null;
    public $ipLocRequests = array();
    public $ipLocRequest = false;
    protected $last_ip_loc = 0;
    public $lfswRequests = array();
    public $lfswRequest = false;
    protected $last_lfsw_req = 0;
    public $LFSWService;
    protected $last_update_check = 0;
    protected $last_download = 0;
    public $updateCheckRequest = false;
    public $updateService;
    public $downloadRequests = array();
    public $downloadRequest = false;

    public function __construct() {
        $this->debug = true;
    }

    public function ipLocationRequest($host_id, $UName, $IPAddress) {
        $this->ipLocRequests[] = array('host_id' => $host_id, 'UName' => $UName, 'ip' => $IPAddress);
    }

    public function ipLocationResponse($host_id, $UName, $json) {
        if (isset($json['status']) && $json['status'] == 'success') {
            $this->insim->playerService->saveIP($host_id, $UName, $json);
        }
    }

    public function LFSWwrRequest($host_id, \Insim\Model\Track $track) {
        if ($track->LFSWid == null)
            return false;
        
        array_unshift($this->lfswRequests, array('host_id' => $host_id, 'params' => array('action' => 'wr', 'track' => $track->LFSWid)));
    }
    
    public function LFSWpbRequest($host_id, $player){
        $this->lfswRequests[] = array('host_id' => $host_id, 'params' => array('action' => 'pb', 'racer' => $player->UName));
    }

    public function updateCheckResponse($json) {
        if (isset($json['status']) && $json['status'] == 'ok') {

            // validate response
            if ($this->updateService->validateResponse($json, $this->downloadRequests)) {
                //\Zend\Debug\Debug::dump($this->downloadRequests);
            }
        }
    }

    public function run() {
        // Check response
        if ($this->ipLocRequest) {
            $this->ipLocRequest->socketPerform();
            if ($this->ipLocRequest)
                $this->ipLocRequest->socketSelect();
        }

        if ($this->updateCheckRequest) {
            $this->updateCheckRequest->socketPerform();
            if ($this->updateCheckRequest)
                $this->updateCheckRequest->socketSelect();
        }

        if ($this->downloadRequest) {
            $this->downloadRequest->socketPerform();
            if ($this->downloadRequest)
                $this->downloadRequest->socketSelect();
        }

        if ($this->lfswRequest) {
            $this->lfswRequest->socketPerform();
            if ($this->lfswRequest)
                $this->lfswRequest->socketSelect();
        }

        // Do IP location requests
        if (count($this->ipLocRequests) > 0) {
            if ((time() - $this->last_ip_loc) >= RequestsService::IP_LOC_DELAY) {
                $req = array_shift($this->ipLocRequests);

                $this->ipLocRequest = new \cURL\Request('http://ip-api.com/json/' . $req['ip'] . '?fields=49921');
                $this->ipLocRequest->getOptions()
                        ->set(CURLOPT_TIMEOUT, 10)
                        ->set(CURLOPT_RETURNTRANSFER, true);
                $this->ipLocRequest->addListener('complete', function (\cURL\Event $event) use($req) {
                    $response = $event->response;
                    $json = json_decode($response->getContent(), true);

                    $this->ipLocationResponse($req['host_id'], $req['UName'], $json);
                    $this->ipLocRequest = false;
                });

                $this->log('IP location request: ' . $req['ip']);
                $this->last_ip_loc = time();
            }
        }

        // Check for update
        if ((time() - $this->last_update_check >= 10800) /*&& date('H:i') == UpdateService::UPDATE_TIME*/) {

            $this->updateCheckRequest = $this->updateService->updateRequest();

            if ($this->updateCheckRequest) {
                $this->updateCheckRequest->getOptions()
                        ->set(CURLOPT_TIMEOUT, 10)
                        ->set(CURLOPT_RETURNTRANSFER, true)
                        ->set(CURLOPT_SSL_VERIFYPEER, false);

                $this->updateCheckRequest->addListener('complete', function (\cURL\Event $event) {
                    $response = $event->response;
                    $json = json_decode($response->getContent(), true);
                    $this->updateCheckResponse($json);
                    $this->updateCheckRequest = false;
                });

                $this->log('Update check request');

                $this->last_update_check = time();
            }
        }

        // Download updates
        if (count($this->downloadRequests) > 0) {
            if ((time() - $this->last_download) >= RequestsService::DOWNLOAD_DELAY) {
                $this->log('Download request: ' . count($this->downloadRequests));
                $req = array_shift($this->downloadRequests);

                $fp = fopen($req['path'], 'w+');

                $userAgent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36';

                $this->downloadRequest = new \cURL\Request($req['url']);
                $this->downloadRequest->getOptions()
                        ->set(CURLOPT_TIMEOUT, 60)
                        ->set(CURLOPT_RETURNTRANSFER, true)
                        ->set(CURLOPT_FILE, $fp)
                        ->set(CURLOPT_SSL_VERIFYPEER, false)
                        ->set(CURLOPT_FOLLOWLOCATION, true)
                        ->set(CURLOPT_USERAGENT, $userAgent);
                $this->downloadRequest->addListener('complete', function (\cURL\Event $event) use ($fp) {
                    fclose($fp);

                    if (count($this->downloadRequests) == 0) {
                        $this->updateService->installUpdates();
                    }

                    $this->downloadRequest = false;
                });

                $this->last_download = time();
            }
        }

        // Do LFSW requests
        if (count($this->lfswRequests) > 0) {
            if ((time() - $this->last_lfsw_req) >= $this->LFSWService->delay) {
                $req = array_shift($this->lfswRequests);

                $doRequest = $this->LFSWService->getRequest($req, $this->lfswRequest);
                if ($doRequest) {

                    $this->lfswRequest->getOptions()
                            ->set(CURLOPT_TIMEOUT, 10)
                            ->set(CURLOPT_ENCODING, 'gzip,deflate')
                            ->set(CURLOPT_RETURNTRANSFER, true);
                    $this->lfswRequest->addListener('complete', function (\cURL\Event $event) use($req) {
                        $response = $event->response;
                        $json = json_decode(gzinflate($response->getContent()), true);

                        $this->LFSWService->processResponse($req, $json);

                        // Update UI
                        switch ($req['params']['action']) {
                            case 'wr':
                                if(isset($this->insim->playerService->players[$this->insim->host->id])){
                                    foreach ($this->insim->playerService->players[$this->insim->host->id] as $player) {
                                        if (isset($player->ui->ui_list['track']) && $player->ui->ui_list['track']->displayed) {
                                            $player->ui->UpdateTrackWR();
                                        }
                                    }
                                }
                                break;
                        }


                        $this->lfswRequest = false;
                    });

                    $this->last_lfsw_req = time();
                }
            }
        }
    }

}
