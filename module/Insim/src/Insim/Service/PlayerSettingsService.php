<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Zend\Db\TableGateway\TableGateway;
use Zend\Debug\Debug;

class PlayerSettingsService extends CoreService {

    public $player_id = 0;
    protected $cache = null;

    public function __construct(TableGateway $tableGateway) {
        $this->debug = true;
        parent::__construct($tableGateway);
    }

    /**
     * Load player settings or create default
     */
    public function load() {
        $where['player_id'] = $this->player_id;
        $settings = $this->fetchAllBy($where, $buffered = false, false);

        // Load default settings
        if (count($settings) < 1) {
            $where['player_id'] = 0;
            $settings = $this->fetchAllBy($where, $buffered = true, false);

            // Save default settings
            foreach ($settings as $setting) {
                $setting->id = 0;
                $setting->player_id = $this->player_id;
                $this->save($setting);
            }
        }

        foreach ($settings as $setting) {
            $this->cache[$setting->name] = $setting->value;
        }
    }

    /**
     * Get single player setting
     * @param type $key
     * @return value
     */
    public function get($key) {
        if (isset($this->cache[$key])) {
            return $this->cache[$key];
        }

        // Create default setting
        $setting = $this->getBy(array('player_id' => 0, 'name' => $key));
        if ($setting) {
            $setting->id = 0;
            $setting->player_id = $this->player_id;
            $this->save($setting);
            
            $this->cache[$key] = $setting->value;
            return $this->cache[$key];
        }

        return false;
    }

    /**
     * Update setting
     * @param type $key
     * @param type $value
     */
    public function update($key, $value) {
        $this->cache[$key] = $value;

        $setting = $this->getBy(array('player_id' => $this->player_id, 'name' => $key));
        if ($setting) {
            $setting->value = $value;
            $this->save($setting);
        }
    }

}
