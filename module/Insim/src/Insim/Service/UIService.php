<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Insim\Model\Host;
use Insim\Packets\isBFN;
use Insim\Packets\isBTC;
use Insim\Packets\isBTT;
use Insim\Packets\isLap;
use Insim\Packets\isSPX;
use Insim\Packets\Packet;
use Insim\Types\GridModes;
use Insim\Types\MsgTypes;
use Insim\UI\UIDebugLap;
use Insim\UI\UIGrid;
use Insim\UI\UIHelp;
use Insim\UI\UIHost;
use Insim\UI\UIPlayersList;
use Insim\UI\UIPlayersPBs;
use Insim\UI\UIPlayersSC;
use Insim\UI\UIRaceControl;
use Insim\UI\UIRecentPlayers;
use Insim\UI\UITimeBar;
use Insim\UI\UITopLaps;
use Insim\UI\UITrack;
use Insim\UI\UITrackInfo;
use Insim\UI\UITrackVote;
use Insim\UI\UIUserSettings;
use Insim\UI\UIVersion;

class UIService extends CoreService {

    protected $tableConns;
    public $lapsService;
    public $trackService;
    public $LFSWService;
    public $player;
    public $players;
    public $ui_list = array();
    public $ui_list_perm = array();

    public function __construct() {
        $this->debug = true;
    }

    public function handlePacket($header, $rawPacket) {
        switch ($header['Type']) {
            case Packet::ISP_BTC:
                $packet = new isBTC($rawPacket);

                foreach ($this->ui_list as $ui) {
                    if (($this->player->UCID == $packet->UCID && $ui->checkID($packet->ClickID)) OR $this->player->host->local) {
                        $ui->eventClick($packet);
                    }
                }
                foreach ($this->ui_list_perm as $ui) {
                    if (($this->player->UCID == $packet->UCID) OR $this->player->host->local) {
                        $ui->eventClick($packet);
                    }
                }
                break;
            case Packet::ISP_BTT:
                $packet = new isBTT($rawPacket);

                foreach ($this->ui_list as $ui) {
                    if (($this->player->UCID == $packet->UCID && $ui->checkID($packet->ClickID)) OR $this->player->host->local) {
                        $ui->eventType($packet);
                    }
                }
                foreach ($this->ui_list_perm as $ui) {
                    if (($this->player->UCID == $packet->UCID) OR $this->player->host->local) {
                        $ui->eventType($packet);
                    }
                }
                break;
            case Packet::ISP_BFN:
                $packet = new isBFN($rawPacket);
                $this->playerRequest($packet);
                break;
        }
    }

    public function update() {
        if (isset($this->ui_list_perm['rc-restart'])) {
            $this->ui_list_perm['rc-restart']->update();
        }

        if (isset($this->ui_list_perm['timebar'])) {
            $this->ui_list_perm['timebar']->update();
        }

        if (isset($this->ui_list)) {
            foreach ($this->ui_list as $ui) {
                $ui->update();
            }
        }

        if (isset($this->ui_list['debug_timers'])) {
            $this->ui_list['debug_timers']->update();
        }
    }

    public function showPlayersList() {
        $this->log('UI: players list');

        unset($this->ui_list);

        $playerList = new UIPlayersList('playerList', $this->player, $width = 122, $height = 103, $top = 30);

        $playerList->title = $this->player->translator->translateLFS('PLAYERS_TITLE');

        $this->ui_list['playerList'] = &$playerList;
        $playerList->show();
    }

    public function showTopLaps() {
        $this->log('UI: top laps');

        unset($this->ui_list);

        $topLaps = new UITopLaps('topLaps', $this->player, $width = 122, $height = 103, $top = 30);

        $topLaps->title = $this->player->translator->translateLFS('TOP_TITLE');
        $topLaps->items_per_page = 14;
        $topLaps->rows_per_page = 14;
        $topLaps->current_page = 1;

        /* if(isset($this->player->pll->CName)){
          $topLaps->setCarFilter($this->player->pll->CName);
          } */

        if (isset($this->player->pll->CName)) {
            $car = $this->player->pll->CName;
            if (isset($topLaps->cars[$car])) {
                $topLaps->cars[$car]['state'] = 1;
            }
        }

        $this->ui_list['topLaps'] = &$topLaps;

        $topLaps->show();
    }

    public function showDebug($options = array()) {
        unset($this->ui_list);

        switch ($options['type']) {
            case 'timers':
                $this->log('UI: debug timers');
                $ui = new \Insim\UI\UIDebugTimers('debug_timers', $this->player);
                $ui->title = 'Servers status';
                $this->ui_list['debug_timers'] = &$ui;
                $ui->show();
                break;
            case 'light':
                $this->player->host->insim->hostService->test();
                break;
        }
    }

    public function showStatus() {
        $this->log('UI: status');
        $ui = new \Insim\UI\UIDebugTimers('debug_status', $this->player);
        $ui->title = 'Servers status';
        $this->ui_list['debug_status'] = &$ui;
        $ui->show();
    }

    public function showHelp() {
        $this->log('UI: help');

        unset($this->ui_list);

        $help = new UIHelp('help', $this->player);
        $help->title = $this->player->translator->translateLFS('HELP_TITLE');

        $this->ui_list['help'] = &$help;

        $help->show();
    }

    public function showHelpAdmin() {
        $this->log('UI: Admin help');

        unset($this->ui_list);

        $help = new UIHelp('help-admin', $this->player);
        $help->title = $this->player->translator->translateLFS('HELP_ADMIN_TITLE');
        $help->list = 'admin';

        $this->ui_list['help-admin'] = &$help;

        $help->show();
    }

    public function showHelpAll() {
        $this->log('UI: Admin help');

        unset($this->ui_list);

        $help = new UIHelp('help-all', $this->player);
        $help->title = $help->title = $this->player->translator->translateLFS('HELP_ALL_TITLE');
        $help->list = null;

        $this->ui_list['help-all'] = &$help;

        $help->show();
    }

    public function showVersion() {
        $this->log('UI: version');

        unset($this->ui_list);

        $version = new UIVersion('version', $this->player);
        $version->title = $this->player->translator->translateLFS('CHANGELOG_TITLE');

        $this->ui_list['version'] = &$version;

        $version->show();
    }

    public function showTrack() {
        $this->log('UI: track');

        unset($this->ui_list);

        $trackUI = new UITrack('track', $this->player);
        $trackUI->title = $this->player->translator->translateLFS('TRACK_TITLE');

        $this->ui_list['track'] = &$trackUI;

        $wrs = $this->LFSWService->fetchWRs(function() {
            $this->player->host->insim->requestsService->LFSWwrRequest($this->player->host->id, $this->player->host->TrackInfo);
        }, $this->player->host->TrackInfo->LFSWid, null, null);

        $data = array(
            'track' => $this->player->host->TrackInfo,
            'wrs' => $wrs
        );

        $trackUI->setData($data);
        $trackUI->show();
    }

    public function showHost($params) {
        $this->log('UI: host');

        unset($this->ui_list);

        $hostUI = new UIHost('host', $this->player);
        $hostUI->title = 'Host configuration';

        $this->ui_list['host'] = &$hostUI;

        $hostUI->show();
    }

    public function showUserSettings() {
        $this->log('UI: user settings');

        unset($this->ui_list);
        $userUI = new UIUserSettings('user-settings', $this->player);
        $userUI->title = $this->player->translator->translateLFS('USER_SETTINGS_TITLE');

        $this->ui_list['user-settings'] = &$userUI;

        $userUI->show();
    }

    public function showTrackVote($tracks = array()) {
        unset($this->ui_list);

        $UIVote = new UITrackVote('trackVote', $this->player, $width = 80, $height = 150, $top = 32);

        $this->ui_list['trackVote'] = &$UIVote;
        $UIVote->setTracks($tracks);
        
        $UIVote->delayedShow(4);
    }

    public function showRecentPlayersList() {
        $this->log('UI: recent players list');

        unset($this->ui_list);

        $recentPlayers = new UIRecentPlayers('recentPlayers', $this->player, $width = 108, $height = 103, $top = 30);

        $recentPlayers->title = 'Recently disconnected players';

        $this->ui_list['recentPlayers'] = &$recentPlayers;
        $recentPlayers->show();
    }

    public function showPlayersPBs() {
        $this->log('UI: players PBs');

        unset($this->ui_list);

        $ui = new UIPlayersPBs('playersPBs', $this->player, $width = 122, $height = 103, $top = 30);

        $ui->title = 'Players PBs';
        $ui->items_per_page = 14;
        $ui->rows_per_page = 14;
        $ui->current_page = 1;

        // Filter by host allowed cars
        if (isset($this->player->host->allowedCars)) {
            foreach ($this->player->host->allowedCars as $carName) {
                $ui->cars[$carName]['state'] = 1;
            }
        }

        $this->ui_list['playersPBs'] = &$ui;

        $ui->show();
    }

    public function showGrid() {
        $this->log('UI: start grid');

        $grid_mode = $this->player->host->settings->get('race-grid-mode');
        if ($grid_mode == GridModes::RANDOM) {
            $this->player->sendTranslatedMsg('MSG_RANDOM_GRID', MsgTypes::YELLOW);
            return;
        }

        unset($this->ui_list);

        $ui = new UIGrid('startGrid', $this->player, $width = 135, $height = 103, $top = 30);

        $this->ui_list['startGrid'] = &$ui;
        $ui->show();
    }

    public function showSC() {
        $this->log('UI: sc');

        unset($this->ui_list);

        $ui = new UIPlayersSC('playersSC', $this->player, $width = 122, $height = 103, $top = 30);

        $this->ui_list['playersSC'] = &$ui;
        $ui->show();
    }

    public function showLayouts($params) {
        $this->log('UI: help');

        unset($this->ui_list);

        $ui = new \Insim\UI\UILayouts('layouts', $this->player);
        $ui->title = $this->player->translator->translateLFS('LAYOUTS_TITLE');
        $ui->trackCode = isset($params['X']) ? $params['X'] : null;

        $this->ui_list['layouts'] = &$ui;

        $ui->show();
    }

    public function UpdatePlayersList() {
        $playerList = $this->ui_list['playerList'];
    }

    public function UpdatePlayersListRemove($ucid) {
        $this->log('Remove player from list');

        $playerList = &$this->ui_list['playerList'];
        foreach ($playerList->rows as $key => $row) {
            if (isset($row['ucid']) && $row['ucid'] == $ucid) {
                $playerList->updatePlayersRemove($key);
                break;
            }
        }
    }

    public function playerRequest(isBFN $packet) {
        if ($this->player->UCID != $packet->UCID)
            return;

        if (!$this->player->requestClear) {
            $this->userClearButtons($packet);

            $this->showUITrackInfo();
            $this->showUITimeBar();

            // Show RaceCounter if is any in progress
            $active_timer = $this->player->host->getTimerByName(Host::RESTART_TIMERS);
            if ($active_timer) {
                $this->reShowUICounter($active_timer->timer + Host::RESTART_DELAY, $show = true, $delay = 0, $active_timer->name);
            }

            $this->showUserSettings();
            $this->player->requestClear = true;
        } else {
            $this->userClearButtons($packet);
        }
    }

    public function userClearButtons(isBFN $packet) {
        if ($this->player->UCID != $packet->UCID)
            return;

        foreach ($this->ui_list as $key => $ui) {
            $ui->close();
            unset($this->ui_list[$key]);
        }

        foreach ($this->ui_list_perm as $key => $ui) {
            $ui->close();
            unset($this->ui_list_perm[$key]);
        }

        $this->player->requestClear = false;
    }

    public function showUITrackInfo() {
        if (!$this->player->settings->get('trinfo_show'))
            return;

        if (isset($this->ui_list_perm['track-info']) && $this->ui_list_perm['track-info']->displayed)
            return;

        $ui = new UITrackInfo('track-info', $this->player);

        $this->ui_list_perm['track-info'] = &$ui;
        $ui->show();
    }

    /**
     * Show race counter
     * @param type $secs
     * @param type $show
     */
    public function showUICounter($secs, $show = false, $delay = 0, $title = '') {
        if (!isset($this->ui_list_perm['rc-restart'])) {
            $ui = new UIRaceControl('rc-restart', $this->player);
            $this->ui_list_perm['rc-restart'] = &$ui;
        } else {
            $ui = $this->ui_list_perm['rc-restart'];
            $ui->redrawTitle = true;
        }

        $title = $this->player->translator->translateLFS($title);
        
        $ui->setData($secs, function() {
            $this->ui_list_perm['rc-restart']->close();
            unset($this->ui_list_perm['rc-restart']);
        }, $title, $delay);

        if ($show && !$ui->displayed) {
            $ui->show();
        }
    }

    public function reShowUICounter($secs, $show = false, $delay = 0, $type = false) {
        switch ($type) {
            case 'qualify': $title = 'Qualifying starts';
                break;
            case 'restart': $title = 'Race restarts';
                break;
            case 'end': $title = 'Race ends';
                break;
            case 'trackRotate': $title = 'Track change';
                break;
            default: $title = '';
                break;
        }

        $this->showUICounter($secs, $show, $delay, $title);
    }

    public function hideUITrackInfo() {
        if (isset($this->ui_list_perm['track-info'])) {
            $this->ui_list_perm['track-info']->close();
            unset($this->ui_list_perm['track-info']);
        }
    }

    public function showUITimeBar() {
        if (!$this->player->settings->get('timebar_show'))
            return;

        if (isset($this->ui_list_perm['timebar']) && $this->ui_list_perm['timebar']->displayed) {
            return;
        }

        $ui = new UITimeBar('timebar', $this->player);

        $this->ui_list_perm['timebar'] = &$ui;
        $ui->show();
    }

    public function hideUITimeBar() {
        if (isset($this->ui_list_perm['timebar'])) {
            $this->ui_list_perm['timebar']->close();
            unset($this->ui_list_perm['timebar']);
        }
    }

    // Events
    public function eventJoinTrack() {
        $this->showUITrackInfo();
        $this->showUITimeBar();
    }

    public function eventPit() {
        $this->log('UI: pit');
        $this->hideUITimeBar();
    }

    public function eventSpetate() {
        $this->showUITrackInfo();
        $this->hideUITimeBar();
    }

    /**
     * Called when host settings changed
     * @param string $key
     * @param mixed $value
     */
    public function eventHostSettingChanged($key, $value) {
        switch ($key) {
            // Update small track info
            case 'track-rotation':
            case 'rotation-races':
                if (isset($this->ui_list_perm['track-info']) && $this->ui_list_perm['track-info']->displayed) {
                    $this->ui_list_perm['track-info']->update();
                }
                break;
        }
    }

    /**
     * Called on new player's UI when he connects
     */
    public function eventNewConn() {
        // Show trackInfo
        $this->showUITrackInfo();

        // Show RaceCounter if is any in progress
        $active_timer = $this->player->host->getTimerByName(Host::RESTART_TIMERS);
        if ($active_timer) {
            $this->reShowUICounter($active_timer->timer + Host::RESTART_DELAY, $show = true, $delay = 0, $active_timer->name);
        }
    }

    /**
     * Called when connection info received
     */
    public function eventWelcome() {
        $this->player->sendTranslatedMsg('MSG_WELCOME_1', $color = MsgTypes::INFO);
        $this->player->sendTranslatedMsg('MSG_WELCOME_2', $color = MsgTypes::INFO);
    }

    /**
     * Called when race started
     */
    public function eventRaceStarted() {
        // Update short track info (color)
        if (isset($this->ui_list_perm['track-info']) && $this->ui_list_perm['track-info']->displayed) {
            $this->ui_list_perm['track-info']->update();
        }

        // Update time bar
        if (isset($this->ui_list_perm['timebar']) && $this->ui_list_perm['timebar']->displayed) {
            $this->ui_list_perm['timebar']->eventRaceStarted();
        }
    }

    /**
     * Called after first driver finishes race
     */
    public function eventRaceFinished() {
        // Update small track info
        if (isset($this->ui_list_perm['track-info']) && $this->ui_list_perm['track-info']->displayed) {
            $this->ui_list_perm['track-info']->update();
        }
    }

    /**
     * Called when vote is canceled (!cv)
     */
    public function eventVoteCanceled() {
        if (isset($this->ui_list_perm['rc-restart']) && $this->ui_list_perm['rc-restart']->displayed) {
            $this->ui_list_perm['rc-restart']->close();
            unset($this->ui_list_perm['rc-restart']);
        }
    }

    public function eventRaceEnded() {
        $this->log('UI: race ended');
    }

    public function eventTrackChanged() {
        // Update track info
        if (isset($this->ui_list_perm['track-info']) && $this->ui_list_perm['track-info']->displayed)
            $this->ui_list_perm['track-info']->update();

        $this->hideUITimeBar();
        $this->showUITimeBar();

        // Update time bar
        /* if (isset($this->ui_list_perm['timebar']) && $this->ui_list_perm['timebar']->displayed) {
          $this->ui_list_perm['timebar']->eventTrackChanged();
          } */
    }

    public function eventSplit(isSPX $split) {
        if (isset($this->ui_list_perm['timebar']) && $this->ui_list_perm['timebar']->displayed) {
            $this->ui_list_perm['timebar']->eventSplit($split);
        }
    }

    public function eventLap(isLap $lap) {
        if (isset($this->ui_list_perm['timebar']) && $this->ui_list_perm['timebar']->displayed) {
            $this->ui_list_perm['timebar']->eventLap($lap);
        }
    }

    public function eventCarReset() {
        // Update time bar
        if (isset($this->ui_list_perm['timebar']) && $this->ui_list_perm['timebar']->displayed) {
            $this->ui_list_perm['timebar']->eventCarReset();
        }
    }

    public function UpdateTrackWR() {
        $track = $this->trackService->getByShortName($this->player->host->Track);
        $wrs = $this->LFSWService->fetchWRs(null, $track->LFSWid, null, null);

        if (isset($this->ui_list['track'])) {
            $this->ui_list['track']->updateWR(array('wrs' => $wrs));
        }
    }

}
