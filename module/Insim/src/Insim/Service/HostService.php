<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Insim\Model\Host;
use Insim\Model\PlayerClass;
use Insim\Model\Timer;
use Insim\Packets\isAXI;
use Insim\Packets\isFIN;
use Insim\Packets\isOCO;
use Insim\Packets\isRES;
use Insim\Packets\isRST;
use Insim\Packets\isSMALL;
use Insim\Packets\isSTA;
use Insim\Packets\isTINY;
use Insim\Packets\isVER;
use Insim\Packets\Packet;
use Insim\Types\GridModes;
use Insim\Types\InSimTypes;
use Insim\Types\MsgTypes;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Debug\Debug;
use const VERSION;

class HostService extends CoreService {

    const CONFIGFILE = 'setup.cfg';

    public $trackService;
    public $hostSettingsService;
    public $resultService;
    public $safetyService;
    public $carService;
    public $layoutService;
    public $host;
    public $hosts;

    public function __construct(TableGateway $tableGateway) {
        parent::__construct($tableGateway);

        $this->debug = true;
    }

    public function handlePacket($header, $rawPacket) {
        switch ($header['Type']) {
            case Packet::ISP_STA:
                $this->stateChanged(new isSTA($rawPacket));
                break;
            case Packet::ISP_AXI:
                $this->layoutChanged(new isAXI($rawPacket));
                break;
            case Packet::ISP_RST:
                $this->raceStarted(new isRST($rawPacket));
                break;
            case Packet::ISP_RES:
                $this->confirmedFinish(new isRES($rawPacket));
                break;
            case Packet::ISP_SMALL:
                $this->smallPackets(new isSMALL($rawPacket));
                break;
        }
    }

    public function update() {
        if (isset($this->host)) {
            $this->host->update();
        }
    }

    public function timeout($error = 'timeout') {
        $this->host->status = InSimTypes::CONN_TIMEOUT;
        $this->host->version = '';
        $this->host->licence = '';
        $this->host->disconnect_reason = $error;
        $this->host->lastPing = 0;
        $this->host->updated = date('Y-m-d H:i:s');

        socket_close($this->host->socket);
        $this->save($this->host);

        $this->host->insim->playerService->clearConnected();

        $this->log('TimeOut: ' . $this->host->name);
    }

    public function disconnect() {
        $this->host->status = InSimTypes::CONN_NOTCONNECTED;
        $this->host->version = '';
        $this->host->licence = '';
        $this->host->lastPing = 0;
        $this->host->updated = date('Y-m-d H:i:s');

        socket_close($this->host->socket);
        $this->save($this->host);

        $this->host->insim->playerService->clearConnected();

        $this->log('Disconnected: ' . $this->host->name);
    }

    /**
     * Read config file
     */
    public function readConfig() {
        if (empty($this->host->path)) {
            $this->log('Server path not specified (' . $this->host->name . ')');
            return;
        }
        if (!file_exists($this->host->path . HostService::CONFIGFILE)) {
            $this->host->path .= '\\';
        }
        if (!file_exists($this->host->path . HostService::CONFIGFILE)) {
            $this->log('Unable to read server config file (' . $this->host->name . ')');
            return;
        }

        $lines = array();

        $handle = fopen($this->host->path . HostService::CONFIGFILE, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $line = trim($line);
                if (substr($line, 0, 2) == '//' || empty($line))
                    continue;

                $value = substr($line, 1);
                $values = explode('=', $value);
                if (count($values) == 2)
                    $this->host->config[$values[0]] = $values[1];
            }

            fclose($handle);
        }
    }

    public function rotation() {
        $this->host->sendCmd('/end');

        // Rotation enabled check
        if (!$this->host->settings->get('track-rotation')) {
            return;
        }

        $this->host->addTimer(new Timer('clear', 4, function() {
            $this->log('Removing all players from grid');
            $this->host->sendCmd('/clear');
        }));

        $this->host->canJoinTrack = false;

        // Rotation mode
        switch ($this->host->settings->get('rotation-mode')) {
            case 'vote': $this->rotationVoteTrack();
                break;
            case 'random': $this->rotationRandom();
                break;
            case 'manual':
            default: $this->rotationManual();
                break;
        }
    }

    public function rotationVoteTrack() {
        $this->log('Track rotation - vote');

        // Start counter
        $this->host->addTimer(new Timer('trackVoteTime', 16, function() {
            $this->trackVoteDone();
        }));

        $this->startTrackVote();
        $this->host->track_state = Host::TRACK_VOTE_IN_PROGRESS;

        // Show counter
        $this->host->addTimer(new Timer('trackVoteShowTime', 4, function() {
            $this->host->insim->playerService->eventCounterStarted(10, true, 0, 'PLEASE_VOTE');
        }));
    }

    public function startTrackVote() {
        $this->host->track_state = Host::TRACK_VOTE_IN_PROGRESS;
        $this->host->voteTracks = $this->rotationGetAvailableTracks(true);
        $this->host->canVoteTrack = true;

        $this->log('Track vote started');

        if (isset($this->host->insim->playerService->players[$this->host->id])) {
            foreach ($this->host->insim->playerService->players[$this->host->id] as $player) {
                $player->ui->showTrackVote($this->host->voteTracks);
                $player->voted = false;
            }
        }
    }

    public function trackVoteDone() {
        // Get track with most votes
        $this->host->canVoteTrack = false;
        $mostVotes = array();
        $max = 0;

        // Find max vote number
        foreach ($this->host->voteTracks as $track) {
            if ($track['votes'] > $max) {
                $max = $track['votes'];
            }
        }

        foreach ($this->host->voteTracks as $key => $track) {
            if ($track['votes'] == $max) {
                $mostVotes[] = $key;
            }
        }

        // Find winner
        $winner = $mostVotes[0];
        $count = count($mostVotes);
        if ($count > 1) {
            $winner = $mostVotes[rand(0, ($count - 1))];
        }

        // Get list of circuits for selected track
        $this->host->voteTracks = array();
        $circuits = $this->rotationGetAvailableTracks(false, array('track' => array($winner)));
        foreach ($circuits as $circuit) {
            $this->host->voteTracks[$circuit['name']] = $circuit;
        }

        $this->log('Circuit vote started');

        // Update players UI
        if (isset($this->host->insim->playerService->players[$this->host->id])) {
            foreach ($this->host->insim->playerService->players[$this->host->id] as $pl) {
                if (isset($pl->ui->ui_list['trackVote'])) {
                    $pl->ui->ui_list['trackVote']->showCircuitVote($this->host->voteTracks);
                }
            }
        }

        // Start counter
        $this->host->addTimer(new Timer('circuitVoteTime', 15, function() {
            $this->circuitVoteDone();
        }));

        // Show counter
        $this->host->insim->playerService->eventCounterStarted(15, true);

        $this->host->canVoteTrack = true;
    }

    public function circuitVoteDone() {
        $this->host->canVoteTrack = false;

        $mostVotes = array();
        $max = 0;

        // Find max vote number
        foreach ($this->host->voteTracks as $track) {
            if ($track['votes'] > $max) {
                $max = $track['votes'];
            }
        }

        foreach ($this->host->voteTracks as $key => $track) {
            if ($track['votes'] == $max) {
                $mostVotes[] = $key;
            }
        }

        // Find winner
        $winner = $mostVotes[0];
        $count = count($mostVotes);
        if ($count > 1) {
            $winner = $mostVotes[rand(0, ($count - 1))];
        }

        $layout = false;
        if (isset($this->host->voteTracks[$winner])) {
            $circuit = $this->host->voteTracks[$winner];

            if (isset($circuit['open_track'])) {
                $winner = $circuit['open_track'];
            }
            if (isset($circuit['layout'])) {
                $layout = $circuit['layout'];
            }
        }

        $this->host->changeTrack($winner, 2, function() use($layout) {
            if ($layout) {
                $this->host->layoutService->changeLayout($layout);
            } else {
                $this->host->layoutService->clearLayout();
            }
        });

        // Update UI
        if (isset($this->host->insim->playerService->players[$this->host->id])) {
            foreach ($this->host->insim->playerService->players[$this->host->id] as $player) {
                if (isset($player->ui->ui_list['trackVote']))
                    $player->ui->ui_list['trackVote']->close();
            }
        }

        $this->host->sendMsgHost("> Next track: {$winner}", MsgTypes::YELLOW);

        $this->log('Track rotation (vote) - next track: ' . $winner);

        // set qualify
        $qualify = (int) $this->host->settings->get('rotation-qualify');
        $this->host->sendCmd("/qual {$qualify}");
    }

    /**
     * Called after player clicked vote option
     * @param type $track
     * @param PlayerClass $player
     * @return type
     */
    public function rotationVoteClickEvent($track, PlayerClass $player) {
        if (!$this->host->canVoteTrack) {
            return;
        }

        $trackName = $track['name']['value'];

        // Ignore vote for same item
        if ($player->voted == $trackName) {
            return;
        }

        if (isset($this->host->voteTracks[$trackName])) {
            // Remove vote from previous item
            if ($player->voted && isset($this->host->voteTracks[$player->voted])) {
                $this->host->voteTracks[$player->voted]['votes'] -= 1;
                $removeVote = $player->voted;
            }
            $this->host->voteTracks[$trackName]['votes'] += 1;
            $player->voted = $trackName;
        }

        if (isset($this->host->insim->playerService->players[$this->host->id])) {
            foreach ($this->host->insim->playerService->players[$this->host->id] as $pl) {
                if (isset($pl->ui->ui_list['trackVote'])) {
                    $pl->ui->ui_list['trackVote']->updateVote($trackName, $this->host->voteTracks[$trackName]['votes']);

                    if (isset($removeVote)) {
                        $pl->ui->ui_list['trackVote']->updateVote($removeVote, $this->host->voteTracks[$removeVote]['votes']);
                    }
                }
            }
        }

        $this->host->sendMsgHost("{$player->name} ^8voted for {$trackName}");
    }

    public function rotationRandom() {
        $tracks = $this->rotationGetAvailableTracks();

        $tracks_counts = count($tracks);
        if ($tracks_counts > 0) {
            $list = array();
            foreach ($tracks as $tr) {
                $list[] = $tr['name'];
            }

            do {
                $nextTrack = $list[rand(0, ($tracks_counts - 1))];
            } while ($this->host->Track == $nextTrack);

            $this->host->changeTrack($nextTrack);

            $this->log('Track rotation (random) - next track: ' . $nextTrack);
        } else {
            $this->log('Track rotation (random) - no available tracks');
        }
    }

    public function rotationManual() {
        $this->log('Track rotation - manual');
    }

    public function rotationGetAvailableTracks($getTracks = false, $where = array()) {
        $reversed = $this->host->settings->get('tracks-types-reversed');
        $open = $this->host->settings->get('tracks-types-open');

        // Track length
        if ($this->host->settings->get('rotation-length-min') > 0) {
            $where['lenght']['min'] = $this->host->settings->get('rotation-length-min');
        }
        if ($this->host->settings->get('rotation-length-max') > 0) {
            $where['lenght']['max'] = $this->host->settings->get('rotation-length-max');
        }

        // Licence
        if (isset($this->host->config['mode'])) {
            switch (strtolower($this->host->config['mode'])) {
                case 'demo': $where['licence'] = ['DEMO'];
                    $reversed = false;
                    break;
                case 's1': $where['licence'] = ['DEMO', 'S1'];
                    break;
                case 's2': $where['licence'] = ['DEMO', 'S1', 'S2'];
                    break;
                case 's3': $where['licence'] = ['DEMO', 'S1', 'S2', 'S3'];
                    break;
            }
        }

        // Filter track
        if (!isset($where['track'])) {
            foreach ($this->trackService->tracks as $trackName => $track) {
                if ($this->host->settings->get("tracks-{$track['shortName']}")) {
                    $where['track'][] = $trackName;
                }
            }
        }

        // Grid check
        if ($this->host->settings->get('rotation-grid')) {
            $where['grid'] = count($this->host->playerService->players[$this->host->id]);
        }

        $where['types']['asphalt'] = $this->host->settings->get('tracks-types-asphalt');
        $where['types']['rallycross'] = $this->host->settings->get('tracks-types-rally');
        $where['types']['oval'] = $this->host->settings->get('tracks-types-oval');
        //$where['types']['open'] = $this->host->settings->get('tracks-types-open');

        $tracks = array();

        if ($getTracks) {
            foreach ($this->trackService->getTracks($where) as $track) {
                if (!isset($tracks[$track->track]))
                    $tracks[$track->track] = array('name' => $track->track, 'votes' => 0);
            }
        } else {
            foreach ($this->trackService->getTracks($where) as $track) {
                $tracks[] = array('name' => $track->short_name, 'long_name' => $track->circuit, 'votes' => 0);

                // Add reversed config
                if ($reversed && $track->reverse) {
                    $tracks[] = array('name' => "{$track->short_name}R", 'long_name' => "{$track->circuit} rev.", 'votes' => 0);
                }
            }

            // Add open tracks
            if ($open) {
                $trackName = array_values($where['track'])[0];
                $trackCode = $this->trackService->getShortName($trackName);
                $layouts = $this->host->layoutService->getAvailableLayoutsByCode($trackCode);

                foreach ($layouts as $openTrack => $layouts) {
                    foreach ($layouts as $layout) {
                        $name = explode('_', $layout, 2);

                        if (count($name) == 2 && !empty($name[1])) {
                            $tracks[] = array('name' => $name[0], 'long_name' => $name[1], 'votes' => 0, 'open_track' => $openTrack, 'layout' => $layout);
                        } else {
                            $tracks[] = array('name' => $name[0], 'long_name' => $name[0], 'votes' => 0, 'open_track' => $openTrack, 'layout' => $layout);
                        }
                    }
                }
            }
        }

        return $tracks;
    }

    /**
     * Small packet handler
     * @param isSMALL $packet
     */
    public function smallPackets(isSMALL $packet) {
        switch ($packet->SubT) {
            case isSMALL::SMALL_ALC:    // Allowed cars info
                $this->host->allowedCars = isSMALL::parseAllowedCars($packet->UVal);
                break;
            case isSMALL::SMALL_VTA:
                $this->voteActionEvent($packet);
                break;
        }
    }

    // Events
    /**
     * Host connected event
     * @param Host $host
     * @param type $rawPacket
     * @param type $insimService
     */
    public function connected(Host &$host, $rawPacket, $insimService) {
        $packet = new isVER($rawPacket);

        $host->status = InSimTypes::CONN_CONNECTED;
        $host->connected = date('Y-m-d H:i:s');
        $host->updated = date('Y-m-d H:i:s');
        $host->version = $packet->Version;
        $host->licence = $packet->Product;
        $host->disconnect_reason = '';
        $host->lastPing = 0;
        $this->save($host);

        $this->host = &$host;

        // Clear previous connections and players
        $this->host->insim->playerService->clearConnected();

        // Request all connections
        $tiny_ncn = new isTINY();
        $tiny_ncn->ReqI = true;
        $tiny_ncn->SubT = isTINY::TINY_NCN;
        $this->host->insim->send($tiny_ncn);

        // Request players
        $tiny_npl = new isTINY();
        $tiny_npl->ReqI = true;
        $tiny_npl->SubT = isTINY::TINY_NPL;
        $this->host->insim->send($tiny_npl);

        // Request connection info
        $tiny_nci = new isTINY();
        $tiny_nci->ReqI = true;
        $tiny_nci->SubT = isTINY::TINY_NCI;
        $this->host->insim->send($tiny_nci);

        // Request state
        $tiny_sta = new isTINY();
        $tiny_sta->ReqI = true;
        $tiny_sta->SubT = isTINY::TINY_SST;
        $this->host->insim->send($tiny_sta);

        // Request layout info
        $tiny_axi = new isTINY();
        $tiny_axi->ReqI = true;
        $tiny_axi->SubT = isTINY::TINY_AXI;
        $this->host->insim->send($tiny_axi);

        // Request race info
        $tiny_rst = new isTINY();
        $tiny_rst->ReqI = true;
        $tiny_rst->SubT = isTINY::TINY_RST;
        $this->host->insim->send($tiny_rst);

        $this->log('Connected to: ' . $host->name);

        // Load settings
        $this->readConfig();
        $this->host->settings = clone $this->hostSettingsService;
        $this->host->settings->host_id = $this->host->id;
        $this->host->settings->load();

        // Apply allowed cars
        $this->host->applyAllowedCars();

        // ResultsService
        $this->host->results = clone $this->resultService;
        $this->host->results->host = $this->host;

        // SafetyService
        $this->host->safetyService = clone $this->safetyService;
        $this->host->safetyService->host = $this->host;

        // TimerService
        $this->host->timerService = new \Insim\Service\TimerService();

        // LayoutService
        $this->host->layoutService = clone $this->layoutService;
        $this->host->layoutService->host = $this->host;
        $this->host->layoutService->init();

        $host->sendMsgHost('KingTracker ' . VERSION . ' connected to server', MsgTypes::INFO);
    }

    public function stateChanged(isSTA $packet) {
        $this->host->setSTA($packet, function($first = false) {
            $this->trackChanged($first);
        });
        $this->save($this->host);
    }

    public function layoutChanged(isAXI $packet) {
        $this->host->LName = $packet->LName;
        $this->host->splitsCount = $packet->NumCP;

        $this->host->LShortName = '';
        if ($this->host->LName) {
            $name = explode('_', $this->host->LName, 3);
            if (count($name) > 1 && !empty($name[1])) {
                $this->host->LShortName = "{$name[0]}_{$name[1]}";
            }
        }

        $this->save($this->host);

        $this->host->layoutService->eventLayoutChanged($packet);

        // UI event
        if (isset($this->host->playerService->players[$this->host->id])) {
            foreach ($this->host->playerService->players[$this->host->id] as $player) {
                if (isset($player->ui)) {
                    $player->lapsService->eventTrackChanged();
                    $player->ui->eventTrackChanged();
                }
            }
        }
    }

    public function trackChanged($first = false) {
        $track = $this->trackService->getByShortName($this->host->Track);

        // UI event
        if (!$first) {
            if (isset($this->host->playerService->players[$this->host->id])) {
                foreach ($this->host->playerService->players[$this->host->id] as $player) {
                    if (isset($player->ui)) {
                        $player->lapsService->eventTrackChanged();
                        $player->ui->eventTrackChanged();
                    }
                }
            }
        }

        if ($track) {
            $this->host->TrackInfo = $track;

            // LFSW WR Request
            $this->host->insim->requestsService->LFSWwrRequest($this->host->id, $track);
        }

        // Request allowed cars
        $tiny_alc = new isTINY();
        $tiny_alc->ReqI = true;
        $tiny_alc->SubT = isTINY::TINY_ALC;
        $this->host->insim->send($tiny_alc);
    }

    public function raceStarted(isRST $packet) {
        $this->host->setRST($packet);
    }

    /**
     * Called when first player finishes race
     * @param isFIN $packet
     * @param $count - count race as finished
     * @return type
     */
    public function raceFinished(isFIN $packet, $count = true) {
        if ($this->host->isFinished) {
            return;
        }

        if ($this->host->race_state == Host::QUALIFY_INPROGRESS OR $this->host->race_state == Host::QUALIFY_FINISHED) {
            return;
        }

        $this->host->finish($count);

        $this->log('Race finished ' . $this->host->finishedCount);
    }

    public function confirmedFinish(isRES $packet) {
        if ($packet->isConfirmed()) {
            $this->host->results->addResult($packet);
        }
    }

    /**
     * Called when rece start / end countdown is started
     * @param isSMALL $packet
     */
    public function voteActionEvent(isSMALL $packet) {
        switch ($packet->UVal) {
            case isSMALL::VOTE_RESTART:
                if ($this->host->race_state == Host::QUALIFY_INPROGRESS OR $this->host->race_state == Host::QUALIFY_FINISHED) {
                    // TODO Qualify results reorder
                } else {
                    $this->reorderGrid();
                }
                break;
        }
    }

    /**
     * Generate grid for next race
     */
    public function generateGrid() {
        $grid = array();

        $lastResult = $this->host->results->getLastFinishedRace();

        if (empty($lastResult)) {
            return $grid;
        }

        $results = $this->host->results->fetchPlayers($lastResult);

        if (empty($results)) {
            return $grid;
        }


        $pos = 1;
        foreach ($results as $res) {
            $grid[$pos] = array(
                'PLID' => $res->PLID,
                'UCID' => $res->UCID,
                'PName' => $res->PName,
                'UName' => $res->UName,
                'pos' => $res->pos,
                'car' => $res->car
            );
            $pos++;
        }

        $grid_mode = $this->host->settings->get('race-grid-mode');
        $grid_reverse = $this->host->settings->get('race-grid-reverse');

        switch ($grid_mode) {
            case GridModes::RANDOM:
                $array_keys = array_keys($grid);
                shuffle($array_keys);

                $newGrid = array();
                $position = 1;
                foreach ($array_keys as $new_pos) {
                    $newGrid[$position] = $grid[$new_pos];
                    $position++;
                }

                return $newGrid;
                break;
            case GridModes::REVERSE:
                if (strpos($grid_reverse, '%') !== FALSE) {
                    $grid_reverse = ceil(count($grid) * intval($grid_reverse) / 100);
                } else {
                    $grid_reverse = intval($grid_reverse);
                }

                if ($grid_reverse == 0 OR $grid_reverse >= count($grid)) {
                    krsort($grid);                                              // Reverse all players
                } else {
                    $toReverse = array_slice($grid, 0, $grid_reverse, true);    // Slice top players
                    krsort($toReverse);                                         // Reverse
                    $rest = array_slice($grid, $grid_reverse, null, true);      // Rest of players
                    $grid = array_merge($toReverse, $rest);                     // Merge
                }

                break;
            case GridModes::FINISH:
            default :
                break;
        }

        return $grid;
    }

    // Commands
    public function cancelVote() {
        $this->host->sendCmd('/cv');
        $this->host->removeTimer();

        // UI Kill restart counter
        if (isset($this->host->insim->playerService->players[$this->host->id])) {
            foreach ($this->host->insim->playerService->players[$this->host->id] as $player) {
                $player->ui->eventVoteCanceled();
            }
        }

        $this->log('Vote canceled');
    }

    /**
     * Set race laps
     * @param type $laps
     */
    public function setLaps($laps) {
        $this->host->sendCmd("/laps {$laps}");

        if ($laps > 0) {
            $sLaps = "{$laps} lap" . ($laps > 1 ? 's' : '');
            $this->host->sendMsgHost("> Race length has been changed to {$sLaps}", MsgTypes::YELLOW);
        }
    }

    /**
     * Set race hours
     * @param type $hours
     */
    public function setHours($hours) {
        $this->host->sendCmd("/hours {$hours}");

        $sHours = "{$hours} hour" . ($hours > 1 ? 's' : '');
        $this->host->sendMsgHost("> Race length has been changed to {$sHours}", MsgTypes::YELLOW);
    }

    /**
     * Reorder starting grid
     */
    public function reorderGrid() {
        $grid = $this->generateGrid();
        $all_players = $this->toArray($this->host->playerService->getCurrentPlayers(array('PLID' => 'PLID', 'PName_utf' => 'PName_utf')), 'PLID');

        $position = 1;
        $order = array();
        foreach ($grid as $gr) {
            $order[$position] = intval($gr['PLID']);
            $position++;
        }

        foreach ($all_players as $player) {
            if (!in_array($player->PLID, $order)) {
                $order[$position] = intval($player->PLID);
                $position++;
            }
        }

        if (!empty($order)) {
            $reo = new \Insim\Packets\isREO();
            $reo->PLID = $order;
            $reo->NumP = count($order);

            $this->host->insim->send($reo);
        }
    }

    /**
     * Test command (!test <action> [param])
     * @param type $params
     * @return type
     */
    public function test($params) {
        switch ($params['action']) {
            case 'light':
                $this->testLight($params);
                return;
            case 'rotate':
                $this->testRotate($params);
                return;
        }
    }

    public function testLight($params) {
        switch ($params['param']) {
            case 'on':
                $light = new isOCO();
                $light->OCOAction = 4;
                $light->Index = 240;
                $light->Data = 4;
                $this->host->insim->send($light);
                break;
            case 'off':
                $light = new isOCO();
                $light->OCOAction = 5;
                $light->Index = 240;
                $light->Data = 4;
                $this->host->insim->send($light);
                break;
        }

        /* $light = new \Insim\Packets\isOCO();
          $light->OCOAction = 5;
          $light->Index = 149;
          $light->Identifier = 1;
          $light->Data = 1; */
    }

    public function testRotate($params) {
        $this->rotation();
    }

    // DB
    public function fetchAll($buffered = false, $tableGateway = false) {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('hosts');
        $select->where->equalTo('hosts.active', 1);

        $resultSet = $this->tableGateway->selectWith($select);

        if ($buffered)
            $resultSet->buffer();

        return $resultSet;
    }

}
