<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Zend\Db\TableGateway\TableGateway;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\Permissions\Acl\Role\GenericRole;

class ACLService extends CoreService {

    protected $acl;
    protected $roles;
    protected $groups;
    public $playerRole = 'guest';
    public $player;

    public function __construct(TableGateway $tableGateway) {
        parent::__construct($tableGateway);
    }

    public function init(\Insim\Model\PlayerClass $player = null) {
        $this->player = &$player;
        $this->groups = array();
        $this->acl = new Acl();
        $this->roles = include BASE_PATH . '/config/insim.acl.roles.php';

        $allActions = array();
        foreach ($this->roles as $role => $actions) {
            $this->groups[] = $role;
            $role = new GenericRole($role);
            $this->acl->addRole($role);

            $allActions = array_merge($actions, $allActions);

            // adding resources
            foreach ($actions as $action) {
                if (!$this->acl->hasResource($action))
                    $this->acl->addResource(new GenericResource($action));
            }

            // adding restrictions
            foreach ($allActions as $action) {
                $this->acl->allow($role, $action);
            }
        }

        if ($this->player) {
            $aclDB = $this->getBy($where = array('UName' => $this->player->UName));
            if ($aclDB) {
                $this->playerRole = $aclDB->role;
            }
        }
    }

    public function check($action) {
        if ($this->acl->hasResource($action) && $this->acl->isAllowed($this->playerRole, $action)) {
            return true;
        }
        return false;
    }

    public function getGroups() {
        return $this->groups;
    }

    public static function getRoles($rolesOnly = true) {
        $acl = include BASE_PATH . '/config/insim.acl.roles.php';
        
        if(!$rolesOnly){
            return $acl;
        }
       
        $return = [];
        
        foreach($acl as $key => $val){
            $return[] = $key;
        }
        
        return $return;
    }

    public function getByLicence($name) {
        return $this->getBy(['UName' => $name]);
    }

    public function accessDenied() {
        $msg = new \Insim\Packets\isMTC();
        $msg->UCID = $this->player->UCID;
        $msg->Text = '^1Access denied';
        $this->player->host->insim->send($msg);
    }

}
