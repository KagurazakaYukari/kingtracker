<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Zend\Db\TableGateway\TableGateway;
use Zend\Debug\Debug;

class HostSettingsService extends CoreService {

    public $host_id = 0;
    protected $cache = null;

    public function __construct(TableGateway $tableGateway) {
        $this->debug = true;
        parent::__construct($tableGateway);
    }

    /**
     * Load settings or create default
     */
    public function load() {
        $where['host_id'] = $this->host_id;
        $settings = CoreService::toArray($this->fetchAllBy($where, $buffered = true, false), 'name');

        $where['host_id'] = 0;
        $settings_default = $this->fetchAllBy($where, $buffered = true, false);

        foreach ($settings_default as $setting) {
            // Default setting
            if (!isset($settings[$setting->name])) {
                $settings[$setting->name] = $setting;
                $setting->id = 0;
                $setting->host_id = $this->host_id;
                $this->save($setting);
            }
        }

        foreach ($settings as $setting) {
            if (isset($setting->data['type'])) {
                switch ($setting->data['type']) {
                    case 'json':
                        $this->cache[$setting->name] = empty($setting->value) ? [] : json_decode($setting->value, true);
                        break;
                }
            } else {
                $this->cache[$setting->name] = $setting->value;
            }
        }

        //\Zend\Debug\Debug::dump($this->cache);
    }

    /**
     * Get single setting
     * @param type $key
     * @return value
     */
    public function get($key) {
        if (isset($this->cache[$key])) {
            return $this->cache[$key];
        }

        // Create default setting
        $setting = $this->getBy(array('host_id' => 0, 'name' => $key));

        if ($setting) {
            $setting->id = 0;
            $setting->host_id = $this->host_id;
            $this->save($setting);
            $this->cache[$key] = $setting->value;
            return $this->cache[$key];
        }

        return false;
    }

    /**
     * Get default setting value
     * @param type $key
     * @return type
     */
    public function getDefault($key) {
        $setting = $this->getBy(array('host_id' => 0, 'name' => $key));
        return $setting ? $setting->value : null;
    }

    /**
     * Update setting
     * @param type $key
     * @param type $value
     */
    public function update($key, $value) {
        $this->cache[$key] = $value;

        $setting = $this->getBy(array('host_id' => $this->host_id, 'name' => $key));
        if ($setting) {
            $setting->value = $value;
            $this->save($setting);
        }
    }

    /**
     * Save allowed cars
     * @param type $car (XFG, XRG, ...)
     * @param type $value
     */
    public function saveAllowedCar($car, $value) {
        if (isset($this->cache['allowed-cars'][$car]['enabled'])) {
            $this->cache['allowed-cars'][$car]['enabled'] = $value;
        } else {
            $this->cache['allowed-cars'][$car] = ['enabled' => $value];
        }

        $setting = $this->getBy(array('host_id' => $this->host_id, 'name' => 'allowed-cars'));

        if ($setting) {
            $setting->value = json_encode($this->cache['allowed-cars']);
            $this->save($setting);
        }
    }

    /**
     * Save allowed cars restrictions
     * @param type $car (XFG, XRG, ...)
     * @param type $key (res, mass)
     * @param type $value
     */
    public function saveAllowedCarRest($car, $key, $value) {
        if (isset($this->cache['allowed-cars'][$car])) {
            $this->cache['allowed-cars'][$car][$key] = $value;
        } else {
            $this->cache['allowed-cars'][$car] = [$key => $value];
        }

        $setting = $this->getBy(array('host_id' => $this->host_id, 'name' => 'allowed-cars'));

        if ($setting) {
            $setting->value = json_encode($this->cache['allowed-cars']);
            $this->save($setting);
        }
    }

    public function getPitWindows() {
        $pits = (int) $this->get('race-pits');
        $windowFrom = json_decode($this->get('race-pit-window-from'), true);
        $windowFrom = !$windowFrom ? [] : $windowFrom;
        $windowTo = json_decode($this->get('race-pit-window-to'), true);
        $windowTo = !$windowTo ? [] : $windowTo;

        $pitWindows = array();

        for ($i = 1; $i <= $pits; $i++) {
            $pitWindows[$i] = ['from' => 0, 'to' => 0];
        }

        foreach ($windowFrom as $key => $value) {
            $pitWindows[$key]['from'] = $value;
        }

        foreach ($windowTo as $key => $value) {
            $pitWindows[$key]['to'] = $value;
        }

        return $pitWindows;
    }

    /**
     * Get required pit work
     * @param type $pitNumber
     * @return type
     */
    public function getPitWork($pitNumber = 1) {
        $work = json_decode($this->get('race-pit-work'), true);
        $work = $work ? $work : [];

        return isset($work[$pitNumber]) ? $work[$pitNumber] : 'none';
    }

    /**
     * Get required pit number
     * @param type $currentLap
     */
    public function getRequiredPitNumber($currentLap, $donePits) {
        $pitWindows = $this->getPitWindows();
        $pitNumber = 0;

        foreach ($pitWindows as $key => $window) {
            // Skip already done pit stop
            if (isset($donePits[$key]) && $donePits[$key]) {
                continue;
            }

            // Case X - Y (from X to Y lap) 
            if (($window['from'] != 0 && $currentLap >= $window['from']) && ($window['to'] != 0 && $currentLap <= $window['to'])) {
                $pitNumber = $key;
                break;
            }

            // Case 0 - X (unlimited from to X lap)
            if ($window['from'] == 0 && ($window['to'] != 0 && $currentLap <= $window['to'])) {
                $pitNumber = $key;
                break;
            }

            // Case X - 0 (from X lap to unlimited)
            if (($window['from'] != 0 && $currentLap >= $window['from']) && $window['to'] == 0) {
                $pitNumber = $key;
                break;
            }

            // Case 0 - 0 (unlimited from to)
            if ($window['from'] == 0 && $window['to'] == 0) {
                $pitNumber = $key;
                break;
            }
        }

        return $pitNumber;
    }
    
    public function getLayoutDir(){
        Debug::dump($this->get('lytdir'));
    }

}
