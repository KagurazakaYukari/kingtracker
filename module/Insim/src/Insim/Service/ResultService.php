<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Insim\Helper\InSimHelper;
use Insim\Model\Host;
use Insim\Model\Result;
use Insim\Model\ResultEvent;
use Insim\Model\ResultPlayer;
use Insim\Packets\isLap;
use Insim\Packets\isRES;
use Insim\Packets\isRST;
use Insim\Packets\isSPX;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Debug\Debug;

class ResultService extends CoreService {

    const MAX_RESULTS_HISTORY = 10;

    public $host;
    protected $current_result_id;
    protected $resultPlayerTable;
    protected $resultEventTable;

    public function __construct(TableGateway $tableGateway) {
        parent::__construct($tableGateway);
        $this->debug = true;
    }

    /**
     * Player confirmed finish or qualify?
     * @param isRES $packet
     * @return type
     */
    public function addResult(isRES $packet) {
        if (!isset($this->current_result_id)) {
            return;
        }

        // Result not added to table
        if ($packet->ResultNum == 255) {
            return;
        }

        $resultPlayer = new ResultPlayer();
        $resultPlayer->result_id = $this->current_result_id;
        $resultPlayer->pos = $packet->ResultNum + 1;
        $resultPlayer->car = $packet->CName;
        $resultPlayer->UName = $packet->UName;
        $resultPlayer->BTime = $packet->BTime;
        $resultPlayer->BTime_string = InSimHelper::timeToString($packet->BTime);
        $resultPlayer->TTime = $packet->TTime;
        $resultPlayer->TTime_string = InSimHelper::timeToString($packet->TTime);
        $resultPlayer->Flags = $packet->Flags;
        $resultPlayer->LapsDone = $packet->LapsDone;
        $resultPlayer->NumStops = $packet->NumStops;
        $resultPlayer->PSeconds = $packet->PSeconds;
        $this->save($resultPlayer, $this->resultPlayerTable);

        // Update number of finished players
        $result = $this->getByID($this->current_result_id);
        $result->finished = $packet->NumRes;
        $this->save($result);
    }

    /**
     * Add split event
     * @param isSPX $packet
     * @param type $UName
     */
    public function addEventSplit(isSPX $packet, $UName) {
		return;
		
        if (!isset($this->current_result_id)) {
            return;
        }

        $event = new ResultEvent();
        $event->UName = $UName;
        $event->event = ResultEvent::SPLIT;
        $event->result_id = $this->current_result_id;
        $event->data = array(
            'split' => $packet->Split,
            'STime' => $packet->STime,
            'ETime' => $packet->ETime,
            'STime_string' => InSimHelper::timeToString($packet->STime),
            'ETime_string' => InSimHelper::timeToString($packet->ETime),
        );
        $event->time = date('Y-m-d H:i:s');

        $this->save($event, $this->resultEventTable);
    }

    /**
     * Add lap event
     * @param isLap $packet
     * @param type $UName
     * @return type
     */
    public function addEventLap(isLap $packet, $UName) {
		return;
		
        if (!isset($this->current_result_id)) {
            return;
        }

        $event = new ResultEvent();
        $event->UName = $UName;
        $event->event = ResultEvent::LAP;
        $event->result_id = $this->current_result_id;
        $event->data = array(
            'LapsDone' => $packet->LapsDone,
            'LTime' => $packet->LTime,
            'ETime' => $packet->ETime,
            'LTime_string' => InSimHelper::timeToString($packet->LTime),
            'ETime_string' => InSimHelper::timeToString($packet->ETime),
        );
        $event->time = date('Y-m-d H:i:s');

        $this->save($event, $this->resultEventTable);
    }

    /**
     * Race started - create new result
     * @param isRST $packet
     */
    public function startResults(isRST $packet) {
        $result = new Result();
        $result->host_id = $this->host->id;
        $result->type = ($this->host->race_state == Host::QUALIFY_INPROGRESS ? Result::TASK_QUALIFY : Result::TYPE_RACE);
        $result->info = $packet;
        $result->time = date('Y-m-d H:i:s');

        $this->current_result_id = $this->save($result);

        // Delete old result
        $resultsCount = $this->count(array('host_id' => $this->host->id));

        if ($resultsCount >= ResultService::MAX_RESULTS_HISTORY) {
            $lastResult = $this->fetchAllBy(array('host_id' => $this->host->id), false, false, 'time ASC', 1);

            if ($lastResult) {
                $lastResult = $lastResult->current();
                $this->delete($lastResult->id);
            }
        }
    }

    /**
     * Get last finished race results
     * @return type
     */
    public function getLastFinishedRace() {
        $resultSet = $this->tableGateway->select(function(Select $select) {
            $select->where->and->equalTo('type', Result::TYPE_RACE);
            $select->where->and->greaterThan('finished', 0);

            $select->order('time DESC');
        });
        return $resultSet->current();
    }

    /**
     * Fetch players finish results
     * @param Result $result
     * @param type $inRaceOnly = true
     * @return type
     */
    public function fetchPlayers(Result $result, $inRaceOnly = true) {
        $resultSet = $this->resultPlayerTable->select(function(Select $select) use ($result, $inRaceOnly) {
            $select->where->and->equalTo('result_id', $result->id);

            if ($inRaceOnly) {
                $select->join('hosts_conns', 'results_player.UName = hosts_conns.UName', array('PName' => 'PName', 'UCID' => 'UCID'));
                $select->join('hosts_players', 'hosts_conns.UCID = hosts_players.UCID', array('PLID' => 'PLID'));
            }else{
                $select->join('players', 'results_player.UName = players.UName', array('PName' => 'PName'));
            }

            $select->order('pos ASC');
        });

        return $resultSet->buffer();
    }

    public function setResultPlayerTable(TableGateway $table) {
        $this->resultPlayerTable = $table;
    }

    public function setResultEventTable(TableGateway $table) {
        $this->resultEventTable = $table;
    }

}
