<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class Host extends CoreModel {

    const TRACK_LOADED = 1;
    const TRACK_LOADING = 2;
    const TRACK_VOTE_IN_PROGRESS = 3;
    const RACE_INPROGRESS = 1;
    const RACE_FINISHED = 2;
    const QUALIFY_INPROGRESS = 3;
    const QUALIFY_FINISHED = 4;
    const RESTART_TIMERS = array('restart', 'end', 'qualify', 'trackRotate');
    const RESTART_DELAY = 4;

    public $id;
    public $name;
    public $ip;
    public $port;
    public $passwd;
    public $path = null;
    public $active = 1;
    public $local = 0;
    public $localUCID = 0;
    public $status = 0;
    public $version;
    public $licence;
    public $connected;
    public $updated;
    public $disconnect_reason;
    public $Wind;
    public $Weather;
    public $Track;
    public $TrackName = '';
    public $TrackInfo = null;
    public $lastTrack = '';
    public $trackChanged = false;
    public $changeTrackTo = '';
    public $reversed = false;
    public $LName;
    public $LShortName;
    public $RaceLaps;
    public $QualMins;
    public $ViewPLID = 0;
    public $splitsCount = 0;
    public $timing = 0;
    public $socket = false;
    public $lastPing = 0;
    public $config = array();
    public $hostConfigKeys = array(
        'canreset', 'vote', 'select', 'midrace', 'mustpit', 'fcv', 'cruise', 'autokick'
    );
    public $allowedCars = array();
    // Restart timer
    public $timer = 0;
    public $isCounting = false;
    public $timerEvent = 'restart';
    public $timerEvents = array();
    protected $timer_sec_last = 0;
    public $timerOffset = 1;
    // Finished races
    public $isFinished = false;
    public $finishedCount = 0;
    public $callbacks = array();
    // Services
    public $insim = null;               // InsimService
    public $playerService = null;
    public $hostService = null;
    public $settings = null;            // HostSettingsService
    public $track_state = Host::TRACK_LOADED;
    public $race_state = null;
    public $voteTracks = array();
    public $canVoteTrack = false;
    public $canJoinTrack = true;
    public $timerService;
    public $results;        // ResultService
    public $safetyService;
    public $layoutService;

    public function __construct() {
        
    }

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
        $this->ip = (!empty($data['ip'])) ? $data['ip'] : null;
        $this->port = (!empty($data['port'])) ? $data['port'] : null;
        $this->passwd = (!empty($data['passwd'])) ? $data['passwd'] : null;
        $this->path = (!empty($data['path'])) ? $data['path'] : null;
        $this->active = (!empty($data['active'])) ? $data['active'] : null;
        $this->local = (!empty($data['local'])) ? $data['local'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : null;
        $this->version = (!empty($data['version'])) ? $data['version'] : null;
        $this->licence = (!empty($data['licence'])) ? $data['licence'] : null;
        $this->connected = (!empty($data['connected'])) ? $data['connected'] : null;
        $this->updated = (!empty($data['updated'])) ? $data['updated'] : null;
        $this->disconnect_reason = (!empty($data['disconnect_reason'])) ? $data['disconnect_reason'] : null;

        $this->Wind = (!empty($data['Wind'])) ? $data['Wind'] : null;
        $this->Track = (!empty($data['Track'])) ? $data['Track'] : null;
        $this->LName = (!empty($data['LName'])) ? $data['LName'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['name'] = (!empty($this->name)) ? $this->name : null;
        $data['path'] = (!empty($this->path)) ? $this->path : null;
        $data['ip'] = (!empty($this->ip)) ? $this->ip : null;
        $data['port'] = (!empty($this->port)) ? $this->port : null;
        $data['passwd'] = (!empty($this->passwd)) ? $this->passwd : null;
        $data['active'] = (!empty($this->active)) ? $this->active : null;
        $data['local'] = (!empty($this->local)) ? $this->local : null;
        $data['status'] = (!empty($this->status)) ? $this->status : null;
        $data['version'] = (!empty($this->version)) ? $this->version : null;
        $data['licence'] = (!empty($this->licence)) ? $this->licence : null;
        $data['connected'] = (!empty($this->connected)) ? $this->connected : null;
        $data['updated'] = (!empty($this->updated)) ? $this->updated : null;
        $data['disconnect_reason'] = (!empty($this->disconnect_reason)) ? $this->disconnect_reason : null;

        $data['Wind'] = (!empty($this->Wind)) ? $this->Wind : null;
        $data['Track'] = (!empty($this->Track)) ? $this->Track : null;
        $data['LName'] = (!empty($this->LName)) ? $this->LName : null;

        return $data;
    }

    public function timingToString() {
        if ($this->QualMins > 0) {
            return $this->QualMins . ' min';
        }

        if ($this->RaceLaps == 0) {
            return $this->QualMins == 0 ? 'practice' : '---';
        } elseif ($this->RaceLaps >= 1 && $this->RaceLaps <= 99) {
            return $this->RaceLaps . ' laps';
        } elseif ($this->RaceLaps >= 100 && $this->RaceLaps <= 190) {
            return (($this->RaceLaps - 100) * 10 + 100) . ' laps';
        } elseif ($this->RaceLaps >= 191 && $this->RaceLaps <= 238) {
            return ($this->RaceLaps - 190) . ' hours';
        }
    }

    public function stateToString() {
        if ($this->QualMins > 0) {
            return 'Qualification';
        }

        if ($this->RaceLaps == 0) {
            return $this->QualMins == 0 ? 'Practice' : '';
        } else {
            return 'Race';
        }
    }

    public function windToString() {
        switch ($this->Wind) {
            case 0:
                return 'no';
            case 1:
                return 'weak';
            case 2:
                return 'strong';
        }
    }

    public function weatherToString() {
        $track = substr($this->Track, 0, 2);

        switch ($track) {
            case 'BL':
                switch ($this->Weather) {
                    case 0: return 'Clear day';
                    case 1: return 'Cloudy afternoon';
                    case 2: return 'Cloudy sunset';
                }
                break;
            case 'SO':
                switch ($this->Weather) {
                    case 0: return 'Clear afternoon';
                    case 1: return 'Overcast day';
                    case 2: return 'Cloudy sunset';
                }
                break;
            case 'FE':
                switch ($this->Weather) {
                    case 0: return 'Clear day';
                    case 1: return 'Cloudy sunset';
                    case 2: return 'Overcast dusk';
                }
                break;
            case 'AU':
                switch ($this->Weather) {
                    case 0: return 'Overcast afternoon';
                    case 1: return 'Clear morning';
                    case 2: return 'Cloudy sunset';
                }
                break;
            case 'KY':
                switch ($this->Weather) {
                    case 0: return 'Clear day';
                    case 1: return 'Cloudy afternoon';
                    case 2: return 'Cloudy morning';
                }
                break;
            case 'WE':
                switch ($this->Weather) {
                    case 0: return 'Clear day';
                    case 1: return 'Cloudy sunset';
                }
                break;
            case 'AS':
                switch ($this->Weather) {
                    case 0: return 'Clear day';
                    case 1: return 'Cloudy afternoon';
                }
                break;
            case 'RO':
                switch ($this->Weather) {
                    case 0: return 'Clear day';
                    case 1: return 'Clear morning';
                    case 2: return 'Overcast afternoon';
                    case 3: return 'Clear sunset';
                }
                break;
        }

        return $this->Weather;
    }

    public function sendMsgHost($text, $color = \Insim\Types\MsgTypes::NORMAL) {
        $this->sendMsgPlayer($text, 255, $color);
    }

    public function sendMsgPlayer($text, $ucid = 255, $color = \Insim\Types\MsgTypes::NORMAL) {
        if (!$this->local) {
            $msg = new \Insim\Packets\isMTC();
            $msg->UCID = $ucid;
            $msg->Text = $color . $text;
        } else {
            if ($ucid == $this->localUCID) {
                $msg = new \Insim\Packets\isMSL();
                $msg->Msg = $color . $text;
            }
        }

        if (isset($msg))
            $this->insim->send($msg);
    }

    public function sendDebugMsg($text) {
        if (DEBUG_CHAT) {
            $this->sendMsgPlayer($text);
        }
    }

    public function sendCmd($text) {
        if (!$this->local) {
            $msg = new \Insim\Packets\isMST();
            $msg->Msg = $text;
        } else {
            $msg = new \Insim\Packets\isMSL();
            $msg->Msg = 'CMD: ' . $text;
        }

        $this->insim->send($msg);
    }

    public function saveHostConfig($key, $value) {
        $this->sendCmd("/{$key} {$value}");
        $this->config[$key] = $value;
    }

    public function update() {
        if ($this->timerService) {
            $this->timerService->run();
        }
    }

    public function changeTrack($trackName, $timer = 9, $callback = null) {
        $this->changeTrackTo = $trackName;
        $this->finishedCount = 0;
        $this->track_state = Host::TRACK_LOADING;

        $this->addTimer(new Timer('trackChange', $timer, function() {
            if ($this->changeTrackTo)
                $this->sendCmd("/track {$this->changeTrackTo}");
        }));

        if ($callback) {
            $this->callbacks[\Insim\Types\Event::TRACK_LOADED][] = $callback;
        }

        $this->sendMsgHost('> Changing track, please wait ...', \Insim\Types\MsgTypes::YELLOW);
    }

    public function addTimer(Timer $timer) {
        $this->timerService->addTimer($timer);
        $this->sendDebugMsg("DEBUG: timer added {$timer->name} ({$timer->uid}) - {$timer->timer} s");
    }

    public function removeTimer($uuid = null) {
        $this->sendDebugMsg("DEBUG: timers removed");
        $this->timerService->removeTimer($uuid);
    }

    public function getTimerByName($name) {
        return $this->timerService->getTimerByName($name);
    }

    public function restartCommand($delay, $command, $lenght = null) {
        switch ($command) {
            case 'qualify':
                if ($delay == -1) {
                    $this->sendCmd("/qual {$lenght}");
                    $this->sendMsgHost("> Qualify time changed to {$lenght} minutes", \Insim\Types\MsgTypes::YELLOW);
                } else {
                    $this->insim->playerService->eventCounterStarted($delay, true, 0, 'QUALIFY_STARTS');
                    $this->addTimer(new Timer('qualify', ($delay - self::RESTART_DELAY), function() use($lenght) {
                        $this->sendCmd("/qual {$lenght}");
                        $this->sendCmd('/qualify');
                    }));
                }
                break;
            case 'end':
                $this->insim->playerService->eventCounterStarted($delay, true, 0, 'RACE_ENDS');
                $this->addTimer(new Timer('end', ($delay - self::RESTART_DELAY), function() {
                    $this->hostService->rotation();
                }));
                break;
            case 'restart':
                $this->insim->playerService->eventCounterStarted($delay, true, 0, 'RACE_STARTS');
                $this->addTimer(new Timer('restart', ($delay - self::RESTART_DELAY), function() {
                    $this->sendCmd('/restart');
                }));
                break;
        }
    }

    public function applyAllowedCar($car, $value) {
        $this->settings->saveAllowedCar($car, $value);
        $this->applyAllowedCars();
    }

    public function applyAllowedCarRest($car, $key, $value) {
        $this->settings->saveAllowedCarRest($car, $key, $value);
        $this->applyAllowedCars();
    }

    public function applyAllowedCars() {
        $cars = $this->settings->get('allowed-cars');
        $UVal = 0;

        $isHCP = new \Insim\Packets\isHCP();

        foreach ($cars as $carCode => $car) {
            if (isset($car['enabled']) && $car['enabled']) {
                $insimCar = \Insim\Model\Car::getInsimCode($carCode);
                $UVal += $insimCar;
            }
        }

        $small_alc = new \Insim\Packets\isSMALL();
        $small_alc->SubT = \Insim\Packets\isSMALL::SMALL_ALC;
        $small_alc->UVal = $UVal;
        $this->insim->send($small_alc);

        for ($i = 0; $i < 32; ++$i) {
            $handicap = new \Insim\Packets\CarHCP();
            $handicap->H_Mass = 0;
            $handicap->H_TRes = 0;

            $isHCP->Info[$i] = $handicap;
        }

        $index = 0;
        foreach (Car::$insimCodes as $car => $code) {
            $handicap = new \Insim\Packets\CarHCP();
            $handicap->H_Mass = isset($cars[$car]['mass']) ? $cars[$car]['mass'] : 0;
            $handicap->H_TRes = isset($cars[$car]['res']) ? $cars[$car]['res'] : 0;
            $isHCP->Info[$index] = $handicap;
            $index++;
        }

        $this->insim->send($isHCP);
    }

    // Events
    /**
     * Track loaded event
     */
    public function trackLoaded() {
        if ($this->track_state != Host::TRACK_LOADED) {
            $this->track_state = Host::TRACK_LOADED;
            $this->canJoinTrack = true;

            if (isset($this->callbacks[\Insim\Types\Event::TRACK_LOADED]) && count($this->callbacks[\Insim\Types\Event::TRACK_LOADED]) > 0) {
                foreach ($this->callbacks[\Insim\Types\Event::TRACK_LOADED] as $key => $callback) {
                    call_user_func($callback);
                    unset($this->callbacks[\Insim\Types\Event::TRACK_LOADED][$key]);
                }
            }
        }
    }

    /**
     * Called when race start
     */
    public function setRST(\Insim\Packets\isRST $packet) {
        $this->splitsCount = $packet->splitsCount;
        $this->timing = $packet->Timing;
        $this->RaceLaps = $packet->RaceLaps;
        $this->QualMins = $packet->QualMins;
        $this->reversed = $packet->isReversed();

        $this->isFinished = false;
        $this->playerService->eventRaceStarted();

        // Race started
        if ($this->RaceLaps > 0) {
            $this->race_state = Host::RACE_INPROGRESS;
        }

        // Qualify started
        if ($this->RaceLaps == 0 && $this->QualMins > 0) {
            $this->race_state = self::QUALIFY_INPROGRESS;
        }

        if ($packet->ReqI == 0) {
            $this->removeTimer();
            $this->results->startResults($packet);
        }
    }

    /**
     * Called on state change
     * @param \Insim\Packets\isSTA $packet
     */
    public function setSTA(\Insim\Packets\isSTA $packet, $trackChangedCallBack = null) {
        $this->Track = $packet->Track;
        $this->Wind = $packet->Wind;
        $this->Weather = $packet->Weather;
        $this->ViewPLID = $packet->ViewPLID;

        // Race finnished before tracker connected detection
        if (!$this->isFinished && $packet->NumFinished > 0) {
            $this->hostService->raceFinished(new \Insim\Packets\isFIN(), $count = false);
        }

        if (empty($this->lastTrack)) {
            $this->lastTrack = $this->Track;
            $trackChangedCallBack($first = true);
        }

        if ($this->Track != $this->lastTrack) {
            $this->lastTrack = $this->Track;
            if ($trackChangedCallBack) {
                $trackChangedCallBack();
            }
        }
    }

    /**
     * Called when first player finish race
     */
    public function finish($count = true) {
        $this->race_state = Host::RACE_FINISHED;
        $this->isFinished = true;

        if ($count)
            $this->finishedCount++;

        // UI Event
        if (isset($this->insim->playerService->players[$this->id])) {
            foreach ($this->insim->playerService->players[$this->id] as $player) {
                $player->ui->eventRaceFinished();
            }
        }

        $restart_after = $this->settings->get('race-auto-restart-after') - self::RESTART_DELAY;
        $delayed_show = $restart_after <= 34 ? 0 : 30;

        // Track rotation enabled and finished enough races
        if ($this->settings->get('track-rotation') && $this->finishedCount >= $this->settings->get('rotation-races')) {
            $this->addTimer(new Timer('trackRotate', $restart_after, function() {
                $this->hostService->rotation();
            }));

            // Show counter
            $this->addTimer(new Timer('rc_timer', $delayed_show, function() use ($restart_after, $delayed_show) {
                $this->insim->playerService->eventCounterStarted(($restart_after - $delayed_show + self::RESTART_DELAY), true, $delayed_show, $title = 'TRACK_CHANGE');
            }));
            return;
        }

        // Restart timer
        if ($this->settings->get('race-auto-restart')) {
            $this->addTimer(new Timer('restart', $restart_after, function() {
                $this->sendCmd('/restart');
            }));

            // Show counter
            $this->addTimer(new Timer('rc_timer', $delayed_show, function() use ($restart_after, $delayed_show) {
                $this->insim->playerService->eventCounterStarted(($restart_after - $delayed_show + self::RESTART_DELAY), true, $delayed_show, $title = 'RACE_STARTS');
            }));
        }
    }

    // Commands
    public function sendPlayerToPit($UName) {
        $this->sendCmd("/pitlane {$UName}");
    }

    public function givePlayerPenalty($UName, $penalty) {
        //p_dt USERNAME       :give drive through penalty
        //p_sg USERNAME       :give stop-go penalty
        //p_30 USERNAME       :give 30 second time penalty
        //p_45 USERNAME       :give 45 second time penalty

        switch ($penalty) {
            case 'dt': $this->sendCmd("/p_dt {$UName}");
                break;
            case 'sg': $this->sendCmd("/p_sg {$UName}");
                break;
            case '30': $this->sendCmd("/p_30 {$UName}");
                break;
            case '45': $this->sendCmd("/p_45 {$UName}");
                break;
        }
    }

    public function clearPlayerPenalty($UName) {
        $this->sendCmd("/p_clear {$UName}");
    }

    public function spectatePlayer($UName) {
        $this->sendCmd("/spec {$UName}");
    }

}
