<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class Player extends CoreModel {

    public $id;
    public $UName;
    public $PName;
    public $PName_utf;
    public $IPAddress;
    public $country;
    public $UserID;
    public $lang;
    public $timezone;
    public $gmt;
    public $isp;
    public $last_joined;
    
    public $role;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->UName = (!empty($data['UName'])) ? $data['UName'] : null;
        $this->PName = (!empty($data['PName'])) ? $data['PName'] : null;
        $this->PName_utf = (!empty($data['PName_utf'])) ? $data['PName_utf'] : null;
        $this->IPAddress = (!empty($data['IPAddress'])) ? $data['IPAddress'] : null;
        $this->UserID = (!empty($data['UserID'])) ? $data['UserID'] : null;
        $this->lang = (!empty($data['lang'])) ? $data['lang'] : null;
        $this->country = (!empty($data['country'])) ? $data['country'] : null;
        $this->timezone = (!empty($data['timezone'])) ? $data['timezone'] : null;
        $this->gmt = (!empty($data['gmt'])) ? $data['gmt'] : null;
        $this->isp = (!empty($data['isp'])) ? $data['isp'] : null;
        $this->last_joined = (!empty($data['last_joined'])) ? $data['last_joined'] : null;
        $this->role = (!empty($data['role'])) ? $data['role'] : 'guest';
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['UName'] = (!empty($this->UName)) ? $this->UName : null;
        $data['PName'] = (!empty($this->PName)) ? $this->PName : null;
        $data['PName_utf'] = (!empty($this->PName_utf)) ? $this->PName_utf : null;
        $data['IPAddress'] = (!empty($this->IPAddress)) ? $this->IPAddress : null;
        $data['UserID'] = (!empty($this->UserID)) ? $this->UserID : null;
        $data['lang'] = (!empty($this->lang)) ? $this->lang : null;
        $data['country'] = (!empty($this->country)) ? $this->country : null;
        $data['timezone'] = (!empty($this->timezone)) ? $this->timezone : null;
        $data['gmt'] = (!empty($this->gmt)) ? $this->gmt : null;
        $data['isp'] = (!empty($this->isp)) ? $this->isp : null;
        $data['last_joined'] = (!empty($this->last_joined)) ? $this->last_joined : null;
        return $data;
    }

}
