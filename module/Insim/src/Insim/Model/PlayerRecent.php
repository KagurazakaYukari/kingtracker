<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class PlayerRecent extends CoreModel {

    public $id;
    public $player_id;
    public $host_id;
    public $PName;
    public $UName;
    public $date;
    public $reason;
    public $reason_string = '';

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->player_id = (!empty($data['player_id'])) ? $data['player_id'] : null;
        $this->host_id = (!empty($data['host_id'])) ? $data['host_id'] : null;
        $this->PName = (!empty($data['PName'])) ? trim($data['PName']) : null;
        $this->UName = (!empty($data['UName'])) ? trim($data['UName']) : null;
        $this->reason = (isset($data['reason'])) ? $data['reason'] : 0;
        $this->reason_string = \Insim\Packets\isCNL::reasonToString($this->reason);
        $this->date = (!empty($data['date'])) ? $data['date'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['player_id'] = (!empty($this->player_id)) ? $this->player_id : null;
        $data['host_id'] = (!empty($this->host_id)) ? $this->host_id : null;
        $data['PName'] = (!empty($this->PName)) ? $this->PName : null;
        $data['UName'] = (!empty($this->UName)) ? $this->UName : null;
        $data['reason'] = (isset($this->reason)) ? $this->reason : 0;
        
        return $data;
    }
}
