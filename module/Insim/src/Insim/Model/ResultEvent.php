<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class ResultEvent extends CoreModel{
    const SPLIT = 'SPLIT';
    const LAP = 'LAP';
    const PIT = 'PIT';
    const PENALTY = 'PENALTY';
    
    public $id;
    public $result_id;
    public $UName;
    public $event;
    public $data;
    public $time;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->result_id = (!empty($data['result_id'])) ? $data['result_id'] : null;
        $this->UName = (!empty($data['UName'])) ? $data['UName'] : null;
        $this->event = (!empty($data['event'])) ? $data['event'] : null;
        $this->data = (!empty($data['data'])) ? json_decode($data['data'], true) : null;
        $this->time = (!empty($data['time'])) ? $data['time'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['result_id'] = (!empty($this->result_id)) ? $this->result_id : null;
        $data['UName'] = (!empty($this->UName)) ? $this->UName : null;
        $data['event'] = (!empty($this->event)) ? $this->event : null;
        $data['data'] = (!empty($this->data)) ? json_encode($this->data) : null;
        $data['time'] = (!empty($this->time)) ? $this->time : null;
        return $data;
    }

}