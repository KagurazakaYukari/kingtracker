<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class HostPlayer extends CoreModel{
    public $host_id;
    public $UCID;
    public $PLID;
    public $PName;
    public $PName_utf;
    public $PType;
    public $Flags;
    public $Flags_string;
    public $CName;
    public $SetF;

    public function exchangeArray($data) {
        $this->host_id = (!empty($data['host_id'])) ? $data['host_id'] : null;
        $this->UCID = (!empty($data['UCID'])) ? $data['UCID'] : null;
        $this->PLID = (!empty($data['PLID'])) ? $data['PLID'] : null;
        $this->PName = (!empty($data['PName'])) ? $data['PName'] : null;
        $this->PName_utf = (!empty($data['PName_utf'])) ? $data['PName_utf'] : null;
        $this->PType = (!empty($data['PType'])) ? $data['PType'] : null;
        $this->Flags = (!empty($data['Flags'])) ? $data['Flags'] : null;
        $this->Flags_string = (!empty($data['Flags_string'])) ? $data['Flags_string'] : null;
        $this->CName = (!empty($data['CName'])) ? $data['CName'] : null;
        $this->SetF = (!empty($data['SetF'])) ? $data['SetF'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['host_id'] = (!empty($this->host_id)) ? $this->host_id : null;
        $data['UCID'] = (!empty($this->UCID)) ? $this->UCID : null;
        $data['PLID'] = (!empty($this->PLID)) ? $this->PLID : null;
        $data['PName'] = (!empty($this->PName)) ? $this->PName : null;
        $data['PName_utf'] = (!empty($this->PName_utf)) ? $this->PName_utf : null;
        $data['PType'] = (!empty($this->PType)) ? $this->PType : null;
        $data['Flags'] = (!empty($this->Flags)) ? $this->Flags : null;
        $data['Flags_string'] = (!empty($this->Flags_string)) ? $this->Flags_string : null;
        $data['CName'] = (!empty($this->CName)) ? $this->CName : null;
        $data['SetF'] = (!empty($this->SetF)) ? $this->SetF : null;
        return $data;
    }

}