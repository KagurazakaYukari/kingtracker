<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class ResultPlayer extends CoreModel{
    
    public $id;
    public $result_id;
    public $UName;
    public $PName;
    public $UCID;
    public $PLID;
    public $pos;
    public $car;
    public $TTime;
    public $BTime;
    public $TTime_string;
    public $BTime_string;
    public $NumStops;
    public $LapsDone;
    public $PSeconds;
    public $Flags;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->result_id = (!empty($data['result_id'])) ? $data['result_id'] : null;
        $this->UName = (!empty($data['UName'])) ? $data['UName'] : null;
        $this->PName = (!empty($data['PName'])) ? $data['PName'] : null;
        $this->UCID = (!empty($data['UCID'])) ? $data['UCID'] : null;
        $this->PLID = (!empty($data['PLID'])) ? $data['PLID'] : null;
        $this->pos = (!empty($data['pos'])) ? $data['pos'] : null;
        $this->car = (!empty($data['car'])) ? $data['car'] : null;
        $this->TTime = (!empty($data['TTime'])) ? $data['TTime'] : null;
        $this->BTime = (!empty($data['BTime'])) ? $data['BTime'] : null;
        $this->TTime_string = (!empty($data['TTime_string'])) ? $data['TTime_string'] : null;
        $this->BTime_string = (!empty($data['BTime_string'])) ? $data['BTime_string'] : null;
        $this->NumStops = (!empty($data['NumStops'])) ? $data['NumStops'] : null;
        $this->LapsDone = (!empty($data['LapsDone'])) ? $data['LapsDone'] : null;
        $this->PSeconds = (!empty($data['PSeconds'])) ? $data['PSeconds'] : null;
        $this->Flags = (!empty($data['Flags'])) ? $data['Flags'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['result_id'] = (!empty($this->result_id)) ? $this->result_id : null;
        $data['UName'] = (!empty($this->UName)) ? $this->UName : null;
        $data['pos'] = $this->pos;
        $data['car'] = (!empty($this->car)) ? $this->car : null;
        $data['TTime'] = (!empty($this->TTime)) ? $this->TTime : 0;
        $data['BTime'] = (!empty($this->BTime)) ? $this->BTime : null;
        $data['TTime_string'] = (!empty($this->TTime_string)) ? $this->TTime_string : 0;
        $data['BTime_string'] = (!empty($this->BTime_string)) ? $this->BTime_string : null;
        $data['NumStops'] = (!empty($this->NumStops)) ? $this->NumStops : 0;
        $data['LapsDone'] = (!empty($this->LapsDone)) ? $this->LapsDone : 0;
        $data['PSeconds'] = (!empty($this->PSeconds)) ? $this->PSeconds : 0;
        $data['Flags'] = (!empty($this->Flags)) ? $this->Flags : 0;
        return $data;
    }

}