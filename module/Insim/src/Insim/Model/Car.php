<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class Car extends CoreModel {

    public static $insimCodes = array(
        'XFG' => 1,
        'XRG' => 2,
        'XRT' => 4,
        'RB4' => 8,
        'FXO' => 16,
        'LX4' => 32,
        'LX6' => 64,
        'MRT' => 128,
        'UF1' => 256,
        'RAC' => 512,
        'FZ5' => 1024,
        'FOX' => 2048,
        'XFR' => 4096,
        'UFR' => 8192,
        'FO8' => 16384,
        'FXR' => 32768,
        'XRR' => 65536,
        'FZR' => 131072,
        'BF1' => 262144,
        'FBM' => 524288,
    );
    public $id;
    public $code;
    public $name;
    public $mass;
    public $res;
    public $class;
    public $licence;
    public $custom;
    public $ord;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->code = (!empty($data['code'])) ? $data['code'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
        $this->mass = (!empty($data['mass'])) ? $data['mass'] : null;
        $this->res = (!empty($data['res'])) ? $data['res'] : null;
        $this->class = (!empty($data['class'])) ? $data['class'] : null;
        $this->licence = (!empty($data['licence'])) ? $data['licence'] : null;
        $this->custom = (!empty($data['custom'])) ? $data['custom'] : null;
        $this->ord = (!empty($data['ord'])) ? $data['ord'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['code'] = (!empty($this->code)) ? $this->code : null;
        $data['name'] = (!empty($this->name)) ? $this->name : null;
        $data['mass'] = (!empty($this->mass)) ? $this->mass : null;
        $data['res'] = (!empty($this->res)) ? $this->res : null;
        $data['class'] = (!empty($this->class)) ? $this->class : null;
        $data['licence'] = (!empty($this->licence)) ? $this->licence : null;
        $data['custom'] = (!empty($this->custom)) ? $this->custom : null;
        $data['ord'] = (!empty($this->ord)) ? $this->ord : null;

        return $data;
    }

    public static function getInsimCode($car) {
        return isset(self::$insimCodes[$car]) ? self::$insimCodes[$car] : 0;
    }

}
