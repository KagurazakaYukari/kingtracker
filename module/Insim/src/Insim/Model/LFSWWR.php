<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class LFSWWR extends CoreModel{
    public $id;
    public $id_wr;
    public $track;
    public $car;
    public $splits;
    public $splits_string;
    public $time;
    public $time_string;
    public $flags;
    public $UName;
    public $timestamp;
    public $last_update;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : 0;
        $this->id_wr = (!empty($data['id_wr'])) ? $data['id_wr'] : null;
        $this->car = (!empty($data['car'])) ? $data['car'] : null;
        $this->track = (!empty($data['track'])) ? $data['track'] : null;
        $this->splits = (!empty($data['splits'])) ? json_decode($data['splits'], true) : null;
        $this->splits_string = (!empty($data['splits_string'])) ? json_decode($data['splits_string'], true) : null;
        $this->time = (!empty($data['time'])) ? $data['time'] : null;
        $this->time_string = (!empty($data['time_string'])) ? $data['time_string'] : null;
        $this->flags = (!empty($data['flags'])) ? $data['flags'] : null;
        $this->UName = (!empty($data['UName'])) ? $data['UName'] : null;
        $this->timestamp = (!empty($data['timestamp'])) ? $data['timestamp'] : null;
        $this->last_update = (!empty($data['last_update'])) ? $data['last_update'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : 0;
        $data['id_wr'] = (!empty($this->id_wr)) ? $this->id_wr : null;
        $data['car'] = (!empty($this->car)) ? $this->car : null;
        $data['track'] = (!empty($this->track)) ? $this->track : null;
        $data['splits'] = (!empty($this->splits)) ? json_encode($this->splits) : null;
        $data['splits_string'] = (!empty($this->splits_string)) ? json_encode($this->splits_string) : null;
        $data['time'] = (!empty($this->time)) ? $this->time : null;
        $data['time_string'] = (!empty($this->time_string)) ? $this->time_string : null;
        $data['flags'] = (!empty($this->flags)) ? $this->flags : null;
        $data['UName'] = (!empty($this->UName)) ? $this->UName : null;
        $data['timestamp'] = (!empty($this->timestamp)) ? $this->timestamp : null;
        $data['last_update'] = (!empty($this->last_update)) ? $this->last_update : null;
        
        return $data;
    }
}