<?php

namespace Insim\Model;

use Application\Model\CoreModel;
use Insim\Helper\Point2D;
use Insim\Packets\CompCar;
use Insim\Types\MsgTypes;

class PlayerClass extends CoreModel {

    public $PLID;
    public $UCID;
    public $host;
    public $host_id;
    public $ui;
    public $acl;
    public $commandService;
    public $playerService;
    public $lapsService;
    public $translator;
    public $player_id;
    public $name;
    public $UName;
    public $pll;
    public $local = false;
    public $compCar = null;
    public $position = null;
    public $settings;
    public $voted = false;
    public $requestClear = false;
    // Lag variables
    public $last_lag_state = false;
    public $lag_timer = 0;
    public $lag_total = 0;
    // Safety
    public $idle_time = null;
    public $isIdle = false;
    public $isIdleWarned = false;
    // Pit stops
    public $pitstops = array();

    public function __construct($UCID) {
        $this->UCID = $UCID;
    }

    public function exchangeArray($data) {
        $this->PLID = (!empty($data['PLID'])) ? $data['PLID'] : null;
        $this->UCID = (!empty($data['UCID'])) ? $data['UCID'] : null;
        $this->host_id = (!empty($data['host_id'])) ? $data['host_id'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['PLID'] = (!empty($this->PLID)) ? $this->PLID : null;
        $data['UCID'] = (!empty($this->UCID)) ? $this->UCID : null;
        $data['host_id'] = (!empty($this->host_id)) ? $this->host_id : null;
        return $data;
    }

    /**
     * Send message to user
     * @param string $msg
     * @param string $color
     */
    public function sendMsg($msg, $color = MsgTypes::NORMAL) {
        if (isset($this->host) && isset($this->UCID)) {
            $this->host->sendMsgPlayer($msg, $this->UCID, $color);
        }
    }

    /**
     * Send translated message to user
     * @param string $msg
     * @param MsgTypes $color = MsgTypes::NORMAL
     * @param array $params [key => value]
     */
    public function sendTranslatedMsg($msg, $color = MsgTypes::NORMAL, $params = []) {
        if (isset($this->host) && isset($this->UCID)) {
            $out = $this->translator->translateLFS($msg);

            if ($params) {
                foreach ($params as $key => $value) {
                    $out = str_replace('{' . $key . '}', $value, $out);
                }
            }

            $this->host->sendMsgPlayer($out, $this->UCID, $color);
        }
    }

    public function setCompCar(CompCar $compCar) {
        $this->compCar = $compCar;
        $this->position = new Point2D($compCar->X, $compCar->Y);

        //$this->lagDetection();
    }

    private function lagDetection() {
        if (!$this->last_lag_state) {
            // Lag start
            if ($this->compCar->isLagging()) {
                $this->last_lag_state = true;
                $this->lag_timer = microtime(true);
            }
        } else {
            // Lag stop
            if (!$this->compCar->isLagging()) {
                $this->last_lag_state = false;
                $time_elapsed_secs = microtime(true) - $this->lag_timer;
                $this->lag_total += $time_elapsed_secs;

                $time_in_ms = number_format(($time_elapsed_secs * 1000), 0);

                if ($time_in_ms >= 300) {
                    //$this->sendMsg('You were lagging '.$time_in_ms.' ms', MsgTypes::YELLOW);
                }
            }
        }
    }

}
