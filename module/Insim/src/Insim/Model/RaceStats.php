<?php

namespace Insim\Model;

use Application\Model\CoreModel;
use Insim\Helper\InSimHelper;
use Insim\Packets\isLap;
use Insim\Packets\isSPX;

class RaceStats extends CoreModel {

    public $host;
    public $players;
    public $numLaps;
    public $currentLap;
    public $splits;
    public $sectors;
    public $laps;
    public $finish_sector;

    public function __construct() {
        $this->reset();
    }

    public function reset() {
        $this->numLaps = 0;
        $this->currentLap = 1;
        $this->splits = [];
        $this->sectors = [];
        $this->laps = [];
        $this->finish_sector = null;

        if (count($this->players) > 0) {
            foreach ($this->players as $player) {
                if ($player['stint']) {
                    $player['stint']->reset();
                }
            }
        }
    }

    public function eventSplit(isSPX $split) {
        if ($split->STime == 3600000) {
            return;
        }

        foreach ($this->players as $player) {
            if ($player['PLID'] == $split->PLID) {
                $player['stint']->addSplit($split);
            }
        }
    }

    public function eventLap(isLap $lap) {
        if ($lap->LTime == 3600000) {
            return;
        }

        foreach ($this->players as $player) {
            if ($player['PLID'] == $lap->PLID) {
                $player['stint']->addLap($lap);
            }
        }
    }

    public function eventNewPlayer(PlayerClass $player) {
        if (isset($this->players[$player->UCID])) {
            $this->players[$player->UCID]['PLID'] = $player->PLID;
            $this->players[$player->UCID]['name'] = $player->name;
            $this->players[$player->UCID]['car'] = $player->pll->CName;
        } else {
            $temp = array(
                'PLID' => $player->PLID,
                'name' => $player->name,
                'car' => $player->pll->CName,
                'stint' => new Stint()
            );
            $temp['stint']->splits_count = $player->host->splitsCount;

            $this->players[$player->UCID] = $temp;
        }
    }

    public function eventRaceStarted($splitsCount) {
        $this->reset();

        if (count($this->players) > 0) {
            foreach ($this->players as $key => $player) {
                if (!$player['stint'])
                    continue;

                $this->players[$key]['stint']->splits_count = $splitsCount;
            }
        }
    }

    public function getTheoreticalBest() {
        $return = array();
        $best = null;

        foreach ($this->players as $player) {
            if (!$player['stint']->tbLap)
                continue;

            if ($best > $player['stint']->tbLap OR $best == null) {
                $best = $player['stint']->tbLap;
            }

            $sectors = array();
            foreach ($player['stint']->bestSectors as $sector) {
                $sectors[] = InSimHelper::timeToString($sector);
            }

            $return[] = array(
                'name' => $player['name'],
                'PLID' => $player['PLID'],
                'car' => $player['car'],
                'sectors' => $player['stint']->bestSectors,
                'sectors_string' => $sectors,
                'tb' => $player['stint']->tbLap,
                'tb_string' => InSimHelper::timeToString($player['stint']->tbLap),
            );
        }

        // Sort
        usort($return, function($a, $b) {
            return $a['tb'] - $b['tb'];
        });

        // Diffs
        foreach ($return as $key => $value) {
            $diff = $best ? ($value['tb'] - $best) : 0;
            if ($diff > 0) {
                $return[$key]['tb_diff'] = InSimHelper::diffToString($diff);
            } else {
                $return[$key]['tb_diff'] = '';
            }
        }

        return $return;
    }

    public function exchangeArray($data) {
        
    }

    public function exchangeObject() {
        
    }

}
