<?php

return array(
    'guest' => array( 
        'opt',
        'ver',
        'lh',
        'help',
        'pl',
        'tr',
        'top',
        'tm',
        'sc',
        'grid'
    ),
    'marshal' => array(
        'kick',
        'spec',
        'penalty',
        'race-vote',
        'race-vote-cancel',
        'restart',
        'qualify',
        'end',
        'cv',
        'recent'
    ),
    'admin'=> array(
        'debug',
        'test',
        'ban',
        'unban',
        'host',
        'laps',
        'hours',
        'reorder',
        'reload',
        'status',
        'acl',
        'axlist',
    ),
    'superadmin' => array()
);